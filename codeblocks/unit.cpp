/////////////////////////////////////////////////
//                  UNIT.CPP                   //
//                                             //
//     by Gartner Esther & Briere Benjamin     //
//                                             //
/////////////////////////////////////////////////

#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <SDL/SDL_ttf.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

#include "structures.h"
#include "map.h"
#include "unit.h"
#include "menu.h"
#include "player.h"
#include "game.h"
#include "aStar.h"

#define MAXUNITE 10
#define NBUNITE 5


void soldierCreator(int x, int y, int nbSquare, SDL_Surface* surface, map_square *game_map, int type, player *p, int player){
    int sizeSquare;
    int spaceBetween = 0;
    int placement = 0;
    if(surface->w / nbSquare > (surface->h / nbSquare)){
        sizeSquare = (surface->h - (nbSquare-1)*spaceBetween) / nbSquare;
    }
    else{
        sizeSquare = (surface->w - (nbSquare-1)*spaceBetween) / nbSquare;
    }
    int cX, cY;
    cX = x/(sizeSquare);
    cY = y/(sizeSquare);
/*
    printf("\n LECTURE DE LA LISTE UNITE DE PLAYER ==================================\n");
    for (int i = 0 ; i < MAXUNITE ; i++){
        printf("--- %d : type %d \n",i, p[player].liste[i].type_soldier != player+1);
    }
    printf("\n FIN LECTURE DE LA LISTE UNITE DE PLAYER ===============================\n");
*/

    if(type == 1){
        game_map[cY*nbSquare+cX].type_soldier = type+player*5;
        game_map[cY*nbSquare+cX].dexterity = 3;
        game_map[cY*nbSquare+cX].life = 100;
        game_map[cY*nbSquare+cX].range = 1;
        game_map[cY*nbSquare+cX].strength = 30;
        game_map[cY*nbSquare+cX].fuel = 0;
        game_map[cY*nbSquare+cX].action = 0;
        game_map[cY*nbSquare+cX].player = player+1;
        for (int i = 0 ; i < MAXUNITE ; i++){
            if(placement == 0 && p[player].liste[i].player != player+1){
                printf("\n--------------------------------placement de la nouvelle unit� en case : %d \n",i);
                p[player].liste[i] = game_map[cY*nbSquare+cX];
                placement = 1;
            }
        }

    }

    if(type == 2){
        game_map[cY*nbSquare+cX].type_soldier = type+player*5;
        game_map[cY*nbSquare+cX].dexterity = 2;
        game_map[cY*nbSquare+cX].life = 100;
        game_map[cY*nbSquare+cX].range = 1;
        game_map[cY*nbSquare+cX].strength = 40;
        game_map[cY*nbSquare+cX].fuel = 0;
        game_map[cY*nbSquare+cX].action = 0;
        game_map[cY*nbSquare+cX].player = player+1;
        for (int i = 0 ; i < MAXUNITE ; i++){
            if(placement == 0 && p[player].liste[i].player != player+1){
                printf("\n--------------------------------placement de la nouvelle unit� en case : %d \n",i);
                p[player].liste[i] = game_map[cY*nbSquare+cX];
                placement = 1;
            }
        }
    }

    if(type == 3){
        game_map[cY*nbSquare+cX].type_soldier = type+player*5;
        game_map[cY*nbSquare+cX].dexterity = 4;
        game_map[cY*nbSquare+cX].life = 100;
        game_map[cY*nbSquare+cX].range = 2;
        game_map[cY*nbSquare+cX].strength = 60;
        game_map[cY*nbSquare+cX].fuel = 0;
        game_map[cY*nbSquare+cX].action = 0;
        game_map[cY*nbSquare+cX].player = player+1;
        for (int i = 0 ; i < MAXUNITE ; i++){
            if(placement == 0 && p[player].liste[i].player != player+1){
                printf("\n-------------------------------- placement de la nouvelle unit� en case : %d \n",i);
                p[player].liste[i] = game_map[cY*nbSquare+cX];
                printf("ACHAT TANK = X:%d |Y:%d / player %d / type %d \n",p[player].liste[i].cX, p[player].liste[i].cY, p[player].liste[i].player, p[player].liste[i].type_soldier);
                placement = 1;
            }
        }
    }

    if(type == 4){
        game_map[cY*nbSquare+cX].type_soldier = type+player*5;
        game_map[cY*nbSquare+cX].dexterity = 5;
        game_map[cY*nbSquare+cX].life = 100;
        game_map[cY*nbSquare+cX].range = 1;
        game_map[cY*nbSquare+cX].strength = 60;
        game_map[cY*nbSquare+cX].fuel = 0;
        game_map[cY*nbSquare+cX].action = 0;
        game_map[cY*nbSquare+cX].player = player+1;
        for (int i = 0 ; i < MAXUNITE ; i++){
            if(placement == 0 && p[player].liste[i].player != player+1){
                printf("\n--------------------------------placement de la nouvelle unit� en case : %d \n",i);
                p[player].liste[i] = game_map[cY*nbSquare+cX];
                placement = 1;
            }
        }
    }

    if(type == 5){
        game_map[cY*nbSquare+cX].type_soldier = type+player*5;
        game_map[cY*nbSquare+cX].dexterity = 4;
        game_map[cY*nbSquare+cX].life = 100;
        game_map[cY*nbSquare+cX].range = 3;
        game_map[cY*nbSquare+cX].strength = 75;
        game_map[cY*nbSquare+cX].fuel = 0;
        game_map[cY*nbSquare+cX].action = 0;
        game_map[cY*nbSquare+cX].player = player+1;
        for (int i = 0 ; i < MAXUNITE ; i++){
            if(placement == 0 && p[player].liste[i].player != player+1){
                p[player].liste[i] = game_map[cY*nbSquare+cX];
                placement = 1;
            }
        }
    }

}

void soldierDisplay(SDL_Surface* surface, GLuint texture_soldier[], int nbSquare, map_square *game_map, float aspectRatio){
    int spaceBetween = 0;
    int sizeSquare;
    if(surface->w / nbSquare > (surface->h / nbSquare)){
        sizeSquare = (surface->h - (nbSquare-1)*spaceBetween) / nbSquare;
    }
    else{
        sizeSquare = (surface->w - (nbSquare-1)*spaceBetween) / nbSquare;
    }
    for(int y = 0 ; y< nbSquare ; y++)
    {
        for (int x = 0; x < nbSquare ; x++)
        {
            if(game_map[y*nbSquare+x].type_soldier > 0 ){

                glEnable(GL_TEXTURE_2D);
                glBindTexture(GL_TEXTURE_2D, texture_soldier[(game_map[y*nbSquare+x].type_soldier)-1]);
                glEnable(GL_BLEND);
                glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
                glColor3ub(255, 255, 255);
                drawSquare(surface->w, surface->h, game_map[y*nbSquare+x].cX*sizeSquare+(sizeSquare/2), game_map[y*nbSquare+x].cY*sizeSquare+(sizeSquare/2), sizeSquare, sizeSquare, aspectRatio);
                glDisable(GL_BLEND);
                glBindTexture(GL_TEXTURE_2D, 0);
                glDisable(GL_TEXTURE_2D);
            }
        }
    }
}

map_square unit_soldier(int x, int y, int nbSquare, map_square *game_map, int id){
    game_map[y*nbSquare+x].cX = x ;
    game_map[y*nbSquare+x].cY = y ;
    game_map[y*nbSquare+x].type_soldier = (id-1)*NBUNITE + 1 ;
    game_map[y*nbSquare+x].player = id;
    game_map[y*nbSquare+x].life = 100;
    game_map[y*nbSquare+x].strength = 30;
    game_map[y*nbSquare+x].dexterity = 3;
    game_map[y*nbSquare+x].range = 1;
    game_map[y*nbSquare+x].fuel = 3;
    game_map[y*nbSquare+x].action = 1;
    return game_map[y*nbSquare+x];
}

void reapprovisionnement(int nbSquare, map_square *game_map, player *p, int player){
    int x, y;
    //printf("\nREAPPROVISIONNEMENT ============================================================================= \n");
    for( int i = 0; i < MAXUNITE ; i++){
        if (p[player].liste[i].player == player+1){
            //printf("\nCASE %d UNITE APPARTENANT � %d \n", i, p[player].liste[i].player);
            //printf("REAPPROVISONNEMENT UNITE %d \n", i);
            p[player].liste[i].fuel = p[player].liste[i].dexterity;
            p[player].liste[i].action = 1;
            game_map[(p[player].liste[i].cY)*nbSquare+(p[player].liste[i].cX)].fuel = p[player].liste[i].fuel;
            game_map[(p[player].liste[i].cY)*nbSquare+(p[player].liste[i].cX)].action = 1;
            //printf("ETAT ACTION : case %d | liste %d \n", game_map[(p[player].liste[i].cY)*nbSquare+(p[player].liste[i].cX)].action ,p[player].liste[i].action);
            //printf("ETAT FUEL : case %d | liste %d \n", game_map[(p[player].liste[i].cY)*nbSquare+(p[player].liste[i].cX)].fuel ,p[player].liste[i].fuel);
            //printf("ETAT CASE : X %d | Y %d \n", game_map[(p[player].liste[i].cY)*nbSquare+(p[player].liste[i].cX)].cX ,game_map[(p[player].liste[i].cY)*nbSquare+(p[player].liste[i].cX)].cY);
            //printf("ETAT LISTE : X %d | Y %d \n", p[player].liste[i].cX, p[player].liste[i].cY);
        }
    }
/*
    for( int i = 0; i < MAXUNITE ; i++){
            printf("\nCASE %d UNITE APPARTENANT � %d \n", i, p[player].liste[i].player);
        }

    printf("================================================================================================= \n\n");
*/
}

void captureBuilding(map_square *game_map, player *game_player, int nbSquare,  float aspectRatio, int pl, int sizeSquare, int x, int y){

    int available;
    if (x != -1 || y != -1){

        if((game_map[y*nbSquare + x].type_soldier == 1 || game_map[y*nbSquare + x].type_soldier == 6 ) && game_map[y*nbSquare + x].action == 1){ // fantassin
          if(game_map[y*nbSquare + x].ground == 7) // case ville neutre
          {
              game_map[y*nbSquare + x].ground = 8 + pl;

              game_player[pl].ville = game_player[pl].ville + 1;

              game_map[y*nbSquare + x].action = 0;
              for( int i = 0; i < game_player[pl].nbUnite ; i++){
                if(game_player[pl].liste[i].cX == x && game_player[pl].liste[i].cY){
                    game_player[pl].liste[i].action = 0;
                }
              }
          }
          else{
            if( pl == 0){
                if(game_map[y*nbSquare + x].ground == 9) // case ville bleue
                {
                  game_map[y*nbSquare + x].ground = 7;
                  game_player[1].ville = game_player[1].ville - 1;
                  game_map[y*nbSquare + x].action = 0;
                  for( int i = 0; i < game_player[pl].nbUnite ; i++){
                        if(game_player[pl].liste[i].cX == x && game_player[pl].liste[i].cY){
                            game_player[pl].liste[i].action = 0;
                        }
                    }
                }
            }
            else{

                if(game_map[y*nbSquare + x].ground == 8) // case ville rouge
                {

                  game_map[y*nbSquare + x].ground = 7;
                  game_player[0].ville = game_player[0].ville - 1;
                  game_map[y*nbSquare + x].action = 0;
                  for( int i = 0; i < game_player[pl].nbUnite ; i++){
                        if(game_player[pl].liste[i].cX == x && game_player[pl].liste[i].cY){
                            game_player[pl].liste[i].action = 0;
                        }
                    }
                }
            }
          }
        }
    }
}

void rangeCreator(map_range *game_range, map_square *game_map, int x, int y, int nbSquare){
  //printf("========================================================================== HELLO \n\n");
    int range = game_map[y*nbSquare + x].range;
    //printf("========================================================================== RANGE = %d \n", range);
    int j = 0;
    int increment = 1;
    for (int i = 0; i<=range ; i++){ // partie sup�rieur du triangle
        if(y-i >= 0 && y-i<nbSquare && ((y-i) != y)){
        //printf("CASE COMPATIBLE : X %d | Y %d \n", x, y-i);
        game_range[(y-i)*nbSquare + x].x = x;
        game_range[(y-i)*nbSquare + x].y = y-i;
        }
        for (j ; j<range; j++){
            if(y-i >= 0 && y-i<nbSquare){
                if(x-increment>= 0 && x-increment < nbSquare){
                    //printf("CASE COMPATIBLE cas gauche : X %d | Y %d \n", x-increment, y-i);
                    game_range[(y-i)*nbSquare + x-increment].x = x-increment;
                    game_range[(y-i)*nbSquare + x-increment].y = y-i;
                }
                if(x+increment>= 0 && x+increment < nbSquare){
                    //printf("CASE COMPATIBLE cas droite : X %d | Y %d \n", x+increment, y-i);
                    game_range[(y-i)*nbSquare + x+increment].x = x+increment;
                    game_range[(y-i)*nbSquare + x+increment].y = y-i;
                }
                increment++;
             }
         }
         increment = 1;
         j = i+1;
    }
    j = 1;
    int tampon = range;
        for (int i = 1; i<=range ; i++){ // partie inf�rieur du triangle
            if(y+i >= 0 && y+i<nbSquare){
                printf("CASE COMPATIBLE : X %d | Y %d \n", x, y+i);
                 game_range[(y+i)*nbSquare + x].x = x;
                 game_range[(y+i)*nbSquare + x].y = y+i;
            }
            for (j = 1 ; j< tampon; j++){
                if(y+i >= 0 && y+i<nbSquare){
                    if(x-j>= 0 && x-j < nbSquare){
                        printf("CASE COMPATIBLE : X %d | Y %d \n", x-j, y+i);
                        game_range[(y+i)*nbSquare + x-j].x = x-j;
                        game_range[(y+i)*nbSquare + x-j].y = y+i;
                    }
                    if(x+j>= 0 && x+j < nbSquare){
                        printf("CASE COMPATIBLE : X %d | Y %d \n", x+j, y+i);
                        game_range[(y+i)*nbSquare + x+j].x = x+j;
                        game_range[(y+i)*nbSquare + x+j].y = y+i;
                    }
                 }
            }
            tampon --;
        }
}

void mouvementCreator(SDL_Surface* surface, map_range *game_range, map_square *game_map, int x, int y, int nbSquare, int type, float aspectRatio){
    //printf("========================================================================== HELLO \n\n");
    float range = game_map[y*nbSquare + x].fuel ;
    //printf("========================================================================== MOUVEMENT = %d \n", range);
    int j = 0;
    int increment = 1;
    float valid;
    for (int i = 0; i<=range * 2 ; i++){ // partie sup�rieur du triangle
        if(y-i >= 0 && y-i<nbSquare && ((y-i) != y)){
        //printf("CASE COMPATIBLE : X %d | Y %d \n", x, y-i);
            if(game_map[(y-i)*nbSquare + x].type_soldier == 0){ //pas d'unit� pr�sentes sur la case
                valid = mapPath(surface, nbSquare, &game_map[0], aspectRatio, x, y, x, y-i);
                if(valid != -1 && valid<=range){
                    game_range[(y-i)*nbSquare + x].x = x;
                    game_range[(y-i)*nbSquare + x].y = y-i;
                }
            }
        }
        for (j ; j<=range * 2; j++){
            if(y-i >= 0 && y-i<nbSquare){
                if(x-increment>= 0 && x-increment < nbSquare){
                    if(game_map[(y-i)*nbSquare + x-increment].type_soldier == 0){
                        valid = mapPath(surface, nbSquare, &game_map[0], aspectRatio, x, y, x-increment, y-i);
                        if(valid != -1 && valid<=range){
                            game_range[(y-i)*nbSquare + x-increment].x = x-increment;
                            game_range[(y-i)*nbSquare + x-increment].y = y-i;
                        }
                    }
                }
                if(x+increment>= 0 && x+increment < nbSquare){
                    if(game_map[(y-i)*nbSquare + x+increment].type_soldier == 0){
                        valid = mapPath(surface, nbSquare, &game_map[0], aspectRatio, x, y, x+increment, y-i);
                        if(valid != -1 && valid<=range){
                            game_range[(y-i)*nbSquare + x+increment].x = x+increment;
                            game_range[(y-i)*nbSquare + x+increment].y = y-i;
                        }
                    }
                }
                increment++;
             }
         }
         increment = 1;
         j = i+1;
    }
    j = 1;
    float tampon = range;
        for (int i = 1; i<=range * 2 ; i++){ // partie inf�rieur du triangle
            if(y+i >= 0 && y+i<nbSquare){
                if(game_map[(y+i)*nbSquare + x].type_soldier == 0){
                    valid = mapPath(surface, nbSquare, &game_map[0], aspectRatio, x, y, x, y+i);
                    if(valid != -1 && valid<=range ){
                        printf("valid %f et range %f \n", valid, range);
                        game_range[(y+i)*nbSquare + x].x = x;
                        game_range[(y+i)*nbSquare + x].y = y+i;
                    }
                }
            }
            for (j = 1 ; j< tampon * 2; j++){
                if(y+i >= 0 && y+i<nbSquare){
                    if(x-j>= 0 && x-j < nbSquare){
                        if(game_map[(y+i)*nbSquare + x-j].type_soldier == 0){
                            valid = mapPath(surface, nbSquare, &game_map[0], aspectRatio, x, y, x-j, y+i);
                            if(valid != -1 && valid<=range ){
                                    printf("valid %f et range %f \n", valid, range);
                                game_range[(y+i)*nbSquare + x-j].x = x-j;
                                game_range[(y+i)*nbSquare + x-j].y = y+i;
                            }
                        }
                    }
                    if(x+j>= 0 && x+j < nbSquare){
                        if(game_map[(y+i)*nbSquare + x+j].type_soldier == 0){
                            valid = mapPath(surface, nbSquare, &game_map[0], aspectRatio, x, y, x+j, y+i);
                            if(valid != -1 && valid<=range ){
                                    printf("valid %f et range %f \n", valid, range);
                                game_range[(y+i)*nbSquare + x+j].x = x+j;
                                game_range[(y+i)*nbSquare + x+j].y = y+i;
                            }
                        }
                    }
                 }
            }
            tampon --;
        }
}

void unitMouvement(SDL_Surface* surface, map_square *game_map, player *p, int pl, int x, int y, int destX, int destY, int nbSquare, float aspectRatio){

    printf("x = %d \n", x);
    printf("y = %d \n", y);
    printf("destX = %d \n", destX);
    printf("destY = %d \n", destY);

    float range = game_map[y*nbSquare + x].fuel;
    float distance = mapPath(surface, nbSquare, &game_map[0], aspectRatio, x, y, destX, destY);

    printf("range = %d \n", range);
    printf("distance = %d \n", distance);


    if(distance != -1 && distance <= range){
        printf("case disponible \n");
        printf("================================== destination : x %d | y %d \n", destX, destY);
        printf("================================== depart : x %d | y %d \n", x, y);

        game_map[destY * nbSquare + destX].player = pl+1;
        game_map[destY * nbSquare + destX].type_soldier = game_map[y * nbSquare + x].type_soldier;
        game_map[destY * nbSquare + destX].life = game_map[y * nbSquare + x].life;
        game_map[destY * nbSquare + destX].strength = game_map[y * nbSquare + x].strength;
        game_map[destY * nbSquare + destX].dexterity = game_map[y * nbSquare + x].dexterity;
        game_map[destY * nbSquare + destX].range = game_map[y * nbSquare + x].range;
        game_map[destY * nbSquare + destX].action = game_map[y * nbSquare + x].action;
        game_map[destY * nbSquare + destX].fuel = game_map[y * nbSquare + x].fuel - distance;
        printf("NOUVEAUX FUEL : %f \n", game_map[destY * nbSquare + destX].fuel);

        for (int i = 0; i< MAXUNITE ; i++){
            //printf("dans liste : x %d | y %d ", p[pl].liste[i].cX, p[pl].liste[i].cY );
            if( p[pl].liste[i].cX == x && p[pl].liste[i].cY == y ){
                printf("==================================== valeur avant affectattion : x %d | y %d \n", p[pl].liste[i].cX , p[pl].liste[i].cY);
                p[pl].liste[i].cX = destX;
                p[pl].liste[i].cY = destY;
                p[pl].liste[i].fuel = p[pl].liste[i].fuel - distance;
                printf("NOUVEAUX FUEL : %f \n", p[pl].liste[i].fuel);
                printf("==================================== valeur apr�s affectattion : x %d | y %d  \n", p[pl].liste[i].cX , p[pl].liste[i].cY);
            }
        }

        game_map[y * nbSquare + x].player = 0;
        game_map[y * nbSquare + x].type_soldier = 0;
        game_map[y * nbSquare + x].life = 0;
        game_map[y * nbSquare + x].strength = 0;
        game_map[y * nbSquare + x].dexterity = 0;
        game_map[y * nbSquare + x].range = 0;
        game_map[y * nbSquare + x].action = 0;
        game_map[y * nbSquare + x].fuel =0;

        printf("================================== possesseur : %d \n", game_map[destY * nbSquare + destX].player);
    }
}

int rangeCheck(map_square *game_map, int x, int y, int destX, int destY, int nbSquare){
    //printf("========================================================================== HELLO \n\n");
    int range = game_map[y*nbSquare + x].range*2;
    //printf("========================================================================== RANGE = %d \n", range);
    int j = 0;
    int trouve = 0;
    int increment = 1;
    for (int i = 0; i<=range ; i++){ // partie sup�rieur du triangle
        if(y-i >= 0 && y-i<nbSquare && ((y-i) != y)){
        //printf("CASE COMPATIBLE : X %d | Y %d \n", x, y-i);
            if(game_map[(y-i)*nbSquare + x].cX == destX && game_map[(y-i)*nbSquare + x].cY == destY){
                trouve = 1;
            }
        }
        for (j ; j<range; j++){
            if(y-i >= 0 && y-i<nbSquare){
                if(x-increment>= 0 && x-increment < nbSquare){
                    if(game_map[(y-i)*nbSquare + x-increment].cX == destX && game_map[(y-i)*nbSquare + x-increment].cY == destY){
                        trouve = 1;
                    }
                }
                if(x+increment>= 0 && x+increment < nbSquare){
                    if(game_map[(y-i)*nbSquare + x+increment].cX == destX && game_map[(y-i)*nbSquare + x+increment].cY == destY){
                        trouve = 1;
                    }
                }
                increment++;
             }
         }
         increment = 1;
         j = i+1;
    }
    j = 1;
    int tampon = range;
        for (int i = 1; i<=range ; i++){ // partie inf�rieur du triangle
            if(y+i >= 0 && y+i<nbSquare){
                if(game_map[(y+i)*nbSquare + x].cX == destX && game_map[(y+i)*nbSquare + x].cY == destY){
                    trouve = 1;
                }
            }
            for (j = 1 ; j< tampon; j++){
                if(y+i >= 0 && y+i<nbSquare){
                    if(x-j>= 0 && x-j < nbSquare){
                        if(game_map[(y+i)*nbSquare + x-j].cX == destX && game_map[(y+i)*nbSquare + x-j].cY == destY){
                            trouve = 1;
                        }
                    }
                    if(x+j>= 0 && x+j < nbSquare){
                        if(game_map[(y+i)*nbSquare + x+j].cX == destX && game_map[(y+i)*nbSquare + x+j].cY == destY){
                            trouve = 1;
                        }
                    }
                 }
            }
            tampon --;
        }
    return trouve;
}

float force (int attaquant, int defenseur){

    float res = 1;
    if(attaquant == 1){ // fantassin
        if(defenseur == 2){
            res = 2;
        }
        if(defenseur == 3){
            res = 0.5;
        }
        if(defenseur == 4){
            res = 1;
        }
        if(defenseur == 5){
            res = 0.5;
        }
    }
    if(attaquant == 2){ // bazooka
        if(defenseur == 1){
            res = 0.5;
        }
        if(defenseur == 3){
            res = 2;
        }
        if(defenseur == 4){
            res = 1;
        }
        if(defenseur == 5){
            res = 0.5;
        }
    }
    if(attaquant == 3){ // tank
        if(defenseur == 1){
            res = 2;
        }
        if(defenseur == 2){
            res = 0.5;
        }
        if(defenseur == 4){
            res = 2;
        }
        if(defenseur == 5){
            res = 0.5;
        }
    }
    if(attaquant == 4){ // helicoptere
        if(defenseur == 1){
            res = 2;
        }
        if(defenseur == 2){
            res = 2;
        }
        if(defenseur == 3){
            res = 1;
        }
        if(defenseur == 5){
            res = 1;
        }
    }
    if(attaquant == 5){ // bateau
        if(defenseur == 1){
            res = 1;
        }
        if(defenseur == 2){
            res = 1;
        }
        if(defenseur == 3){
            res = 1;
        }
        if(defenseur == 4){
            res = 1;
        }
    }
    return res;
}

void attaque (map_square *game_map, player *p, int pl, int attaquantX, int attaquantY, int defenseurX, int defenseurY, int nbSquare){
    float forceAttaquant, forceDefenseur;
    float typeAttaquant, typeDefenseur;

    if(pl == 0){
        typeAttaquant = game_map[attaquantY * nbSquare + attaquantX].type_soldier;
        typeDefenseur = game_map[defenseurY * nbSquare + defenseurX].type_soldier - NBUNITE;
        forceAttaquant = force(typeAttaquant, typeDefenseur);
        forceDefenseur = force(typeDefenseur, typeAttaquant);
    }
    else{
        typeAttaquant = game_map[attaquantY * nbSquare + attaquantX].type_soldier - NBUNITE;
        typeDefenseur = game_map[defenseurY * nbSquare + defenseurX].type_soldier;
        forceAttaquant = force(typeAttaquant, typeDefenseur);
        forceDefenseur = force(typeDefenseur, typeAttaquant);
    }

    game_map[attaquantY*nbSquare+attaquantX].action = 0;
    game_map[defenseurY*nbSquare+defenseurX].life=game_map[defenseurY*nbSquare+defenseurX].life- float( float(forceAttaquant) * ( float(game_map[attaquantY*nbSquare+attaquantX].life) * (float(game_map[attaquantY*nbSquare+attaquantX].strength)/100)));
    for (int i = 0; i<MAXUNITE; i++){
        if( pl == 0 ){
            if( p[1].liste[i].cX == defenseurX && p[1].liste[i].cY == defenseurY ){
                p[1].liste[i].life = game_map[defenseurY*nbSquare+defenseurX].life;
            }
        }
        else{
            if( p[0].liste[i].cX == defenseurX && p[0].liste[i].cY == defenseurY ){
                p[0].liste[i].life = game_map[defenseurY*nbSquare+defenseurX].life;
            }
        }
    }

    if(game_map[defenseurY*nbSquare+defenseurX].life > 0){
        game_map[attaquantY*nbSquare+attaquantX].life=game_map[attaquantY*nbSquare+attaquantX].life-(forceDefenseur*(game_map[defenseurY*nbSquare+defenseurX].life * game_map[defenseurY*nbSquare+defenseurX].strength/100));

        for (int i = 0; i<MAXUNITE; i++){
            if( pl == 0 ){
                if( p[1].liste[i].cX == attaquantX && p[1].liste[i].cY == attaquantY ){
                    p[1].liste[i].life = game_map[attaquantY*nbSquare+attaquantY].life;
                }
            }
            else{
                if( p[0].liste[i].cX == attaquantX && p[0].liste[i].cY == attaquantY ){
                    p[0].liste[i].life = game_map[attaquantY*nbSquare+attaquantY].life;
                }
            }
        }

    }
    else{
        game_map[defenseurY*nbSquare+defenseurX].action = 0;
        game_map[defenseurY*nbSquare+defenseurX].dexterity = 0;
        game_map[defenseurY*nbSquare+defenseurX].fuel = 0;
        game_map[defenseurY*nbSquare+defenseurX].life = 0;
        game_map[defenseurY*nbSquare+defenseurX].player = 0;
        game_map[defenseurY*nbSquare+defenseurX].range = 0;
        game_map[defenseurY*nbSquare+defenseurX].strength = 0;
        game_map[defenseurY*nbSquare+defenseurX].type_soldier =0;

        //SUPRRESSION DE L'UNITE DEFENSSEUR VAINCU
        if(pl == 1){
            for (int i = 0; i < MAXUNITE ; i++){
                if(p[0].liste[i].cX == defenseurX && p[0].liste[i].cY == defenseurY){
                    p[0].liste[i] = game_map[defenseurY*nbSquare+defenseurX];
                    p[0].liste[i].cX = 0;
                    p[0].liste[i].cY = 0;
                }
            }
            p[0].nbUnite = p[0].nbUnite - 1;
        }
        else{
            for (int i = 0; i < MAXUNITE ; i++){

                if(p[1].liste[i].cX == defenseurX && p[1].liste[i].cY == defenseurY){
                    p[1].liste[i] = game_map[defenseurY*nbSquare+defenseurX];
                    p[1].liste[i].cX = 0;
                    p[1].liste[i].cY = 0;

                }
            }
            p[1].nbUnite = p[1].nbUnite - 1;
        }
    }

    if(game_map[attaquantY*nbSquare+attaquantX].life <= 0){
        game_map[attaquantY*nbSquare+attaquantX].action = 0;
        game_map[attaquantY*nbSquare+attaquantX].dexterity = 0;
        game_map[attaquantY*nbSquare+attaquantX].fuel = 0;
        game_map[attaquantY*nbSquare+attaquantX].life = 0;
        game_map[attaquantY*nbSquare+attaquantX].player = 0;
        game_map[attaquantY*nbSquare+attaquantX].range = 0;
        game_map[attaquantY*nbSquare+attaquantX].strength = 0;
        game_map[attaquantY*nbSquare+attaquantX].type_soldier =0;

        //SUPRRESSION DE L'UNITE ATTAQUANT VAINCU
        if(pl == 1){
            for (int i = 0; i < MAXUNITE ; i++){
                if(p[1].liste[i].cX == attaquantX && p[1].liste[i].cY == attaquantY){
                    p[1].liste[i] = game_map[attaquantY*nbSquare+attaquantX];
                    p[1].liste[i].cX = 0;
                    p[1].liste[i].cY = 0;
                }
            }
            p[1].nbUnite = p[1].nbUnite - 1;
        }
        else{
            for (int i = 0; i < MAXUNITE ; i++){
                if(p[0].liste[i].cX == attaquantX && p[0].liste[i].cY == attaquantY){
                    p[0].liste[i] = game_map[attaquantY*nbSquare+attaquantX];
                    p[0].liste[i].cX = 0;
                    p[0].liste[i].cY = 0;
                }
            }
            p[0].nbUnite = p[0].nbUnite - 1;
        }
    }

    printf("\n");
}

void animationMouvement(map_square *game_map, SDL_Surface* surface, GLuint texture_soldier[],
                        vector < pair<int,int> > tamp, int *indexTemp, int *tamponX, int *tamponY,
                        int *directionTemp, int sizeSquare, float aspectRatio, int nbSquare, int type, int *tamptest, player *p, int pl){

    std::reverse(tamp.begin(), tamp.end());
    int idSoldat = type-1;

    if( ( tamp[(*indexTemp)+1].first > -1 && tamp[(*indexTemp)+1].first <10) ) {
          if(tamp[*indexTemp+1].first > tamp[*indexTemp].first){
                //printf("vers la droite \n");
              *directionTemp = 1; // vers la droite
          }
          if(tamp[*indexTemp+1].first < tamp[*indexTemp].first){
                //printf("vers la gauche \n");
              *directionTemp = 2; // vers la gauche
          }
          if(tamp[*indexTemp+1].second > tamp[*indexTemp].second){
              //printf("vers le bas \n");
              *directionTemp = 3; // vers le bas
          }
          if(tamp[*indexTemp+1].second < tamp[*indexTemp].second){
              //printf("vers le haut \n");
              *directionTemp = 4; // vers le haut
          }

      }
      //printf("direction : %d \n", *directionTemp);
      if(!(*tamponX == tamp[(tamp.size()-1)].first* sizeSquare + (sizeSquare/2) && *tamponY == tamp[(tamp.size()-1)].second* sizeSquare + (sizeSquare/2))){
          if(*directionTemp == 1){
              if(*indexTemp < (tamp.size()-1)){
                  if(*tamponX == tamp[*indexTemp+1].first* sizeSquare + (sizeSquare/2)){
                      *indexTemp = *indexTemp + 1;
                  }
                  else{
                      glEnable(GL_TEXTURE_2D);
                      glBindTexture(GL_TEXTURE_2D, texture_soldier[idSoldat]);
                      glEnable(GL_BLEND);
                      glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
                      glColor3ub(255, 255, 255);
                      drawSquare(surface->w, surface->h, *tamponX, *tamponY, sizeSquare, sizeSquare, aspectRatio);
                      glDisable(GL_BLEND);
                      glBindTexture(GL_TEXTURE_2D, 0);
                      glDisable(GL_TEXTURE_2D);
                      *tamponX = *tamponX + 20;
                  }
              }
          }
          if(*directionTemp == 2){
              if(*indexTemp < (tamp.size()-1)){
                  if(*tamponX == tamp[*indexTemp+1].first * sizeSquare + (sizeSquare/2)){
                      *indexTemp = *indexTemp + 1;
                  }
                  else{
                      glEnable(GL_TEXTURE_2D);
                      glBindTexture(GL_TEXTURE_2D, texture_soldier[idSoldat]);
                      glEnable(GL_BLEND);
                      glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
                      glColor3ub(255, 255, 255);
                      drawSquare(surface->w, surface->h, *tamponX, *tamponY, sizeSquare, sizeSquare, aspectRatio);
                      glDisable(GL_BLEND);
                      glBindTexture(GL_TEXTURE_2D, 0);
                      glDisable(GL_TEXTURE_2D);
                      *tamponX = *tamponX - 20;
                  }

              }
          }
          if(*directionTemp == 3){
              if(*indexTemp < (tamp.size()-1)){
                  if(*tamponY == tamp[*indexTemp+1].second * sizeSquare + (sizeSquare/2)){
                      *indexTemp = *indexTemp + 1;
                  }
                  else{
                      glEnable(GL_TEXTURE_2D);
                      glBindTexture(GL_TEXTURE_2D, texture_soldier[idSoldat]);
                      glEnable(GL_BLEND);
                      glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
                      glColor3ub(255, 255, 255);
                      drawSquare(surface->w, surface->h, *tamponX, *tamponY, sizeSquare, sizeSquare, aspectRatio);
                      glDisable(GL_BLEND);
                      glBindTexture(GL_TEXTURE_2D, 0);
                      glDisable(GL_TEXTURE_2D);
                      *tamponY = *tamponY + 20;
                  }
              }
          }
          if(*directionTemp == 4){
              if( *indexTemp < (tamp.size()-1) ){
                  if(*tamponY == tamp[*indexTemp+1].second * sizeSquare + (sizeSquare/2)){
                      *indexTemp = *indexTemp + 1;
                  }
                  else{
                      glEnable(GL_TEXTURE_2D);
                      glBindTexture(GL_TEXTURE_2D, texture_soldier[idSoldat]);
                      glEnable(GL_BLEND);
                      glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
                      glColor3ub(255, 255, 255);
                      drawSquare(surface->w, surface->h, *tamponX, *tamponY, sizeSquare, sizeSquare, aspectRatio);
                      glDisable(GL_BLEND);
                      glBindTexture(GL_TEXTURE_2D, 0);
                      glDisable(GL_TEXTURE_2D);
                      *tamponY = *tamponY - 20;
                  }

              }
          }
      }
    else{
        *tamptest = 0;
    }
}

