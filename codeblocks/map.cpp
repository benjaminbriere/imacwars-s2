/////////////////////////////////////////////////
//                 MAP.CPP                     //
//                                             //
//     by Gartner Esther & Briere Benjamin     //
//                                             //
/////////////////////////////////////////////////

#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <iostream>

using namespace std;

#include "structures.h"
#include "map.h"
#include "unit.h"
#include "menu.h"
#include "player.h"
//#include "game.cpp"

#define MAXUNITE 10
#define NBUNITE 5

void drawSquare(int width, int height, int x, int y, int sizeW, int sizeH, float aspectRatio){

  glBegin(GL_QUADS);
    if (aspectRatio > 1){
      glTexCoord2f(0, 1);
      glVertex2f(
        (( -1 + 2. * (x-1*sizeW/2) / (float) width )* aspectRatio),
        -(-1 + 2. * (y+1*sizeH/2) / (float) height));
      glTexCoord2f(1, 1);
      glVertex2f(
        (( -1 + 2. * (x+1*sizeW/2) / (float) width) * aspectRatio),
        -(-1 + 2. * (y+1*sizeH/2) / (float) height));
      glTexCoord2f(1, 0);
      glVertex2f(
        (( -1 + 2. * (x+1*sizeW/2) / (float) width) * aspectRatio),
        -(-1 + 2. * (y-1*sizeH/2) / (float) height));
      glTexCoord2f(0, 0);
      glVertex2f(
        (( -1 + 2. * (x-1*sizeW/2) / (float) width) * aspectRatio),
        -(-1 + 2. * (y-1*sizeH/2) / (float) height));
    }
    if (aspectRatio  == 1){
      glTexCoord2f(0, 1);
      glVertex2f(
        (( -1 + 2. * (x-1*sizeW/2) / (float) width )),
        -(-1 + 2. * (y+1*sizeH/2) / (float) height));
      glTexCoord2f(1, 1);
      glVertex2f(
        (( -1 + 2. * (x+1*sizeW/2) / (float) width )),
        -(-1 + 2. * (y+1*sizeH/2) / (float) height));
      glTexCoord2f(1, 0);
      glVertex2f(
        (( -1 + 2. * (x+1*sizeW/2) / (float) width )),
        -(-1 + 2. * (y-1*sizeH/2) / (float) height));
      glTexCoord2f(0, 0);
      glVertex2f(
        (( -1 + 2. * (x-1*sizeW/2) / (float) width )),
        -(-1 + 2. * (y-1*sizeH/2) / (float) height));
    }
    if (aspectRatio  < 1){
      glTexCoord2f(0, 1);
      glVertex2f(
        (( -1 + 2. * (x-1*sizeW/2) / (float) width )),
        -(-1 + 2. * (y+1*sizeH/2) / (float) height) / aspectRatio );
      glTexCoord2f(1, 1);
      glVertex2f(
        (( -1 + 2. * (x+1*sizeW/2) / (float) width )),
        -(-1 + 2. * (y+1*sizeH/2) / (float) height) / aspectRatio );
      glTexCoord2f(1, 0);
      glVertex2f(
        (( -1 + 2. * (x+1*sizeW/2) / (float) width )),
        -(-1 + 2. * (y-1*sizeH/2) / (float) height) / aspectRatio );
      glTexCoord2f(0, 0);
      glVertex2f(
        (( -1 + 2. * (x-1*sizeW/2) / (float) width )),
        -(-1 + 2. * (y-1*sizeH/2) / (float) height) / aspectRatio );
    }
  glEnd();
}

void draw_map_square(float aspectRatio, int width, int height, int x, int y, int sizeW, int sizeH, int type, GLuint texture_map[]){

  if(type == 1){ // plaine
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, texture_map[0]);
    glColor3ub(255, 255, 255);
    drawSquare(width, height, x, y, sizeW, sizeH, aspectRatio);
    glBindTexture(GL_TEXTURE_2D, 0);
    glDisable(GL_TEXTURE_2D);
  }
  else if(type == 2){ // foret
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, texture_map[1]);
    glColor3ub(255, 255, 255);
    drawSquare(width, height, x, y, sizeW, sizeH, aspectRatio);
    glBindTexture(GL_TEXTURE_2D, 0);
    glDisable(GL_TEXTURE_2D);
  }
  else if(type == 3){ // Mer
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, texture_map[2]);
    glColor3ub(255, 255, 255);
    drawSquare(width, height, x, y, sizeW, sizeH, aspectRatio);
    glBindTexture(GL_TEXTURE_2D, 0);
    glDisable(GL_TEXTURE_2D);
  }
  else if(type == 4){ // desert
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, texture_map[3]);
    glColor3ub(255, 255, 255);
    drawSquare(width, height, x, y, sizeW, sizeH, aspectRatio);
    glBindTexture(GL_TEXTURE_2D, 0);
    glDisable(GL_TEXTURE_2D);
  }
  else if(type == 5){ // montagne
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, texture_map[4]);
    glColor3ub(255, 255, 255);
    drawSquare(width, height, x, y, sizeW, sizeH, aspectRatio);
    glBindTexture(GL_TEXTURE_2D, 0);
    glDisable(GL_TEXTURE_2D);
  }
  else if(type == 6){ // route
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, texture_map[5]);
    glColor3ub(255, 255, 255);
    drawSquare(width, height, x, y, sizeW, sizeH, aspectRatio);
    glBindTexture(GL_TEXTURE_2D, 0);
    glDisable(GL_TEXTURE_2D);
  }
  else if(type == 7){ // ville
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, texture_map[6]);
    glColor3ub(255, 255, 255);
    drawSquare(width, height, x, y, sizeW, sizeH, aspectRatio);
    glBindTexture(GL_TEXTURE_2D, 0);
    glDisable(GL_TEXTURE_2D);
  }
  else if(type == 8){ // ville ROUGE
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, texture_map[7]);
    glColor3ub(255, 255, 255);
    drawSquare(width, height, x, y, sizeW, sizeH, aspectRatio);
    glBindTexture(GL_TEXTURE_2D, 0);
    glDisable(GL_TEXTURE_2D);
  }
  else if(type == 9){ // ville BLEUE
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, texture_map[8]);
    glColor3ub(255, 255, 255);
    drawSquare(width, height, x, y, sizeW, sizeH, aspectRatio);
    glBindTexture(GL_TEXTURE_2D, 0);
    glDisable(GL_TEXTURE_2D);
  }

  else if(type == 10){ // COULEUR TEAM ROUGE
    glColor3ub(240, 80, 56);
    drawSquare(width, height, x, y, sizeW, sizeH, aspectRatio);
  }

  else if(type == 11){ // COULEUR TEAM BLEU
    glColor3ub(96, 88, 240);
    drawSquare(width, height, x, y, sizeW, sizeH, aspectRatio);
  }

    else if(type == 12){ // COULEUR TEAM GRIS
    glColor3ub(240, 240, 240);
    drawSquare(width, height, x, y, sizeW, sizeH, aspectRatio);
  }

    else if(type == 13){ // COULEUR GRIS FONCE
    glColor3ub(150, 150, 150);
    drawSquare(width, height, x, y, sizeW, sizeH, aspectRatio);
  }

  else{
    glColor3ub(255, 255, 255);
    drawSquare(width, height, x, y, sizeW, sizeH, aspectRatio);
  }

}

void generateTexture(SDL_Surface* image, GLuint *texture_id){

    glGenTextures(1, texture_id);
    glBindTexture(GL_TEXTURE_2D, *texture_id);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    GLenum format;

    switch(image->format->BytesPerPixel) {
        case 1:
            format = GL_RED;
            break;
        case 3:
            format = GL_RGB;
            break;
        case 4:
            format = GL_RGBA;
            break;
        default:
            fprintf(stderr, "Format des pixels de l'image %s non supporte.\n");
    }
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, image->w, image->h, 0, format, GL_UNSIGNED_BYTE, image->pixels);
    glBindTexture(GL_TEXTURE_2D, 0);
}

void generateTextureTransparant(SDL_Surface* image, GLuint *texture_id){

    glGenTextures(1, texture_id);
    glBindTexture(GL_TEXTURE_2D, *texture_id);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    GLenum format;

    switch(image->format->BytesPerPixel) {
        case 1:
            format = GL_RED;
            break;
        case 3:
            format = GL_RGB;
            break;
        case 4:
            format = GL_RGBA;
            break;
        default:
            fprintf(stderr, "Format des pixels de l'image %s non supporte.\n");
    }
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image->w, image->h, 0, format, GL_UNSIGNED_BYTE, image->pixels);
    glBindTexture(GL_TEXTURE_2D, 0);
}

int ground(int red, int green, int blue){
  int res = 0;
  if(red == 17 && green == 222 && blue == 97){ // plaine
    res = 1;
  }
  else if(red == 45 && green == 151 && blue == 12){ // foret
    res = 2;
  }
  else if(red == 17 && green == 65 && blue == 222){ // Mer
    res = 3;
  }
  else if(red == 186 && green == 222 && blue == 17){ // desert
    res = 4;
  }
  else if(red == 4 && green == 4 && blue == 4){ // montagne
    res = 5;
  }
  else if(red == 125 && green == 125 && blue == 125){ // route
    res = 6;
  }
  else if(red == 196 && green == 196 && blue == 196){ // ville
    res = 7;
  }
  return res;
}

Uint32 getpixel(SDL_Surface *surface, int x, int y){
    int bpp = surface->format->BytesPerPixel;
    /* Here p is the address to the pixel we want to retrieve */
    Uint8 *p = (Uint8 *)surface->pixels + y * surface->pitch + x * bpp;

    switch(bpp) {
    case 1:
        return *p;
        break;

    case 2:
        return *(Uint16 *)p;
        break;

    case 3:
        if(SDL_BYTEORDER == SDL_BIG_ENDIAN)
            return p[0] << 16 | p[1] << 8 | p[2];
        else
            return p[0] | p[1] << 8 | p[2] << 16;
        break;

    case 4:
        return *(Uint32 *)p;
        break;

    default:
        return 0;       /* shouldn't happen, but avoids warnings */
    }
}

void mapCreator(SDL_Surface* surface, GLuint texture_map[], int nbSquare, map_square *game_map){
    char image_path[] = "doc/map.png";
    SDL_Surface* image_map = IMG_Load(image_path);
    int x, y;
    //int nbSquare = 10;
    int sizeSquare;
    int spaceBetween = 0;
    if(surface->w / nbSquare > (surface->h / nbSquare)){
        sizeSquare = (surface->h - (nbSquare-1)*spaceBetween) / nbSquare;
    }
    else{
        sizeSquare = (surface->w - (nbSquare-1)*spaceBetween) / nbSquare;
    }

    for( y = 0 ; y< image_map->h ; y++)
    {
        for (x = 0; x < image_map->w ; x++)
        {

            SDL_LockSurface(image_map); // V�rouillage de la surface
            //Uint8* pPixels = image_map->pixels; // R�cup�ration d'un pointeur vers les pixels de la surface
            Uint32 p;
            p = getpixel(image_map,x,y);
            int r, g, b; // Variables servant � stocker la couleur du pixel choisi
            r = p & 0xff;
            g = (p >> 8) & 0xff;
            b = (p >> 16) & 0xff;
            SDL_UnlockSurface(image_map); //D�v�rouillage de la surface
            int type = ground(r,g,b);
            game_map[y*nbSquare+x].cX=x;
            game_map[y*nbSquare+x].cY=y;
            game_map[y*nbSquare+x].ground=type;

        }
    }
    SDL_FreeSurface(image_map);
}

void mapDisplay(SDL_Surface* surface, GLuint texture_map[], int nbSquare, map_square *game_map, float aspectRatio){
    int spaceBetween = 0;
    int sizeSquare;
    if(surface->w / nbSquare > (surface->h / nbSquare)){
        sizeSquare = (surface->h - (nbSquare-1)*spaceBetween) / nbSquare;
    }
    else{
        sizeSquare = (surface->w - (nbSquare-1)*spaceBetween) / nbSquare;
    }
    for(int y = 0 ; y< nbSquare ; y++)
    {
        for (int x = 0; x < nbSquare ; x++)
        {
            draw_map_square(aspectRatio,
                            surface->w,
                            surface->h,
                            ((sizeSquare/2)+x*(sizeSquare)+(x*spaceBetween)),
                            ((sizeSquare/2)+y*(sizeSquare)+(y*spaceBetween)),
                            sizeSquare,
                            sizeSquare,
                            game_map[y*nbSquare+x].ground,
                            texture_map);
        }
    }
}

void squareInfo(map_square *game_map, int x, int y, int nbSquare, SDL_Surface* surface){
    int sizeSquare;
    int spaceBetween = 0;
    if(surface->w / nbSquare > (surface->h / nbSquare)){
        sizeSquare = (surface->h - (nbSquare-1)*spaceBetween) / nbSquare;
    }
    else{
        sizeSquare = (surface->w - (nbSquare-1)*spaceBetween) / nbSquare;
    }
    int cX, cY;
    cX = x/(sizeSquare);
    printf("INFO CASE \n", cX);
    printf("valeur de cX : %d \n", cX);
    cY = y/(sizeSquare);
    printf("valeur de cY : %d \n", cY);
    printf("valeur de la case %d : cX = %d | cY = %d | type = %d \n", cX*nbSquare+cX, game_map[cY*nbSquare+cX].cX, game_map[cY*nbSquare+cX].cY, game_map[cY*nbSquare+cX].ground );
    if(game_map[cY*nbSquare+cX].type_soldier == 0){
        printf("pas d'unit�s pr�sentes sur cette case ! \n");
    }
    else{
        printf("unite player : %d \n", game_map[cY*nbSquare+cX].player);
        printf("unite type : %d \n", game_map[cY*nbSquare+cX].type_soldier);
        printf("unite life: %d \n", game_map[cY*nbSquare+cX].life);
        printf("unite strength : %d \n", game_map[cY*nbSquare+cX].strength);
        printf("unite dexterity : %d \n", game_map[cY*nbSquare+cX].dexterity);
        printf("unite range : %d \n", game_map[cY*nbSquare+cX].range);
        printf("unite fuel : %d \n", game_map[cY*nbSquare+cX].fuel);
        printf("unite action : %d \n", game_map[cY*nbSquare+cX].action);
    }
    printf(" \n\n");
}

int squareFree(map_square *game_map, int x, int y, int nbSquare, SDL_Surface* surface, int typeUnit){
    int sizeSquare;
    int spaceBetween = 0;
    if(surface->w / nbSquare > (surface->h / nbSquare)){
        sizeSquare = (surface->h - (nbSquare-1)*spaceBetween) / nbSquare;
    }
    else{
        sizeSquare = (surface->w - (nbSquare-1)*spaceBetween) / nbSquare;
    }
    int cX, cY;
    cX = x/(sizeSquare);
    cY = y/(sizeSquare);
    if(game_map[cY*nbSquare+cX].type_soldier == 0){
        printf("case libre \n");
        if(game_map[cY*nbSquare+cX].ground == 5){
            return 0;
        }
        if(game_map[cY*nbSquare+cX].ground == 3){

            printf("c'est la mer tu es %d \n", typeUnit);
            if (typeUnit == 5 || typeUnit == 4){
                printf("tu es un bateau \n");
                return 1;
            }
            else{
                    printf("tu n'es pas un bateau \n");
                return 0;
            }
        }
        else{
            if(typeUnit == 5){
                return 0;
            }
            else{
                return 1;
            }
        }
    }
    else{
        return 0;
    }
}


void nullMap( int nbSquare, map_square *game_map){
    for(int y = 0 ; y< nbSquare ; y++)
    {
        for (int x = 0; x < nbSquare ; x++)
        {
            game_map[y*nbSquare+x].type_soldier = 0;
            game_map[y*nbSquare+x].player = 0;
            game_map[y*nbSquare+x].life = 0;
            game_map[y*nbSquare+x].strength = 0;
            game_map[y*nbSquare+x].dexterity = 0;
            game_map[y*nbSquare+x].range = 0;
            game_map[y*nbSquare+x].fuel = 0;

        }
    }
}

void nullRange( int nbSquare, map_range *game_range){
    for(int y = 0 ; y< nbSquare ; y++)
    {
        for (int x = 0; x < nbSquare ; x++)
        {
            game_range[y*nbSquare+x].x = -1;
            game_range[y*nbSquare+x].y = -1;
        }
    }
}

void displayRangeMap(SDL_Surface* surface, int nbSquare, map_range *game_range, float aspectRatio){
    int spaceBetween = 0;
    int sizeSquare;
    if(surface->w / nbSquare > (surface->h / nbSquare)){
        sizeSquare = (surface->h - (nbSquare-1)*spaceBetween) / nbSquare;
    }
    else{
        sizeSquare = (surface->w - (nbSquare-1)*spaceBetween) / nbSquare;
    }
    for(int y = 0 ; y< nbSquare ; y++)
    {
        for (int x = 0; x < nbSquare ; x++)
        {
            if(game_range[ y * nbSquare + x].x != -1 && game_range[ y * nbSquare + x].y != -1){
                glEnable(GL_BLEND);
                glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
                glColor4ub(240,50,0,150);
                drawSquare(surface->w, surface->h, ((sizeSquare/2)+x*(sizeSquare)+(x*spaceBetween)), ((sizeSquare/2)+y*(sizeSquare)+(y*spaceBetween)), sizeSquare, sizeSquare, aspectRatio);
                glDisable(GL_BLEND);
            }
        }
    }
}

void displayDeplacementMap(SDL_Surface* surface, int nbSquare, map_range *game_range, float aspectRatio){
    int spaceBetween = 0;
    int sizeSquare;
    if(surface->w / nbSquare > (surface->h / nbSquare)){
        sizeSquare = (surface->h - (nbSquare-1)*spaceBetween) / nbSquare;
    }
    else{
        sizeSquare = (surface->w - (nbSquare-1)*spaceBetween) / nbSquare;
    }
    for(int y = 0 ; y< nbSquare ; y++)
    {
        for (int x = 0; x < nbSquare ; x++)
        {
            if(game_range[ y * nbSquare + x].x != -1 && game_range[ y * nbSquare + x].y != -1){
                glEnable(GL_BLEND);
                glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
                glColor4ub(200,200,0,150);
                drawSquare(surface->w, surface->h, ((sizeSquare/2)+x*(sizeSquare)+(x*spaceBetween)), ((sizeSquare/2)+y*(sizeSquare)+(y*spaceBetween)), sizeSquare, sizeSquare, aspectRatio);
                glDisable(GL_BLEND);
            }
        }
    }
}






