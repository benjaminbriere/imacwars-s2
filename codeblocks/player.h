/////////////////////////////////////////////////
//                 PLAYER.H                    //
//                                             //
//     by Gartner Esther & Briere Benjamin     //
//                                             //
/////////////////////////////////////////////////

void playerCreator(player *game_player, map_square *game_map, int nbSquare);
void playerUnit(player *game_player, int pl);

void playerPower(SDL_Surface* surface, map_square *game_map, player *p,int player, int nbSquare, float aspectRatio, int x, int y);
