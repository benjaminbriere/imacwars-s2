
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "map.cpp"

/* Dimensions initiales et titre de la fenetre */
static const unsigned int WINDOW_WIDTH = 800;
static const unsigned int WINDOW_HEIGHT = 600;
static const char WINDOW_TITLE[] = "IMAC WARS V2 ";
static float aspectRatio;

/* Espace fenetre virtuelle */
static const float GL_VIEW_SIZE = 2.;

/* Nombre de bits par pixel de la fenetre */
static const unsigned int BIT_PER_PIXEL = 32;

/* Nombre minimal de millisecondes separant le rendu de deux images */
static const Uint32 FRAMERATE_MILLISECONDS = 10000 / 60;

void reshape(SDL_Surface** surface, unsigned int width, unsigned int height){
    printf("echo 1 \n");
    SDL_Surface* surface_temp = SDL_SetVideoMode(
        width, height, BIT_PER_PIXEL,
        SDL_OPENGL | SDL_GL_DOUBLEBUFFER | SDL_RESIZABLE);
    printf("echo 2 \n");
    if(NULL == surface_temp)
    {
        fprintf(
            stderr,
            "Erreur lors du redimensionnement de la fenetre.\n");
        exit(EXIT_FAILURE);
    }
    printf("echo 3 \n");
    *surface = surface_temp;
    printf("echo 4 \n");
    aspectRatio = width / (float) height;
    printf("echo 5 \n");
    glViewport(0, 0, (*surface)->w, (*surface)->h);
    printf("echo 6 \n");
    glMatrixMode(GL_PROJECTION);
    printf("echo 7 \n");
    glLoadIdentity();
    printf("echo 8 \n");
    if( aspectRatio > 1)
    {
        printf("echo 8-1 \n");
        gluOrtho2D(
        -GL_VIEW_SIZE / 2. * aspectRatio, GL_VIEW_SIZE / 2. * aspectRatio,
        -GL_VIEW_SIZE / 2., GL_VIEW_SIZE / 2.);
        printf("echo 8-2 \n");
    }
    //printf("echo 9 \n");
    else
    {
        printf("echo 9-1 \n");
        gluOrtho2D(
        -GL_VIEW_SIZE / 2., GL_VIEW_SIZE / 2.,
        -GL_VIEW_SIZE / 2. / aspectRatio, GL_VIEW_SIZE / 2. / aspectRatio);
        printf("echo 9-2 \n");
    }
}

void drawOrigin(int width, int height, int x, int y, int ratio){

  glColor3ub(255, 0, 0);
  glBegin(GL_LINE_STRIP);
  if (aspectRatio > 1){
    glVertex2f(
      (( -1 + 2. * x / (float) width ) * aspectRatio ),
      -(-1 + 2. * y / (float) height));
    glVertex2f(
      (( -1 + 2. * (x+1*ratio) / (float) width ) * aspectRatio ),
      -(-1 + 2. * y / (float) height));
  }
  if (aspectRatio  == 1){
    glVertex2f(
      (-1 + 2. * x / (float) width ),
      -(-1 + 2. * y / (float) height));
    glVertex2f(
      (( -1 + 2. * (x+1*ratio) / (float) width )),
      -(-1 + 2. * y / (float) height));
  }
  if (aspectRatio  < 1){
    glVertex2f(
      ( -1 + 2. * x / (float) width ),
      (-(-1 + 2. * y / (float) height)) / aspectRatio );
    glVertex2f(
      ( -1 + 2. * (x+1*ratio) / (float) width ),
      (-(-1 + 2. * y / (float) height)) / aspectRatio );
  }
  glEnd();
  glColor3ub(0, 255, 0);
  glBegin(GL_LINE_STRIP);
  if (aspectRatio > 1){
    glVertex2f(
      (( -1 + 2. * x / (float) width ) * aspectRatio ),
      -(-1 + 2. * y / (float) height));
    glVertex2f(
      (( -1 + 2. * x / (float) width ) * aspectRatio ),
      -(-1 + 2. * (y-1*ratio) / (float) height));
  }
  if (aspectRatio  == 1){
    glVertex2f(
      (-1 + 2. * x / (float) width ),
      -(-1 + 2. * y / (float) height));
    glVertex2f(
      (( -1 + 2. * x / (float) width )),
      -(-1 + 2. * (y-1*ratio) / (float) height));
  }
  if (aspectRatio  < 1){
    glVertex2f(
      ( -1 + 2. * x / (float) width ),
      (-(-1 + 2. * y / (float) height)) / aspectRatio );
    glVertex2f(
      ( -1 + 2. * x / (float) width ),
      (-(-1 + 2. * (y-1*ratio) / (float) height)) / aspectRatio );
  }
  glEnd();

}

void drawSquare(int width, int height, int x, int y, int sizeW, int sizeH){
    //int r1 = rand() % 255;
  //glColor3ub(0, 255, 0);
  glBegin(GL_QUADS);
    if (aspectRatio > 1){
      glTexCoord2f(0, 0);
      glVertex2f(
        (( -1 + 2. * (x-1*sizeW/2) / (float) width )* aspectRatio),
        -(-1 + 2. * (y+1*sizeH/2) / (float) height));
      glTexCoord2f(1, 0);
      glVertex2f(
        (( -1 + 2. * (x+1*sizeW/2) / (float) width) * aspectRatio),
        -(-1 + 2. * (y+1*sizeH/2) / (float) height));
      glTexCoord2f(1, 1);
      glVertex2f(
        (( -1 + 2. * (x+1*sizeW/2) / (float) width) * aspectRatio),
        -(-1 + 2. * (y-1*sizeH/2) / (float) height));
      glTexCoord2f(0, 1);
      glVertex2f(
        (( -1 + 2. * (x-1*sizeW/2) / (float) width) * aspectRatio),
        -(-1 + 2. * (y-1*sizeH/2) / (float) height));
    }
    if (aspectRatio  == 1){
      glTexCoord2f(0, 0);
      glVertex2f(
        (( -1 + 2. * (x-1*sizeW/2) / (float) width )),
        -(-1 + 2. * (y+1*sizeH/2) / (float) height));
      glTexCoord2f(1, 0);
      glVertex2f(
        (( -1 + 2. * (x+1*sizeW/2) / (float) width )),
        -(-1 + 2. * (y+1*sizeH/2) / (float) height));
      glTexCoord2f(1, 1);
      glVertex2f(
        (( -1 + 2. * (x+1*sizeW/2) / (float) width )),
        -(-1 + 2. * (y-1*sizeH/2) / (float) height));
      glTexCoord2f(0, 1);
      glVertex2f(
        (( -1 + 2. * (x-1*sizeW/2) / (float) width )),
        -(-1 + 2. * (y-1*sizeH/2) / (float) height));
    }
    if (aspectRatio  < 1){
      glTexCoord2f(0, 0);
      glVertex2f(
        (( -1 + 2. * (x-1*sizeW/2) / (float) width )),
        -(-1 + 2. * (y+1*sizeH/2) / (float) height) / aspectRatio );
      glTexCoord2f(1, 0);
      glVertex2f(
        (( -1 + 2. * (x+1*sizeW/2) / (float) width )),
        -(-1 + 2. * (y+1*sizeH/2) / (float) height) / aspectRatio );
      glTexCoord2f(1, 1);
      glVertex2f(
        (( -1 + 2. * (x+1*sizeW/2) / (float) width )),
        -(-1 + 2. * (y-1*sizeH/2) / (float) height) / aspectRatio );
      glTexCoord2f(0, 1);
      glVertex2f(
        (( -1 + 2. * (x-1*sizeW/2) / (float) width )),
        -(-1 + 2. * (y-1*sizeH/2) / (float) height) / aspectRatio );
    }
  glEnd();
}

void draw_map_square(float aspectRatio, int width, int height, int x, int y, int sizeW, int sizeH,
                     int type, GLuint texture_map[]){
    //int r1 = rand() % 255;

  //printf(" Case %d / %d : red : %u  | green : %u  | blue : %u \n", x, y, red, green, blue);

  //printf(" Case tab[1] | int : %d / Glint8 : %u \n", texture_map[1], texture_map[1]);

  if(type == 1){ // plaine
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, texture_map[0]);
    glColor3ub(255, 255, 255);
    drawSquare(width, height, x, y, sizeW, sizeH);
    glBindTexture(GL_TEXTURE_2D, 0);
    glDisable(GL_TEXTURE_2D);
  }
  else if(type == 2){ // foret
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, texture_map[1]);
    glColor3ub(255, 255, 255);
    drawSquare(width, height, x, y, sizeW, sizeH);
    glBindTexture(GL_TEXTURE_2D, 0);
    glDisable(GL_TEXTURE_2D);
  }
  else if(type == 3){ // Mer
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, texture_map[2]);
    glColor3ub(255, 255, 255);
    drawSquare(width, height, x, y, sizeW, sizeH);
    glBindTexture(GL_TEXTURE_2D, 0);
    glDisable(GL_TEXTURE_2D);
  }
  else if(type == 4){ // desert
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, texture_map[3]);
    glColor3ub(255, 255, 255);
    drawSquare(width, height, x, y, sizeW, sizeH);
    glBindTexture(GL_TEXTURE_2D, 0);
    glDisable(GL_TEXTURE_2D);
  }
  else if(type == 5){ // montagne
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, texture_map[4]);
    glColor3ub(255, 255, 255);
    drawSquare(width, height, x, y, sizeW, sizeH);
    glBindTexture(GL_TEXTURE_2D, 0);
    glDisable(GL_TEXTURE_2D);
  }
  else if(type == 6){ // route
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, texture_map[5]);
    glColor3ub(255, 255, 255);
    drawSquare(width, height, x, y, sizeW, sizeH);
    glBindTexture(GL_TEXTURE_2D, 0);
    glDisable(GL_TEXTURE_2D);
  }
  else if(type == 7){ // ville
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, texture_map[6]);
    glColor3ub(255, 255, 255);
    drawSquare(width, height, x, y, sizeW, sizeH);
    glBindTexture(GL_TEXTURE_2D, 0);
    glDisable(GL_TEXTURE_2D);
  }
  else{
    glColor3ub(255, 255, 255);
    drawSquare(width, height, x, y, sizeW, sizeH);
  }

}

void generateTexture(SDL_Surface* image, GLuint *texture_id){

    glGenTextures(1, texture_id);
    glBindTexture(GL_TEXTURE_2D, *texture_id);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    GLenum format;

    switch(image->format->BytesPerPixel) {
        case 1:
            format = GL_RED;
            break;
        case 3:
            format = GL_RGB;
            break;
        case 4:
            format = GL_RGBA;
            break;
        default:
            fprintf(stderr, "Format des pixels de l'image %s non supporte.\n");
    }
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, image->w, image->h, 0, format, GL_UNSIGNED_BYTE, image->pixels);
    glBindTexture(GL_TEXTURE_2D, 0);
}

int ground(int red, int green, int blue){
  int res = 0;
  if(red == 17 && green == 222 && blue == 97){ // plaine
    res = 1;
  }
  else if(red == 45 && green == 151 && blue == 12){ // foret
    res = 2;
  }
  else if(red == 17 && green == 65 && blue == 222){ // Mer
    res = 3;
  }
  else if(red == 186 && green == 222 && blue == 17){ // desert
    res = 4;
  }
  else if(red == 4 && green == 4 && blue == 4){ // montagne
    res = 5;
  }
  else if(red == 125 && green == 125 && blue == 125){ // route
    res = 6;
  }
  else if(red == 196 && green == 196 && blue == 196){ // ville
    res = 7;
  }
  return res;
}

Uint32 getpixel(SDL_Surface *surface, int x, int y){
    int bpp = surface->format->BytesPerPixel;
    /* Here p is the address to the pixel we want to retrieve */
    Uint8 *p = (Uint8 *)surface->pixels + y * surface->pitch + x * bpp;

    switch(bpp) {
    case 1:
        return *p;
        break;

    case 2:
        return *(Uint16 *)p;
        break;

    case 3:
        if(SDL_BYTEORDER == SDL_BIG_ENDIAN)
            return p[0] << 16 | p[1] << 8 | p[2];
        else
            return p[0] | p[1] << 8 | p[2] << 16;
        break;

    case 4:
        return *(Uint32 *)p;
        break;

    default:
        return 0;       /* shouldn't happen, but avoids warnings */
    }
}

void mapCreator(SDL_Surface* surface, GLuint texture_map[], int nbSquare, map_square *game_map){
    char image_path[] = "doc/map.png";
    SDL_Surface* image_map = IMG_Load(image_path);
    int x, y;
    //int nbSquare = 10;
    int sizeSquare;
    int spaceBetween = 0;
    if(surface->w / nbSquare > (surface->h / nbSquare)){
        sizeSquare = (surface->h - (nbSquare-1)*spaceBetween) / nbSquare;
    }
    else{
        sizeSquare = (surface->w - (nbSquare-1)*spaceBetween) / nbSquare;
    }

    for( y = 0 ; y< image_map->h ; y++)
    {
        for (x = 0; x < image_map->w ; x++)
        {

            SDL_LockSurface(image_map); // V�rouillage de la surface
            //Uint8* pPixels = image_map->pixels; // R�cup�ration d'un pointeur vers les pixels de la surface
            Uint32 p;
            p = getpixel(image_map,x,y);
            int r, g, b; // Variables servant � stocker la couleur du pixel choisi
            r = p & 0xff;
            g = (p >> 8) & 0xff;
            b = (p >> 16) & 0xff;
            SDL_UnlockSurface(image_map); //D�v�rouillage de la surface
            int type = ground(r,g,b);
            game_map[y*nbSquare+x].cX=x;
            game_map[y*nbSquare+x].cY=y;
            game_map[y*nbSquare+x].ground=type;
            draw_map_square(aspectRatio, surface->w, surface->h,((sizeSquare/2)+x*(sizeSquare)+(x*spaceBetween)),
                                              ((sizeSquare/2)+y*(sizeSquare)+(y*spaceBetween)),
                                              sizeSquare, sizeSquare, type, texture_map);
        }
    }
    SDL_FreeSurface(image_map);
}

void squareInfo(map_square *game_map, int x, int y, int nbSquare, SDL_Surface* surface){
    int sizeSquare;
    int spaceBetween = 0;
    if(surface->w / nbSquare > (surface->h / nbSquare)){
        sizeSquare = (surface->h - (nbSquare-1)*spaceBetween) / nbSquare;
    }
    else{
        sizeSquare = (surface->w - (nbSquare-1)*spaceBetween) / nbSquare;
    }
    int cX, cY;
    cX = x/(sizeSquare);
    printf("valeur de cX : %d \n", cX);
    cY = y/(sizeSquare);
    printf("valeur de cY : %d \n", cY);
    printf("valeur de la case %d : cX = %d | cY = %d | type = %d \n", cX*nbSquare+cX, game_map[cY*nbSquare+cX].cX, game_map[cY*nbSquare+cX].cY, game_map[cY*nbSquare+cX].ground );
}

int main(int argc, char** argv)
{
    Uint32 startTime = SDL_GetTicks();
    /* Initialisation de la SDL */
    if(-1 == SDL_Init(SDL_INIT_VIDEO))
    {
        fprintf(
            stderr,
            "Impossible d'initialiser la SDL. Fin du programme.\n");
        return EXIT_FAILURE;
    }

    /* Ouverture d'une fenetre et creation d'un contexte OpenGL */
    SDL_Surface* surface;
    reshape(&surface, WINDOW_WIDTH, WINDOW_HEIGHT);

    /* Initialisation du titre de la fenetre */
    SDL_WM_SetCaption(WINDOW_TITLE, NULL);

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    GLuint plaine;
    SDL_Surface* image = IMG_Load("doc/gazon.jpg");
    generateTexture(image, &plaine);

    GLuint mer;
    image = IMG_Load("doc/mer.jpg");
    generateTexture(image, &mer);

    GLuint desert;
    image = IMG_Load("doc/sable.jpg");
    generateTexture(image, &desert);

    GLuint foret;
    image = IMG_Load("doc/foret.jpg");
    generateTexture(image, &foret);

    GLuint ville;
    image = IMG_Load("doc/ville.jpg");
    generateTexture(image, &ville);

    GLuint montagne;
    image = IMG_Load("doc/montagne.jpg");
    generateTexture(image, &montagne);

    GLuint route;
    image = IMG_Load("doc/route.jpg");
    generateTexture(image, &route);


    GLuint texture_map[7];
    texture_map[0] = plaine;
    texture_map[1] = foret;
    texture_map[2] = mer;
    texture_map[3] = desert;
    texture_map[4] = montagne;
    texture_map[5] = route;
    texture_map[6] = ville;
    printf(" Case mer | int : %d / Glint8 : %u \n", mer, mer);
    printf(" Case tab[1] | int : %d / Glint8 : %u \n", texture_map[1], texture_map[1]);

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /* Boucle principale */
    int loop = 1;
    int lancement = 0;
    while(loop)
    {
        /* Recuperation du temps au debut de la boucle */
        Uint32 startTime = SDL_GetTicks();

        /* Placer ici le code de dessin */
        glClear(GL_COLOR_BUFFER_BIT);
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();

        int nbSquare = 10;
        map_square *game_map = NULL;
        game_map = (map_square *) malloc( nbSquare * nbSquare * sizeof(map_square));

        mapCreator(surface, texture_map, nbSquare, &game_map[0]);

        /* Echange du front et du back buffer : mise a jour de la fenetre */
        SDL_GL_SwapBuffers();

        /* Boucle traitant les evenements */
        SDL_Event e;


        while(SDL_PollEvent(&e))
        {
            /* L'utilisateur ferme la fenetre : */
			if(e.type == SDL_QUIT)
			{

				loop = 0;
				break;
			}

			if(	e.type == SDL_KEYDOWN
				&& (e.key.keysym.sym == SDLK_q || e.key.keysym.sym == SDLK_ESCAPE))
			{

				loop = 0;
				break;
			}

            /* Quelques exemples de traitement d'evenements : */
            switch(e.type)
            {
                /* Redimensionnement fenetre */
				//*/
				/*
				case SDL_VIDEORESIZE:
				    {
                    printf("echo");
                    reshape(&surface, e.resize.w, e.resize.h);
                    printf("sortie \n");
                    break;
				    }
				*/
				case SDL_VIDEORESIZE:
				    {

                    int newWidth = e.resize.w;
				    int newHeight = e.resize.h;
				    if(newWidth != surface->w && newHeight != surface->h){

                        reshape(&surface, newWidth, newHeight);
				    }
                    break;
				    }



                //*/
                /* Clic souris */
                case SDL_MOUSEBUTTONUP:
                    printf("clic en (%d, %d)\n", e.button.x, e.button.y);
                    squareInfo(&game_map[0],e.button.x,e.button.y,nbSquare, surface);
                    break;

                /* Touche clavier */
                case SDL_KEYDOWN:
                    printf("echo 13");
                    printf("touche pressee (code = %d)\n", e.key.keysym.sym);
                    break;

                default:
                    break;
            }
        }


        Uint32 elapsedTime = SDL_GetTicks() - startTime;
        /* Si trop peu de temps s'est ecoule, on met en pause le programme */
        if(elapsedTime < FRAMERATE_MILLISECONDS)
        {
            SDL_Delay(FRAMERATE_MILLISECONDS - elapsedTime);
        }
    }

    //glDeleteTextures(1, texture_id);
    //glDeleteTextures(1, &mer);
    glDeleteTextures(1, texture_map);
    /* Liberation de la m�moire occupee par img */
    SDL_FreeSurface(image);
    //SDL_FreeSurface(image1);
    /* Liberation des ressources associees a la SDL */
    SDL_Quit();

    return EXIT_SUCCESS;
}
