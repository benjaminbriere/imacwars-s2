/////////////////////////////////////////////////
//                   aSTAR.H                   //
//                                             //
//     by Gartner Esther & Briere Benjamin     //
//                                             //
/////////////////////////////////////////////////

void displayObstacle(SDL_Surface* surface, int nbSquare, map_square *game_map, float aspectRatio);
void enfiler(file *file, int x, int y, int g, int h, int f, int parentX, int parentY);
void displayOpenList(SDL_Surface* surface, file *file, int nbSquare, map_square *game_map, float aspectRatio);
void displayClosedList(SDL_Surface* surface, file *file, int nbSquare, map_square *game_map, float aspectRatio);
float mapPath(SDL_Surface* surface, int nbSquare, map_square *game_map, float aspectRatio, int startX, int startY,int clicX, int clicY);
vector < pair<int,int> > vectorMapPath(SDL_Surface* surface, int nbSquare, map_square *game_map, float aspectRatio, int startX, int startY, int clicX, int clicY);


void retirerFile(file *file, int x, int y);
void neighbors(node *current, map_square *game_map, int nbSquare, int type);
int rechercherFile(file *file, int x, int y);
int heuristic(node *current, node *endPath);
void parents( node *enfant, node *p);


void enfilerNode(file *file, node *node);
void lectureFile(file *file);

void backFile(file *file, int *x, int *y);

void ajouterArbre(tree_node *arbre, int x, int y, int g, int h, int f, int parentX, int parentY);

int rechercherArbre(tree_node *tree, int x, int y);
