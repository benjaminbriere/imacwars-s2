/////////////////////////////////////////////////
//                    GAME.H                   //
//                                             //
//     by Gartner Esther & Briere Benjamin     //
//                                             //
/////////////////////////////////////////////////

int game(SDL_Surface* surface,
          int nbSquare,
          map_square *game_map,
          player *game_player,
          float aspectRatio,
          GLuint texture_map[],
          GLuint texture_soldier[],
          GLuint texture_figure[],
          GLuint texture_button[],
          GLuint texture_stat[],
          GLuint texture_cursor[],
          GLuint texture_menu[]);
