#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
//#include <SDL/SDL_ttf.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

#include "structures.h"
#include "map.h"
#include "unit.h"
#include "menu.h"
#include "player.h"
#include "game.h"
#include "aStar.h"
#include "ia.h"

#define MAXUNITE 10
#define NBUNITE 5


void computerTurnPurchase(SDL_Surface* surface, map_square *game_map, player *p,int player, int nbSquare, float aspectRatio, int *fin, int sizeSquare){

    int adversaire;
    if(player == 1){
        adversaire = 0;
    }
    else{
        adversaire = 1;
    }

    int nbFantassin = 0;
    int nbBazooka = 0;
    int nbTank = 0;
    int nbCopter = 0;
    int nbShip = 0;

    int nbAdvFantassin = 0;
    int nbAdvBazooka = 0;
    int nbAdvTank = 0;
    int nbAdvCopter = 0;
    int nbAdvShip = 0;

    for (int i = 0; i<MAXUNITE; i++){

        // ORDINATEUR

        if( (p[player].liste[i].type_soldier-5*player) == 1 ){
            nbFantassin++;
        }
        if( (p[player].liste[i].type_soldier-5*player) == 2 ){
            nbBazooka++;
        }
        if( (p[player].liste[i].type_soldier-5*player) == 3 ){
            nbTank++;
        }
        if( (p[player].liste[i].type_soldier-5*player) == 4 ){
            nbCopter++;
        }
        if( (p[player].liste[i].type_soldier-5*player) == 5 ){
            nbShip++;
        }

        // ADVERSAIRE

        if( (p[adversaire].liste[i].type_soldier-5*adversaire) == 1 ){
            nbAdvFantassin++;
        }
        if( (p[adversaire].liste[i].type_soldier-5*adversaire) == 2 ){
            nbAdvBazooka++;
        }
        if( (p[adversaire].liste[i].type_soldier-5*adversaire) == 3 ){
            nbAdvTank++;
        }
        if( (p[adversaire].liste[i].type_soldier-5*adversaire) == 4 ){
            nbAdvCopter++;
        }
        if( (p[adversaire].liste[i].type_soldier-5*adversaire) == 5 ){
            nbAdvShip++;
        }
    }
    printf(" ======== IA =================== \n\n",player+1);
    printf("[PLAYER %d] FANTASSIN  : %d  \n",player+1,nbFantassin);
    printf("[PLAYER %d] BAZOOKA    : %d  \n",player+1,nbBazooka);
    printf("[PLAYER %d] TANK       : %d  \n",player+1,nbTank);
    printf("[PLAYER %d] HELICOPTER : %d  \n",player+1,nbCopter);
    printf("[PLAYER %d] SHIP       : %d  \n\n",player+1,nbShip);

    printf("[PLAYER %d] FANTASSIN  : %d  \n",adversaire+1,nbAdvFantassin);
    printf("[PLAYER %d] BAZOOKA    : %d  \n",adversaire+1,nbAdvBazooka);
    printf("[PLAYER %d] TANK       : %d  \n",adversaire+1,nbAdvTank);
    printf("[PLAYER %d] HELICOPTER : %d  \n",adversaire+1,nbAdvCopter);
    printf("[PLAYER %d] SHIP       : %d  \n\n",adversaire+1,nbAdvShip);

    if(nbFantassin == 0){
        printf("[PLAYER %d] ACHETER FANTASSIN !!! \n",player+1);
        if( p[player].nbUnite < MAXUNITE){
            purchaseUnitComputer(nbSquare, surface, &game_map[0], 1, &p[0], player, sizeSquare);
            p[player].nbUnite = p[player].nbUnite +1 ;
        }
    }
    else{
        printf("[PLAYER %d] FANTASSIN SUFFISANT !!! \n",player+1);
    }

    if( p[player].gold >= 2500 && nbShip < 2){
        printf("[PLAYER %d] CONSEIL : ACHETER SHIP !!! \n",player+1);
        if( p[player].nbUnite < MAXUNITE){
            purchaseUnitComputer(nbSquare, surface, &game_map[0], 5, &p[0], player, sizeSquare);
            p[player].nbUnite = p[player].nbUnite +1 ;
        }
    }
    if(p[player].gold >= 1500){
        if(nbAdvTank + nbAdvFantassin > nbAdvBazooka){
            if( p[player].nbUnite < MAXUNITE){
                purchaseUnitComputer(nbSquare, surface, &game_map[0], 3, &p[0], player, sizeSquare);
                p[player].nbUnite = p[player].nbUnite +1 ;
            }
        }
        else if(nbAdvBazooka + nbAdvFantassin > nbAdvTank) {
            printf("[PLAYER %d] CONSEIL : ACHETER HELICOPTER !!! \n",player+1);
            if( p[player].nbUnite < MAXUNITE){
                purchaseUnitComputer(nbSquare, surface, &game_map[0], 4, &p[0], player, sizeSquare);
                p[player].nbUnite = p[player].nbUnite +1 ;
            }
        }
        else if(nbAdvTank >= nbBazooka){
            printf("[PLAYER %d] CONSEIL : ACHETER BAZOOKA !!! \n",player+1);
            if( p[player].nbUnite < MAXUNITE){
                purchaseUnitComputer(nbSquare, surface, &game_map[0], 2, &p[0], player, sizeSquare);
                p[player].nbUnite = p[player].nbUnite +1 ;
            }
        }

    }
    else if(p[player].gold >= 1000){
        if(nbAdvTank >= nbBazooka){
            printf("[PLAYER %d] CONSEIL : ACHETER BAZOOKA !!! \n",player+1);
            if( p[player].nbUnite < MAXUNITE){
                purchaseUnitComputer(nbSquare, surface, &game_map[0], 2, &p[0], player, sizeSquare);
                p[player].nbUnite = p[player].nbUnite +1 ;
            }
        }
    }
    else if(p[player].gold >= 500){
        if(nbAdvBazooka >= nbFantassin + nbCopter){
            printf("[PLAYER %d] CONSEIL : ACHETER FANTASSIN !!! \n",player+1);
            if( p[player].nbUnite < MAXUNITE){
                purchaseUnitComputer(nbSquare, surface, &game_map[0], 1, &p[0], player, sizeSquare);
                p[player].nbUnite = p[player].nbUnite +1 ;
            }
        }
    }
    else {
        printf("[PLAYER %d] CONSEIL : ATTENTE !!! \n",player+1);
    }
}

void purchaseUnitComputer(int nbSquare, SDL_Surface* surface, map_square *game_map, int type, player *p, int player, int sizeSquare){

    int finded = 0;
    int i,j;
    while(finded == 0){
        i = (rand() % (nbSquare/2)) + nbSquare/2;
        j = (rand() % (nbSquare));
        //printf("finded %d : i %d et j %d \n",finded,i,j);

        if (type == 4){
            if( game_map[j*nbSquare+i].ground != 5){
                if( game_map[j*nbSquare+i].player == 0 ){
                    printf("case disponible helicopter: %d %d \n", i, j);
                    finded = 1;
                    soldierCreator(i*sizeSquare, j*sizeSquare, nbSquare, surface, &game_map[0], type, &p[0], player);
                }
            }
        }
        if (type == 5){
            if( game_map[j*nbSquare+i].ground == 3 ){
                if( game_map[j*nbSquare+i].player == 0 ){
                    printf("case disponible bateau: %d %d \n", i, j);
                    finded = 1;
                    soldierCreator(i*sizeSquare, j*sizeSquare, nbSquare, surface, &game_map[0], type, &p[0], player);
                }
            }
        }
        else if(type != 5 && type !=4){
            if(game_map[j*nbSquare+i].ground != 5 && game_map[j*nbSquare+i].ground != 3 && game_map[j*nbSquare+i].player == 0){
                printf("case disponible autre: %d %d \n", i, j);
                finded = 1;
                soldierCreator(i*sizeSquare, j*sizeSquare, nbSquare, surface, &game_map[0], type, &p[0], player);
            }
        }

    }
    printf("finded %d \n",finded);
    printf("--------------\n");

}


// /!\ PLUS UTILISE CAR LE CAS OU UNITE == FANTASSIN A ETE SCINDE DANS UNE AUTRE FONCTION
void computerTurn(SDL_Surface* surface, map_square *game_map, player *p,int player, int nbSquare, float aspectRatio, int *fin){

    int adversaire;
    if(player == 1){
        adversaire = 0;
    }
    else{
        adversaire = 1;
    }

    int nbFantassin = 0;
    int nbBazooka = 0;
    int nbTank = 0;
    int nbCopter = 0;
    int nbShip = 0;

    int nbAdvFantassin = 0;
    int nbAdvBazooka = 0;
    int nbAdvTank = 0;
    int nbAdvCopter = 0;
    int nbAdvShip = 0;

    for (int i = 0; i<MAXUNITE; i++){

        // ORDINATEUR
        if( (p[player].liste[i].type_soldier-5*player) == 1 ){
            nbFantassin++;
        }
        if( (p[player].liste[i].type_soldier-5*player) == 2 ){
            nbBazooka++;
        }
        if( (p[player].liste[i].type_soldier-5*player) == 3 ){
            nbTank++;
        }
        if( (p[player].liste[i].type_soldier-5*player) == 4 ){
            nbCopter++;
        }
        if( (p[player].liste[i].type_soldier-5*player) == 5 ){
            nbShip++;
        }

        // ADVERSAIRE
        if( (p[adversaire].liste[i].type_soldier-5*adversaire) == 1 ){
            nbAdvFantassin++;
        }
        if( (p[adversaire].liste[i].type_soldier-5*adversaire) == 2 ){
            nbAdvBazooka++;
        }
        if( (p[adversaire].liste[i].type_soldier-5*adversaire) == 3 ){
            nbAdvTank++;
        }
        if( (p[adversaire].liste[i].type_soldier-5*adversaire) == 4 ){
            nbAdvCopter++;
        }
        if( (p[adversaire].liste[i].type_soldier-5*adversaire) == 5 ){
            nbAdvShip++;
        }
    }

    // GESTION MOUVEMENT


    for (int i = 0; i<MAXUNITE; i++){

        if( (p[player].liste[i].type_soldier-5*player) == 1 ){
            nbFantassin++;
        }
        if( (p[player].liste[i].type_soldier-5*player) == 2 ){
            nbBazooka++;
        }
        if( (p[player].liste[i].type_soldier-5*player) == 3 ){
            nbTank++;
        }
        if( (p[player].liste[i].type_soldier-5*player) == 4 ){
            nbCopter++;
        }
        if( (p[player].liste[i].type_soldier-5*player) == 5 ){
            nbShip++;
        }
    }

    vector < villeIA > listeVilleCapturable;
    int nbDeVilleCapturable = 0;
    for(int i = 0; i<nbSquare*nbSquare; i++){
        if(game_map[i].ground == 7){
            villeIA temp;
            temp.x = game_map[i].cX;
            temp.y = game_map[i].cY;
            temp.etat = 0;
            temp.cible = 0;vector < pair<int,int> > listeFantassin;
            listeVilleCapturable.push_back(temp);
            nbDeVilleCapturable++;
        }
        if(game_map[i].ground == 8){
            villeIA temp;
            temp.x = game_map[i].cX;
            temp.y = game_map[i].cY;
            temp.etat = 1;
            temp.cible = 0;
            listeVilleCapturable.push_back(temp);
            nbDeVilleCapturable++;
        }
    }

    for (int i = 0; i< listeVilleCapturable.size() ; i++){
        printf("information ville n�%d : x = %d | y =  %d | etat = %d | cible = %d \n", i, listeVilleCapturable[i].x, listeVilleCapturable[i].y, listeVilleCapturable[i].etat, listeVilleCapturable[i].cible);
    }


    if(nbFantassin > 0){
        for (int k = 0; k<MAXUNITE; k++){
            if( (p[player].liste[k].type_soldier-5*player) == 1 ){
                if( p[player].liste[k].action >= 1 ){
                    if ( p[player].liste[k].ground != 7 && p[player].liste[k].ground != (9-player)){
                        if(nbDeVilleCapturable > 0){
                            // VILLE LA PLUS PROCHE
                            villeIA temp;
                            temp.cible = -1;
                            int distance;

                            for (int i = 0 ; i<listeVilleCapturable.size() ; i++){
                                if(listeVilleCapturable[i].etat == 0){
                                    if(temp.cible == -1){
                                        distance = mapPath(surface, nbSquare, &game_map[0], aspectRatio, p[player].liste[k].cX, p[player].liste[k].cY, listeVilleCapturable[i].x, listeVilleCapturable[i].y);
                                        if ( distance != -1 && listeVilleCapturable[i].cible == 0 ){
                                            temp = listeVilleCapturable[i];
                                        }
                                    }
                                    else{
                                        int distance2 = mapPath(surface, nbSquare, &game_map[0], aspectRatio, p[player].liste[k].cX, p[player].liste[k].cY, listeVilleCapturable[i].x, listeVilleCapturable[i].y);
                                        if( (distance2 != -1 ) && distance > distance2){
                                            if(listeVilleCapturable[i].cible == 0){
                                                temp = listeVilleCapturable[i];
                                                distance = distance2;
                                            }
                                        }
                                        else{
                                        }
                                    }
                                }
                            }
                            if (temp.cible != -1){
                                nbDeVilleCapturable--;
                                for (int j = 0 ; j<listeVilleCapturable.size(); j++){
                                    if(listeVilleCapturable[j].x == temp.x && listeVilleCapturable[j].y == temp.y ){
                                        if( !(p[player].liste[k].cX == listeVilleCapturable[j].x && p[player].liste[k].cY == listeVilleCapturable[j].y) ){
                                            vector < pair<int,int> > path = vectorMapPath(surface, nbSquare, &game_map[0], aspectRatio, p[player].liste[k].cX, p[player].liste[k].cY, listeVilleCapturable[j].x, listeVilleCapturable[j].y);
                                            std::reverse(path.begin(), path.end());
                                            int destX;
                                            int destY;
                                            for(int z = 0; z < path.size(); z++){
                                                printf("CHEMIN = X: %d | Y: %d \n", path[z].first, path[z].second);
                                            }

                                            if( path.size() > p[player].liste[k].fuel ){
                                                destX = path[p[player].liste[k].fuel].first;
                                                destY = path[p[player].liste[k].fuel].second;
                                            }
                                            else{
                                                destX = path[path.size()-1].first;
                                                destY = path[path.size()-1].second;
                                            }
                                            unitMouvement(surface, &game_map[0], &p[0], player, p[player].liste[k].cX, p[player].liste[k].cY, destX, destY, nbSquare, aspectRatio);
                                        }
                                    }
                                }
                            }
                        }
                        else{
                            printf("[PLAYER %d] CONSEIL : SOLDAT %i PLUS DE VILLE CAPTURABLE \n",player+1, k);
                        }

                    }
                }
                else{
                    if ( p[player].liste[k].ground != 7 && p[player].liste[k].ground != (9-player)){
                        printf("[PLAYER %d] CONSEIL : SOLDAT %i DIRECTION VILLE \n",player+1, k);
                    }
                }
            }
        }
    }

    *fin = 0;

}

vector < pair<int,int> > listeFantassin;

void computerTurnMouvementFantassin(SDL_Surface* surface, map_square *game_map, GLuint texture_soldier[], player *p,int player, int nbSquare, float aspectRatio, int *fin, int *numFantassin, vector < pair<int,int> > listeFantassin ,int *x, int *y, int *indexTemp, int *directionTemp, int sizeSquare){

    int adversaire;
    if(player == 1){
        adversaire = 0;
    }
    else{
        adversaire = 1;
    }

    int nbFantassin = 0;
    int nbBazooka = 0;
    int nbTank = 0;
    int nbCopter = 0;
    int nbShip = 0;

    int nbAdvFantassin = 0;
    int nbAdvBazooka = 0;
    int nbAdvTank = 0;
    int nbAdvCopter = 0;
    int nbAdvShip = 0;

    for (int i = 0; i<MAXUNITE; i++){

        // ORDINATEUR
        if( (p[player].liste[i].type_soldier-5*player) == 1 ){
            nbFantassin++;
        }
        if( (p[player].liste[i].type_soldier-5*player) == 2 ){
            nbBazooka++;
        }
        if( (p[player].liste[i].type_soldier-5*player) == 3 ){
            nbTank++;
        }
        if( (p[player].liste[i].type_soldier-5*player) == 4 ){
            nbCopter++;
        }
        if( (p[player].liste[i].type_soldier-5*player) == 5 ){
            nbShip++;
        }

        // ADVERSAIRE
        if( (p[adversaire].liste[i].type_soldier-5*adversaire) == 1 ){
            nbAdvFantassin++;
        }
        if( (p[adversaire].liste[i].type_soldier-5*adversaire) == 2 ){
            nbAdvBazooka++;
        }
        if( (p[adversaire].liste[i].type_soldier-5*adversaire) == 3 ){
            nbAdvTank++;
        }
        if( (p[adversaire].liste[i].type_soldier-5*adversaire) == 4 ){
            nbAdvCopter++;
        }
        if( (p[adversaire].liste[i].type_soldier-5*adversaire) == 5 ){
            nbAdvShip++;
        }
    }

    for (int i = 0; i<MAXUNITE; i++){

        if( (p[player].liste[i].type_soldier-5*player) == 1 ){
            nbFantassin++;
        }
        if( (p[player].liste[i].type_soldier-5*player) == 2 ){
            nbBazooka++;
        }
        if( (p[player].liste[i].type_soldier-5*player) == 3 ){
            nbTank++;
        }
        if( (p[player].liste[i].type_soldier-5*player) == 4 ){
            nbCopter++;
        }
        if( (p[player].liste[i].type_soldier-5*player) == 5 ){
            nbShip++;
        }
    }

    vector < villeIA > listeVilleCapturable;
    int nbDeVilleCapturable = 0;
    for(int i = 0; i<nbSquare*nbSquare; i++){
        if(game_map[i].ground == 7){
            villeIA temp;
            temp.x = game_map[i].cX;
            temp.y = game_map[i].cY;
            temp.etat = 0;
            temp.cible = 0;
            listeVilleCapturable.push_back(temp);
            nbDeVilleCapturable++;
        }
        if(game_map[i].ground == 8){
            villeIA temp;
            temp.x = game_map[i].cX;
            temp.y = game_map[i].cY;
            temp.etat = 0;
            temp.cible = 0;
            listeVilleCapturable.push_back(temp);
            nbDeVilleCapturable++;
        }
    }

    for (int i=0; i<MAXUNITE; i++){
        if( p[player].liste[i].cX == listeFantassin[*numFantassin].first && p[player].liste[i].cY == listeFantassin[*numFantassin].second){
            printf("TROUVE soldat %d ! \n", *numFantassin);

            for(int a=0; a<listeVilleCapturable.size(); a++){

                if( mapPath(surface, nbSquare, &game_map[0], aspectRatio, p[player].liste[i].cX, p[player].liste[i].cY, listeVilleCapturable[a].x, listeVilleCapturable[a].y) == -1 ){
                    nbDeVilleCapturable--;
                }
                printf("NOMBRE DE VILLE CAPTURABLE : %d \n", nbDeVilleCapturable);
                printf("VILLE-2 %d : %d %d ETAT %d CIBLE %d\n", a+1, listeVilleCapturable[a].x, listeVilleCapturable[a].y, listeVilleCapturable[a].etat, listeVilleCapturable[a].cible );
            }


            if( p[player].liste[i].fuel != 0 && p[player].liste[i].action == 1){
                if ( game_map[ p[player].liste[i].cY * nbSquare + p[player].liste[i].cX].ground != 7 && game_map[ p[player].liste[i].cY * nbSquare + p[player].liste[i].cX].ground != (9-player)){
                    if( game_map[ p[player].liste[i].cY * nbSquare + p[player].liste[i].cX].ground == 9){ // cas d'�tre sur une ville du joueur
                        printf("Je suis � la maison soldat %d!!! \n", *numFantassin);
                        printf("nb de ville capturable %d !!! \n", nbDeVilleCapturable);

                        //*fin = 0;
                        //*numFantassin = *numFantassin +1;
                        //printf("numFantassin = %d \n", *numFantassin);
                    }
                    if(nbDeVilleCapturable > 0){
                        printf("ville encore � capturer \n");
                        villeIA temp;
                        temp.cible = -1;
                        int distance;
                        printf("nb de ville capturable : %d ", listeVilleCapturable.size() );
                        for (int j = 0 ; j<listeVilleCapturable.size() ; j++){
                            if(listeVilleCapturable[j].etat == 0){
                                if(temp.cible == -1){
                                    distance = mapPath(surface, nbSquare, &game_map[0], aspectRatio, p[player].liste[i].cX, p[player].liste[i].cY, listeVilleCapturable[j].x, listeVilleCapturable[j].y);
                                    printf("distance : %d \n", distance);
                                    if ( distance != -1 && listeVilleCapturable[i].cible == 0 ){
                                        printf("AFFECTATION ! \n");
                                        temp = listeVilleCapturable[j];
                                    }
                                }
                                else{
                                    int distance2 = mapPath(surface, nbSquare, &game_map[0], aspectRatio, p[player].liste[i].cX, p[player].liste[i].cY, listeVilleCapturable[j].x, listeVilleCapturable[j].y);
                                    printf("distance 2: %d \n", distance2);
                                    if( (distance2 != -1 ) && distance > distance2){
                                        if(listeVilleCapturable[j].cible == 0){
                                            temp = listeVilleCapturable[j];
                                            distance = distance2;
                                        }
                                    }
                                }
                            }
                        }

                        if(temp.cible != -1){
                            printf("direction ville %d %d \n", temp.x, temp.y);
                            for (int j=0; j< listeVilleCapturable.size(); j++){
                                if(listeVilleCapturable[j].x == temp.x && listeVilleCapturable[j].y == temp.y ){
                                    int destX;
                                    int destY;
                                    if( !(p[player].liste[i].cX == listeVilleCapturable[j].x && p[player].liste[i].cY == listeVilleCapturable[j].y) ){
                                        vector < pair<int,int> > path = vectorMapPath(surface, nbSquare, &game_map[0], aspectRatio, p[player].liste[i].cX, p[player].liste[i].cY, listeVilleCapturable[j].x, listeVilleCapturable[j].y);
                                        float distance = mapPath(surface, nbSquare, &game_map[0], aspectRatio, p[player].liste[i].cX, p[player].liste[i].cY, listeVilleCapturable[j].x, listeVilleCapturable[j].y);
                                        printf("DISTANCE ! %f \n", distance );
                                        std::reverse(path.begin(), path.end());
                                        int k = 0;
                                        distance = mapPath(surface, nbSquare, &game_map[0], aspectRatio, p[player].liste[i].cX, p[player].liste[i].cY, path[k].first, path[k].second);
                                        int trouve = 0;
                                        destX = path[k].first;
                                        destY = path[k].second;
                                        while(trouve == 0 && k<path.size()){
                                            k++;
                                            distance = mapPath(surface, nbSquare, &game_map[0], aspectRatio, p[player].liste[i].cX, p[player].liste[i].cY, path[k].first, path[k].second);
                                            if(distance != -1 && distance<= p[player].liste[i].fuel){
                                                destX = path[k].first;
                                                destY = path[k].second;
                                                printf("--- distance %f \n", distance);
                                            }
                                            else{
                                                trouve = 1;
                                            }
                                        }

                                        if( !(*x == destX*sizeSquare + sizeSquare/2 && *y == destY*sizeSquare + sizeSquare/2)){

                                            std::reverse(path.begin(), path.end());
                                            animationMouvement(&game_map[0],
                                                surface,
                                                texture_soldier,
                                                path,
                                                indexTemp,
                                                x,
                                                y,
                                                directionTemp,
                                                sizeSquare,
                                                aspectRatio,
                                                nbSquare,
                                                6,
                                                fin,
                                                &p[0],
                                                1);
                                        }
                                        else{
                                            unitMouvement(surface, &game_map[0], &p[0], player, p[player].liste[i].cX, p[player].liste[i].cY, destX, destY, nbSquare, aspectRatio);
                                            *fin = 0;
                                            *numFantassin = *numFantassin +1;
                                            printf("numFantassin = %d \n", *numFantassin);
                                            if (game_map[ p[player].liste[i].cY * nbSquare + p[player].liste[i].cX].ground == 7 || game_map[ p[player].liste[i].cY * nbSquare + p[player].liste[i].cX].ground == 8){
                                                printf("CAPTURE \n");
                                                captureBuilding(&game_map[0], &p[0], nbSquare,  aspectRatio, player, sizeSquare, p[player].liste[i].cX , p[player].liste[i].cY );
                                            }
                                            listeVilleCapturable.clear();
                                            int nbDeVilleCapturable = 0;
                                            for(int i = 0; i<nbSquare*nbSquare; i++){
                                                if(game_map[i].ground == 7){
                                                    villeIA temp;
                                                    temp.x = game_map[i].cX;
                                                    temp.y = game_map[i].cY;
                                                    temp.etat = 0;
                                                    temp.cible = 0;
                                                    listeVilleCapturable.push_back(temp);
                                                    nbDeVilleCapturable++;
                                                }
                                                if(game_map[i].ground == 8){
                                                    villeIA temp;
                                                    temp.x = game_map[i].cX;
                                                    temp.y = game_map[i].cY;
                                                    temp.etat = 1;
                                                    temp.cible = 0;
                                                    listeVilleCapturable.push_back(temp);
                                                    nbDeVilleCapturable++;
                                                }
                                            }
                                            for(int a=0; a<nbDeVilleCapturable; a++){
                                                printf("VILLE-3 %d : %d %d ETAT %d CIBLE %d\n", a+1, listeVilleCapturable[a].x, listeVilleCapturable[a].y, listeVilleCapturable[a].etat, listeVilleCapturable[a].cible );
                                            }
                                        }
                                    }
                                    else{
                                        *fin = 0;
                                        *numFantassin = *numFantassin +1;
                                        printf("numFantassin = %d \n", *numFantassin);
                                    }
                                }
                            }
                        }
                    }
                    else{
                        //printf("PLUS DE VILLE !!! \n");
                        printf("============================ ID FANTASSIN : %d %d \n\n", p[player].liste[i].cX, p[player].liste[i].cY);
                        //printf("test %d \n", mapPath(surface, nbSquare, &game_map[0], aspectRatio, 0, 3, 1, 0));
                        vector < villeIA > listeSoldat;
                        listeSoldat.clear();
                        int nbDeSoldat = 0;
                        for(int k = 0; k<MAXUNITE; k++){
                            if( (p[0].liste[k].type_soldier) == 1 ){
                                nbDeSoldat++;
                                villeIA temp;
                                temp.x = p[0].liste[k].cX;
                                temp.y = p[0].liste[k].cY;
                                temp.etat = 1;
                                villeIA temp1 = isRange(p[player].liste[i].cX, p[player].liste[i].cY, temp.x, temp.y, p[player].liste[i].range, nbSquare, aspectRatio, surface, &game_map[0], i, &p[0], player ) ;
                                temp.cible = temp1.etat;
                                listeSoldat.push_back(temp);
                            }
                            if( (p[0].liste[k].type_soldier) == 2 ){
                                nbDeSoldat++;
                                villeIA temp;
                                temp.x = p[0].liste[k].cX;
                                temp.y = p[0].liste[k].cY;
                                temp.etat = 2;
                                villeIA temp1 = isRange(p[player].liste[i].cX, p[player].liste[i].cY, temp.x, temp.y, p[player].liste[i].range, nbSquare, aspectRatio, surface, &game_map[0], i, &p[0], player ) ;
                                temp.cible = temp1.etat;
                                listeSoldat.push_back(temp);
                            }
                            if( (p[0].liste[k].type_soldier) == 3 ){

                                nbDeSoldat++;
                                villeIA temp;
                                temp.x = p[0].liste[k].cX;
                                temp.y = p[0].liste[k].cY;
                                temp.etat = 3;
                                villeIA temp1 = isRange(p[player].liste[i].cX, p[player].liste[i].cY, temp.x, temp.y, p[player].liste[i].range, nbSquare, aspectRatio, surface, &game_map[0], i, &p[0], player ) ;


                                temp.cible = temp1.etat;
                                listeSoldat.push_back(temp);
                            }
                            if( (p[0].liste[k].type_soldier) == 4 ){
                                nbDeSoldat++;
                                villeIA temp;
                                temp.x = p[0].liste[k].cX;
                                temp.y = p[0].liste[k].cY;
                                temp.etat = 4;
                                villeIA temp1 = isRange(p[player].liste[i].cX, p[player].liste[i].cY, temp.x, temp.y, p[player].liste[i].range, nbSquare, aspectRatio, surface, &game_map[0], i, &p[0], player ) ;
                                temp.cible = temp1.etat;
                                listeSoldat.push_back(temp);
                            }
                            if( (p[0].liste[k].type_soldier) == 5 ){
                                nbDeSoldat++;
                                villeIA temp;
                                temp.x = p[0].liste[k].cX;
                                temp.y = p[0].liste[k].cY;
                                temp.etat = 5;
                                villeIA temp1 = isRange(p[player].liste[i].cX, p[player].liste[i].cY, temp.x, temp.y, p[player].liste[i].range, nbSquare, aspectRatio, surface, &game_map[0], i, &p[0], player ) ;

                                temp.cible = temp1.etat;

                                listeSoldat.push_back(temp);
                            }
                        }
                        printf("AFFICHAGE SOLDAT \n");
                        int meilleur = 0;
                        for (int k = 0; k<listeSoldat.size(); k++ ){
                            if(listeSoldat[meilleur].cible>listeSoldat[k].cible){
                                meilleur = k;
                            }

                        }
                        printf("Meilleur adversaire : %d \n", meilleur);
                        printf("Case destination %d %d | possesseur : %d \n", listeSoldat[meilleur].x, listeSoldat[meilleur].y, game_map[listeSoldat[meilleur].y * nbSquare + listeSoldat[meilleur].x].player );

                        if(listeSoldat.size()!=0){
                            printf("---------------------- ennemi à porté ? %d \n", toRange(p[player].liste[i].cX, p[player].liste[i].cY, p[player].liste[i].range , nbSquare, &game_map[0]));
                            if(toRange(p[player].liste[i].cX, p[player].liste[i].cY, p[player].liste[i].range , nbSquare, &game_map[0]) > 0){
                                int res = findTarget(p[player].liste[i].cX ,p[player].liste[i].cY, listeSoldat[meilleur].x,listeSoldat[meilleur].y,p[player].liste[i].range , nbSquare, &game_map[0]);
                                printf("RES : %d \n", res);
                                attaque(&game_map[0], &p[0], player, p[player].liste[i].cX, p[player].liste[i].cY, listeSoldat[meilleur].x, listeSoldat[meilleur].y, nbSquare);
                                *fin = 0;
                                *numFantassin = *numFantassin +1;
                                printf("numFantassin = %d \n\n", *numFantassin);

                            }
                            else{
                                villeIA temp1 = isRange(p[player].liste[i].cX, p[player].liste[i].cY,listeSoldat[meilleur].x, listeSoldat[meilleur].y, p[player].liste[i].range, nbSquare, aspectRatio, surface, &game_map[0], i, &p[0], player);
                                if(temp1.x == -1 && temp1.y == -1){
                                    printf("\n ne devrait pas bouger \n");
                                    *fin = 0;
                                    *numFantassin = *numFantassin +1;
                                    i = MAXUNITE;
                                }
                                else{
                                    int destX;
                                    int destY;
                                    if( !(p[player].liste[i].cX == temp1.x && p[player].liste[i].cY == temp1.y) ){
                                        vector < pair<int,int> > path = vectorMapPath(surface, nbSquare, &game_map[0], aspectRatio, p[player].liste[i].cX, p[player].liste[i].cY, temp1.x, temp1.y);
                                        float distance = mapPath(surface, nbSquare, &game_map[0], aspectRatio, p[player].liste[i].cX, p[player].liste[i].cY, temp1.x, temp1.y);
                                        printf("DISTANCE !! %f \n", distance );
                                        std::reverse(path.begin(), path.end());
                                        int k = 0;
                                        int trouve = 0;
                                        destX = path[k].first;
                                        destY = path[k].second;
                                        while(trouve == 0 && k<path.size()){
                                            k++;
                                            distance = mapPath(surface, nbSquare, &game_map[0], aspectRatio, p[player].liste[i].cX, p[player].liste[i].cY, path[k].first, path[k].second);
                                            if(distance != -1 && distance<= p[player].liste[i].fuel){
                                                destX = path[k].first;
                                                destY = path[k].second;
                                                printf("--- distance %f \n", distance);
                                            }
                                            else{
                                                trouve = 1;
                                            }
                                        }
                                        if( !(*x == destX*sizeSquare + sizeSquare/2 && *y == destY*sizeSquare + sizeSquare/2)){
                                            std::reverse(path.begin(), path.end());
                                            animationMouvement(&game_map[0],
                                                        surface,
                                                        texture_soldier,
                                                        path,
                                                        indexTemp,
                                                        x,
                                                        y,
                                                        directionTemp,
                                                        sizeSquare,
                                                        aspectRatio,
                                                        nbSquare,
                                                        6,
                                                        fin,
                                                        &p[0],
                                                        1);
                                        }
                                        else{
                                            printf("************* MOUVEMENT dans fonction ordinateur \n");
                                            printf("************* Depart : %d %d | Arriv� : %d %d \n", p[player].liste[i].cX, p[player].liste[i].cY, destX, destY );
                                            unitMouvement(surface, &game_map[0], &p[0], player, p[player].liste[i].cX, p[player].liste[i].cY, destX, destY, nbSquare, aspectRatio);
                                            *fin = 0;
                                            *numFantassin = *numFantassin +1;
                                            printf("================================== possesseur case %d %d : %d \n",p[player].liste[i].cX,p[player].liste[i].cY, game_map[p[player].liste[i].cY * nbSquare + p[player].liste[i].cX].player);
                                        }
                                    }
                                    else{
                                        printf("je ne bouge pas ! \n");
                                    }
                                }
                            }
                        }
                    }
                }
                else{
                    if (game_map[ p[player].liste[i].cY * nbSquare + p[player].liste[i].cX].ground == 7 || game_map[ p[player].liste[i].cY * nbSquare + p[player].liste[i].cX].ground == 8){
                        printf("CAPTURE \n");
                        captureBuilding(&game_map[0], &p[0], nbSquare,  aspectRatio, player, sizeSquare, p[player].liste[i].cX , p[player].liste[i].cY );
                        int nbDeVilleCapturable = 0;
                        listeVilleCapturable.clear();
                        for(int i = 0; i<nbSquare*nbSquare; i++){
                            if(game_map[i].ground == 7){
                                villeIA temp;
                                temp.x = game_map[i].cX;
                                temp.y = game_map[i].cY;
                                temp.etat = 0;
                                temp.cible = 0;
                                listeVilleCapturable.push_back(temp);
                                nbDeVilleCapturable++;
                            }
                            if(game_map[i].ground == 8){
                                villeIA temp;
                                temp.x = game_map[i].cX;
                                temp.y = game_map[i].cY;
                                temp.etat = 1;
                                temp.cible = 0;
                                listeVilleCapturable.push_back(temp);
                                nbDeVilleCapturable++;
                            }

                            for(int a=0; a<nbDeVilleCapturable; a++){
                                printf("VILLE %d : %d %d ETAT %d CIBLE %d\n", a+1, listeVilleCapturable[a].x, listeVilleCapturable[a].y, listeVilleCapturable[a].etat, listeVilleCapturable[a].cible );
                            }
                        }
                    }
                    else{
                        *fin = 0;
                        *numFantassin = *numFantassin +1;
                        i = MAXUNITE;
                    }
                }
            }
            else{
                *fin = 0;
                *numFantassin = *numFantassin +1;
                i = MAXUNITE;
            }
        }
    }

}

villeIA isRangeRes(int destX, int destY, int range, int nbSquare, int aspectRatio, SDL_Surface* surface, map_square *game_map, int k, player *p, int player ){
  vector < pair<int,int> > path = vectorMapPath(surface, nbSquare, &game_map[0], aspectRatio, p[player].liste[k].cX, p[player].liste[k].cY, destX , destY);
  std::reverse(path.begin(), path.end());
  if( path.size() > p[player].liste[k].fuel ){
    destX = path[p[player].liste[k].fuel].first;
    destY = path[p[player].liste[k].fuel].second;
  }
  else{
    destX = path[path.size()-1].first;
    destY = path[path.size()-1].second;
  }
  villeIA temp;
  temp.x = destX;
  temp.y = destY;
  temp.etat = path.size();
  return temp;
}

// PERMET DE SAVOIR SI UN SOLDAT PEUT ETRE ACCESSIBLE POUR L'ORDINATEUR

villeIA isRange(int x, int y, int destX, int destY, int range, int nbSquare, int aspectRatio, SDL_Surface* surface, map_square *game_map, int k, player *p, int player  ){

  printf("\n CIBLE : %d %d \n", destX, destY);
  int temp1,temp2;

  if(x <= destX){
    //printf("A GAUCHE ! \n");
    if(y <= destY){
      //printf("EN HAUT ! \n");
      //printf("coin haut-gauche ! \n");
      temp1 = outMap(destX - range, nbSquare);
      temp2 = outMap(destY - range, nbSquare);

      float distance1 = mapPath(surface, nbSquare, &game_map[0], aspectRatio, x, y, temp1, destY );
      float distance2 = mapPath(surface, nbSquare, &game_map[0], aspectRatio, x, y, destX, temp2 );
      if(distance1 == -1 && distance2 == -1){
        //printf("coin haut-droite ! \n");
        temp1 = outMap(destX - range, nbSquare);
        temp2 = outMap(destY + range, nbSquare);

        float distance1 = mapPath(surface, nbSquare, &game_map[0], aspectRatio, x, y, temp1, destY );
        float distance2 = mapPath(surface, nbSquare, &game_map[0], aspectRatio, x, y, destX, temp2 );
        if(distance1>-1 && distance2>-1){
          if(distance1<=distance2){
            printf("distance 1 meilleur ! \n");
            return isRangeRes( temp1 , destY, range, nbSquare, aspectRatio, surface, &game_map[0], k, &p[0], player);
          }
          else{
            printf("distance 2 meilleur ! \n");
            return isRangeRes( destX , temp2, range, nbSquare, aspectRatio, surface, &game_map[0], k, &p[0], player);
          }
        }
        else{
          if(distance1 <= -1 && distance2 <= -1){
            villeIA temp;
            temp.x = -1;
            temp.y = -1;
            temp.etat = 9999;
            return temp;
          }
          else{
            if(distance1 <= -1){
              //printf("distance 2 meilleur ! \n");
              return isRangeRes( destX , temp2, range, nbSquare, aspectRatio, surface, &game_map[0], k, &p[0], player);
            }
            else{
              //printf("distance 1 meilleur ! \n");
              return isRangeRes( temp1 , destY, range, nbSquare, aspectRatio, surface, &game_map[0], k, &p[0], player);
            }
          }
        }
      }
      else if(distance1>-1 && distance2>-1){
        if(distance1<=distance2){
          printf("distance 1 meilleur ! \n");
          return isRangeRes( temp1 , destY, range, nbSquare, aspectRatio, surface, &game_map[0], k, &p[0], player);
        }
        else{
          printf("distance 2 meilleur ! \n");
          return isRangeRes( destX , temp2, range, nbSquare, aspectRatio, surface, &game_map[0], k, &p[0], player);
        }
      }
      else{
        if(distance1 <= -1 && distance2 <= -1){
          villeIA temp;
          temp.x = -1;
          temp.y = -1;
          temp.etat = 9999;
          return temp;
        }
        else{
          if(distance1 <= -1){
            printf("distance 2 meilleur ! \n");
            return isRangeRes( destX , temp2, range, nbSquare, aspectRatio, surface, &game_map[0], k, &p[0], player);
          }
          else{
            printf("distance 1 meilleur ! \n");
            return isRangeRes( temp1 , destY, range, nbSquare, aspectRatio, surface, &game_map[0], k, &p[0], player);
          }
        }
      }
    }
    else{
      printf("EN BAS ! \n");
      printf("--- coin bas gauche ! \n");

      temp1 = outMap(destX - range, nbSquare);
      temp2 = outMap(destY + range, nbSquare);

      float distance1 = mapPath(surface, nbSquare, &game_map[0], aspectRatio, x, y, temp1, destY );
      printf("distance 1 : %d \n", distance1);
      float distance2 = mapPath(surface, nbSquare, &game_map[0], aspectRatio, x, y, destX, temp2 );
      printf("distance 2 : %d \n", distance2);
      if(distance1 <= -1 && distance2 <= -1){
        printf("--- coin haut gauche ! \n");

        temp1 = outMap(destX - range, nbSquare);
        temp2 = outMap(destY - range, nbSquare);

        float distance1 = mapPath(surface, nbSquare, &game_map[0], aspectRatio, x, y, temp1, destY );
        float distance2 = mapPath(surface, nbSquare, &game_map[0], aspectRatio, x, y, destX, temp2 );
        if(distance1 >-1 && distance2 >-1){
          if(distance1<=distance2){
            printf("distance 1 meilleur ! \n");
            return isRangeRes( temp1 , destY, range, nbSquare, aspectRatio, surface, &game_map[0], k, &p[0], player);
          }
          else{
            printf("distance 2 meilleur ! \n");
            return isRangeRes( destX , temp2, range, nbSquare, aspectRatio, surface, &game_map[0], k, &p[0], player);
          }
        }
        else{
          if(distance1 <= -1 && distance2 <= -1){
            printf("case non existante ! ");
            villeIA temp;
            temp.x = -1;
            temp.y = -1;
            temp.etat = 9999;
            return temp;
          }
          else{
            if(distance1 <= -1){
              printf("distance 2 meilleur ! \n");
              return isRangeRes( destX , temp2, range, nbSquare, aspectRatio, surface, &game_map[0], k, &p[0], player);
            }
            else{
              printf("distance 1 meilleur ! \n");
              return isRangeRes( temp1 , destY, range, nbSquare, aspectRatio, surface, &game_map[0], k, &p[0], player);
            }
          }
        }
      }
      else if(distance1>-1 && distance2>-1){
        if(distance1<=distance2){
          printf("distance 1 meilleur ! \n");
          return isRangeRes( temp1 , destY, range, nbSquare, aspectRatio, surface, &game_map[0], k, &p[0], player);
        }
        else{
          printf("distance 2 meilleur ! \n");
          return isRangeRes( destX , temp2, range, nbSquare, aspectRatio, surface, &game_map[0], k, &p[0], player);
        }
      }
      else{
        if(distance1 <= -1 && distance2 <= -1){
          villeIA temp;
          temp.x = -1;
          temp.y = -1;
          temp.etat = 9999;
          return temp;
        }
        else{
          if(distance1 <= -1){
            printf("distance 2 meilleur ! \n");
            return isRangeRes( destX , temp2, range, nbSquare, aspectRatio, surface, &game_map[0], k, &p[0], player);
          }
          else{
            printf("distance 1 meilleur ! \n");
            return isRangeRes( temp1 , destY, range, nbSquare, aspectRatio, surface, &game_map[0], k, &p[0], player);
          }
        }
      }
    }
  }
  else{

    printf("A DROITE ! \n");
    if(y <= destY){
      printf("EN HAUT ! \n");
      printf("--- coin bas-gauche ! \n");

      temp1 = outMap(destX + range, nbSquare);
      temp2 = outMap(destY - range, nbSquare);

      float distance1 = mapPath(surface, nbSquare, &game_map[0], aspectRatio, x, y, temp1, destY );
      float distance2 = mapPath(surface, nbSquare, &game_map[0], aspectRatio, x, y, destX , temp2);
      if(distance1 == -1 && distance2 == -1){
        printf("--- coin bas-droite ! \n");

        temp1 = outMap(destX + range, nbSquare);
        temp2 = outMap(destY + range, nbSquare);

        float distance1 = mapPath(surface, nbSquare, &game_map[0], aspectRatio, x, y, temp1, destY );
        float distance2 = mapPath(surface, nbSquare, &game_map[0], aspectRatio, x, y, destX , temp2);
        if(distance1!=-1 && distance2!=-1){
          if(distance1<=distance2){
            printf("distance 1 meilleur ! \n");
            return isRangeRes( temp1 , destY, range, nbSquare, aspectRatio, surface, &game_map[0], k, &p[0], player);
          }
          else{
            printf("distance 2 meilleur ! \n");
            return isRangeRes( destX , temp2, range, nbSquare, aspectRatio, surface, &game_map[0], k, &p[0], player);
          }
        }
        else{
          if(distance1 <= -1 && distance2 <= -1){
            villeIA temp;
            temp.x = -1;
            temp.y = -1;
            temp.etat = 9999;
            return temp;
          }
          else{
            if(distance1 <= -1){
              printf("distance 2 meilleur ! \n");
              return isRangeRes( destX , temp2, range, nbSquare, aspectRatio, surface, &game_map[0], k, &p[0], player);
            }
            else{
              printf("distance 1 meilleur ! \n");
              return isRangeRes( temp1 , destY, range, nbSquare, aspectRatio, surface, &game_map[0], k, &p[0], player);
            }
          }
        }
      }
      else if(distance1>-1 && distance2>-1){
        if(distance1<=distance2){
          printf("distance 1 meilleur ! \n");
          return isRangeRes( temp1 , destY, range, nbSquare, aspectRatio, surface, &game_map[0], k, &p[0], player);
        }
        else{
          printf("distance 2 meilleur ! \n");
          return isRangeRes( destX , temp2, range, nbSquare, aspectRatio, surface, &game_map[0], k, &p[0], player);
        }
      }
      else{
        if(distance1 <= -1 && distance2 <= -1){
          villeIA temp;
          temp.x = -1;
          temp.y = -1;
          temp.etat = 9999;
          return temp;
        }
        else{
          if(distance1 <= -1){
            printf("distance 2 meilleur ! \n");
            return isRangeRes( destX , temp2, range, nbSquare, aspectRatio, surface, &game_map[0], k, &p[0], player);
          }
          else{
            printf("distance 1 meilleur ! \n");
            return isRangeRes( temp1, destY, range, nbSquare, aspectRatio, surface, &game_map[0], k, &p[0], player);
          }
        }
      }

    }
    else{
      printf("EN BAS ! \n");
      printf("--- coin bas-droite ! \n");

      temp1 = outMap(destX + range, nbSquare);
      temp2 = outMap(destY + range, nbSquare);

      float distance1 = mapPath(surface, nbSquare, &game_map[0], aspectRatio, x, y, temp1, destY );
      float distance2 = mapPath(surface, nbSquare, &game_map[0], aspectRatio, x, y, destX , temp2);
      if(distance1 == -1 && distance2 == -1){

        temp1 = outMap(destX + range, nbSquare);
        temp2 = outMap(destY - range, nbSquare);

        float distance1 = mapPath(surface, nbSquare, &game_map[0], aspectRatio, x, y, temp1, destY );
        float distance2 = mapPath(surface, nbSquare, &game_map[0], aspectRatio, x, y, destX , temp2);
        if(distance1>-1 && distance2>-1){
          if(distance1<=distance2){
            printf("distance 1 meilleur ! \n");
            return isRangeRes( temp1 , destY, range, nbSquare, aspectRatio, surface, &game_map[0], k, &p[0], player);
          }
          else{
            printf("distance 2 meilleur ! \n");
            return isRangeRes( destX , temp2, range, nbSquare, aspectRatio, surface, &game_map[0], k, &p[0], player);
          }
        }
        else{
          if(distance1 <= -1 && distance2 <= -1){
            villeIA temp;
            temp.x = -1;
            temp.y = -1;
            temp.etat = 9999;
            return temp;
          }
          else{
            if(distance1 <= -1){
              printf("distance 2 meilleur ! \n");
              return isRangeRes( destX , temp2, range, nbSquare, aspectRatio, surface, &game_map[0], k, &p[0], player);
            }
            else{
              printf("distance 1 meilleur ! \n");
              return isRangeRes( temp1 , destY, range, nbSquare, aspectRatio, surface, &game_map[0], k, &p[0], player);
            }
          }
        }
      }
      else if(distance1>-1 && distance2>-1){
        if(distance1<=distance2){
          printf("distance 1 meilleur ! \n");
          return isRangeRes( temp1 , destY, range, nbSquare, aspectRatio, surface, &game_map[0], k, &p[0], player);
        }
        else{
          printf("distance 2 meilleur ! \n");
          return isRangeRes( destX , temp2, range, nbSquare, aspectRatio, surface, &game_map[0], k, &p[0], player);
        }
      }
      else{
        if(distance1 <= -1 && distance2 <= -1){
          villeIA temp;
          temp.x = -1;
          temp.y = -1;
          temp.etat = 9999;
          return temp;
        }
        else{
          if(distance1 <= -1){
            printf("distance 2 meilleur ! \n");
            return isRangeRes( destX , temp2, range, nbSquare, aspectRatio, surface, &game_map[0], k, &p[0], player);
          }
          else{
            printf("distance 1 meilleur ! \n");
            return isRangeRes( temp1 , destY, range, nbSquare, aspectRatio, surface, &game_map[0], k, &p[0], player);
          }
        }
      }
    }
  }
}

int toRange(int x, int y,int range, int nbSquare, map_square *game_map){
    int compteur = 0;
    int j = 0;
    int increment = 1;
    for (int i = 0; i<=range ; i++){ // partie sup�rieur du triangle
        if(y-i >= 0 && y-i<nbSquare && ((y-i) != y)){
        //printf("CASE COMPATIBLE : X %d | Y %d \n", x, y-i);
            if(game_map[(y-i)*nbSquare + x].player == 1){
                compteur++;
            }
        }
        for (j ; j<range; j++){
            if(y-i >= 0 && y-i<nbSquare){
                if(x-increment>= 0 && x-increment < nbSquare){
                    //printf("CASE COMPATIBLE cas gauche : X %d | Y %d \n", x-increment, y-i);
                    if(game_map[(y-i)*nbSquare + (x-increment)].player == 1){
                        compteur++;
                    }
                }
                if(x+increment>= 0 && x+increment < nbSquare){
                    //printf("CASE COMPATIBLE cas droite : X %d | Y %d \n", x+increment, y-i);
                    if(game_map[(y-i)*nbSquare + (x+increment)].player == 1){
                        compteur++;
                    }
                }
                increment++;
             }
         }
         increment = 1;
         j = i+1;
    }
    j = 1;
    int tampon = range;
        for (int i = 1; i<=range ; i++){ // partie inf�rieur du triangle
            if(y+i >= 0 && y+i<nbSquare){
                //printf("CASE COMPATIBLE : X %d | Y %d \n", x, y+i);
                if(game_map[(y+i)*nbSquare + x].player == 1){
                    compteur++;
                }
            }
            for (j = 1 ; j< tampon; j++){
                if(y+i >= 0 && y+i<nbSquare){
                    if(x-j>= 0 && x-j < nbSquare){
                        //printf("CASE COMPATIBLE : X %d | Y %d \n", x-j, y+i);
                        if(game_map[(y+i)*nbSquare + (x-j)].player == 1){
                            compteur++;
                        }
                    }
                    if(x+j>= 0 && x+j < nbSquare){
                        //printf("CASE COMPATIBLE : X %d | Y %d \n", x+j, y+i);
                        if(game_map[(y+i)*nbSquare + (x+j)].player == 1){
                            compteur++;
                        }
                    }
                 }
            }
            tampon --;
        }
    return compteur;
}

int findTarget(int x, int y, int cibleX, int cibleY, int range, int nbSquare, map_square *game_map){
    int trouve = 0;
    int j = 0;
    int increment = 1;
    for (int i = 0; i<=range ; i++){ // partie sup�rieur du triangle
        if(y-i >= 0 && y-i<nbSquare && ((y-i) != y)){
        //printf("CASE COMPATIBLE : X %d | Y %d \n", x, y-i);
            if(game_map[(y-i)*nbSquare + x].player == 1){
                if(cibleX == x && cibleY == y-i){
                    trouve = 1;
                }
            }
        }
        for (j ; j<range; j++){
            if(y-i >= 0 && y-i<nbSquare){
                if(x-increment>= 0 && x-increment < nbSquare){
                    //printf("CASE COMPATIBLE cas gauche : X %d | Y %d \n", x-increment, y-i);
                    if(cibleX == (x-increment) && cibleY == y-i){
                        trouve = 1;
                    }
                }
                if(x+increment>= 0 && x+increment < nbSquare){
                    //printf("CASE COMPATIBLE cas droite : X %d | Y %d \n", x+increment, y-i);
                    if(game_map[(y-i)*nbSquare + (x+increment)].player == 1){
                        if(cibleX == x+increment && cibleY == y-i ){
                            trouve = 1;
                        }
                    }
                }
                increment++;
             }
         }
         increment = 1;
         j = i+1;
    }
    j = 1;
    int tampon = range;
        for (int i = 1; i<=range ; i++){ // partie inf�rieur du triangle
            if(y+i >= 0 && y+i<nbSquare){
                //printf("CASE COMPATIBLE : X %d | Y %d \n", x, y+i);
                if(game_map[(y+i)*nbSquare + x].player == 1){
                    if(cibleX == x && cibleY == y+i){
                        trouve = 1;
                    }
                }
            }
            for (j = 1 ; j< tampon; j++){
                if(y+i >= 0 && y+i<nbSquare){
                    if(x-j>= 0 && x-j < nbSquare){
                        //printf("CASE COMPATIBLE : X %d | Y %d \n", x-j, y+i);
                        if(game_map[(y+i)*nbSquare + (x-j)].player == 1){
                            if(cibleX == (x-j) && cibleY == (y+i) ){
                                trouve = 1;
                            }
                        }
                    }
                    if(x+j>= 0 && x+j < nbSquare){
                        //printf("CASE COMPATIBLE : X %d | Y %d \n", x+j, y+i);
                        if(game_map[(y+i)*nbSquare + (x+j)].player == 1){
                            if(cibleX == (x+j) && cibleY == (y+i) ){
                                trouve = 1;
                            }
                        }
                    }
                 }
            }
            tampon --;
        }
    return trouve;
}

int outMap(int x, int nbSquare){

    if(x<0){
        return 0;
    }
    else if(x>= nbSquare){
        return nbSquare-1;
    }
    else{
        return x;
    }

}

void computerTurnMouvementOthers(SDL_Surface* surface, map_square *game_map, GLuint texture_soldier[], player *p,int player, int nbSquare, float aspectRatio, int *fin, int *numFantassin, vector < pair<int,int> > listeFantassin ,int *x, int *y, int *indexTemp, int *directionTemp, int sizeSquare){

    int adversaire;
    if(player == 1){
        adversaire = 0;
    }
    else{
        adversaire = 1;
    }

    int nbFantassin = 0;
    int nbBazooka = 0;
    int nbTank = 0;
    int nbCopter = 0;
    int nbShip = 0;

    int nbAdvFantassin = 0;
    int nbAdvBazooka = 0;
    int nbAdvTank = 0;
    int nbAdvCopter = 0;
    int nbAdvShip = 0;

    for (int i = 0; i<MAXUNITE; i++){

        // ORDINATEUR
        if( (p[player].liste[i].type_soldier-5*player) == 1 ){
            nbFantassin++;
        }
        if( (p[player].liste[i].type_soldier-5*player) == 2 ){
            nbBazooka++;
        }
        if( (p[player].liste[i].type_soldier-5*player) == 3 ){
            nbTank++;
        }
        if( (p[player].liste[i].type_soldier-5*player) == 4 ){
            nbCopter++;
        }
        if( (p[player].liste[i].type_soldier-5*player) == 5 ){
            nbShip++;
        }

        // ADVERSAIRE
        if( (p[adversaire].liste[i].type_soldier-5*adversaire) == 1 ){
            nbAdvFantassin++;
        }
        if( (p[adversaire].liste[i].type_soldier-5*adversaire) == 2 ){
            nbAdvBazooka++;
        }
        if( (p[adversaire].liste[i].type_soldier-5*adversaire) == 3 ){
            nbAdvTank++;
        }
        if( (p[adversaire].liste[i].type_soldier-5*adversaire) == 4 ){
            nbAdvCopter++;
        }
        if( (p[adversaire].liste[i].type_soldier-5*adversaire) == 5 ){
            nbAdvShip++;
        }
    }

    for (int i = 0; i<MAXUNITE; i++){

        if( (p[player].liste[i].type_soldier-5*player) == 1 ){
            nbFantassin++;
        }
        if( (p[player].liste[i].type_soldier-5*player) == 2 ){
            nbBazooka++;
        }
        if( (p[player].liste[i].type_soldier-5*player) == 3 ){
            nbTank++;
        }
        if( (p[player].liste[i].type_soldier-5*player) == 4 ){
            nbCopter++;
        }
        if( (p[player].liste[i].type_soldier-5*player) == 5 ){
            nbShip++;
        }
    }

    for (int i=0; i<MAXUNITE; i++){
        if( p[player].liste[i].cX == listeFantassin[*numFantassin].first && p[player].liste[i].cY == listeFantassin[*numFantassin].second){

            if( p[player].liste[i].fuel > 0 ){
                        printf("============================ ID UNITE : %d %d \n\n", p[player].liste[i].cX, p[player].liste[i].cY);
                        //printf("test %d \n", mapPath(surface, nbSquare, &game_map[0], aspectRatio, 0, 3, 1, 0));
                        vector < villeIA > listeSoldat;
                        listeSoldat.clear();
                        int nbDeSoldat = 0;
                        for(int k = 0; k<MAXUNITE; k++){
                            if( (p[0].liste[k].type_soldier) == 1 ){
                                nbDeSoldat++;
                                villeIA temp;
                                temp.x = p[0].liste[k].cX;
                                temp.y = p[0].liste[k].cY;
                                temp.etat = 1;
                                villeIA temp1 = isRange(p[player].liste[i].cX, p[player].liste[i].cY, temp.x, temp.y, p[player].liste[i].range, nbSquare, aspectRatio, surface, &game_map[0], i, &p[0], player ) ;
                                temp.cible = temp1.etat;
                                listeSoldat.push_back(temp);
                            }
                            if( (p[0].liste[k].type_soldier) == 2 ){
                                nbDeSoldat++;
                                villeIA temp;
                                temp.x = p[0].liste[k].cX;
                                temp.y = p[0].liste[k].cY;
                                temp.etat = 2;
                                villeIA temp1 = isRange(p[player].liste[i].cX, p[player].liste[i].cY, temp.x, temp.y, p[player].liste[i].range, nbSquare, aspectRatio, surface, &game_map[0], i, &p[0], player ) ;
                                temp.cible = temp1.etat;
                                listeSoldat.push_back(temp);
                            }
                            if( (p[0].liste[k].type_soldier) == 3 ){
                                nbDeSoldat++;
                                villeIA temp;
                                temp.x = p[0].liste[k].cX;
                                temp.y = p[0].liste[k].cY;
                                temp.etat = 3;
                                villeIA temp1 = isRange(p[player].liste[i].cX, p[player].liste[i].cY, temp.x, temp.y, p[player].liste[i].range, nbSquare, aspectRatio, surface, &game_map[0], i, &p[0], player ) ;


                                temp.cible = temp1.etat;
                                listeSoldat.push_back(temp);
                            }
                            if( (p[0].liste[k].type_soldier) == 4 ){
                                nbDeSoldat++;
                                villeIA temp;
                                temp.x = p[0].liste[k].cX;
                                temp.y = p[0].liste[k].cY;
                                temp.etat = 4;
                                //printf("avant isRange \n");
                                villeIA temp1 = isRange(p[player].liste[i].cX, p[player].liste[i].cY, temp.x, temp.y, p[player].liste[i].range, nbSquare, aspectRatio, surface, &game_map[0], i, &p[0], player ) ;
                                //printf("apr�s isRange \n");
                                temp.cible = temp1.etat;
                                listeSoldat.push_back(temp);
                            }
                            if( (p[0].liste[k].type_soldier) == 5 ){
                                nbDeSoldat++;
                                villeIA temp;
                                temp.x = p[0].liste[k].cX;
                                temp.y = p[0].liste[k].cY;
                                temp.etat = 5;
                                villeIA temp1 = isRange(p[player].liste[i].cX, p[player].liste[i].cY, temp.x, temp.y, p[player].liste[i].range, nbSquare, aspectRatio, surface, &game_map[0], i, &p[0], player ) ;
                                //printf("SHIP : %d %d \n", temp1.x, temp1.y);
                                temp.cible = temp1.etat;

                                listeSoldat.push_back(temp);
                            }
                        }
                        //printf("AFFICHAGE SOLDAT \n");
                        int meilleur = 0;
                        for (int k = 0; k<listeSoldat.size(); k++ ){
                            if(listeSoldat[meilleur].cible>listeSoldat[k].cible){
                                meilleur = k;
                            }
                            //printf("liste soldat : %d %d distance %d type %d\n", listeSoldat[k].x, listeSoldat[k].y, listeSoldat[k].cible, listeSoldat[k].etat );
                        }
                        printf("Meilleur adversaire : %d \n", meilleur);
                        //printf("Case destination %d %d | possesseur : %d \n", listeSoldat[meilleur].x, listeSoldat[meilleur].y, game_map[listeSoldat[meilleur].y * nbSquare + listeSoldat[meilleur].x].player );

                        if(listeSoldat.size()!=0){

                            printf("---------------------- ennemi à porté ? %d \n", toRange(p[player].liste[i].cX, p[player].liste[i].cY, p[player].liste[i].range , nbSquare, &game_map[0]));
                            if(toRange(p[player].liste[i].cX, p[player].liste[i].cY, p[player].liste[i].range , nbSquare, &game_map[0]) > 0){
                                int res = findTarget(p[player].liste[i].cX ,p[player].liste[i].cY, listeSoldat[meilleur].x,listeSoldat[meilleur].y,p[player].liste[i].range , nbSquare, &game_map[0]);
                                printf("RES : %d \n", res);
                                attaque(&game_map[0], &p[0], player, p[player].liste[i].cX, p[player].liste[i].cY, listeSoldat[meilleur].x, listeSoldat[meilleur].y, nbSquare);
                                *fin = 0;
                                *numFantassin = *numFantassin +1;
                                printf("numFantassin = %d \n\n", *numFantassin);
                                i = MAXUNITE;

                            }
                            else if( p[player].liste[i].type_soldier != 10 ){
                                villeIA temp1 = isRange(p[player].liste[i].cX, p[player].liste[i].cY,listeSoldat[meilleur].x, listeSoldat[meilleur].y, p[player].liste[i].range, nbSquare, aspectRatio, surface, &game_map[0], i, &p[0], player);
                                printf("temp1.x %d et temp1.y %d \n", temp1.x, temp1.y);
                                if(temp1.x == -1 && temp1.y == -1){
                                    printf("\n ne devrait pas bouger \n");
                                    *fin = 0;
                                    *numFantassin = *numFantassin +1;
                                }
                                else{
                                    printf("doit bouger \n");
                                    int destX;
                                    int destY;
                                    if( !(p[player].liste[i].cX == temp1.x && p[player].liste[i].cY == temp1.y) ){
                                        vector < pair<int,int> > path = vectorMapPath(surface, nbSquare, &game_map[0], aspectRatio, p[player].liste[i].cX, p[player].liste[i].cY, temp1.x, temp1.y);
                                        float distance = mapPath(surface, nbSquare, &game_map[0], aspectRatio, p[player].liste[i].cX, p[player].liste[i].cY, temp1.x, temp1.y);
                                        printf("DISTANCE !!!! %f \n", distance );
                                        printf("TAILLE DE PATH !!! %d \n", path.size() );
                                        std::reverse(path.begin(), path.end());
                                        int k = 0;
                                        int trouve = 0;
                                        destX = path[k].first;
                                        destY = path[k].second;
                                        while(trouve == 0 && k<path.size()){
                                            printf("recherche de la case intermediaire max \n");
                                            k++;
                                            distance = mapPath(surface, nbSquare, &game_map[0], aspectRatio, p[player].liste[i].cX, p[player].liste[i].cY, path[k].first, path[k].second);
                                            if(distance != -1 && distance<= p[player].liste[i].fuel){
                                                destX = path[k].first;
                                                destY = path[k].second;
                                                printf("--- distance %f \n", distance);
                                            }
                                            else{
                                                trouve = 1;
                                            }
                                        }
                                        printf("--- destX et destY : %d %d \n", destX, destY);
                                        if( !(*x == destX*sizeSquare + sizeSquare/2 && *y == destY*sizeSquare + sizeSquare/2)){
                                            std::reverse(path.begin(), path.end());
                                            animationMouvement(&game_map[0],
                                                        surface,
                                                        texture_soldier,
                                                        path,
                                                        indexTemp,
                                                        x,
                                                        y,
                                                        directionTemp,
                                                        sizeSquare,
                                                        aspectRatio,
                                                        nbSquare,
                                                        p[player].liste[i].type_soldier,
                                                        fin,
                                                        &p[0],
                                                        1);
                                        }
                                        else{
                                            printf("************* MOUVEMENT dans fonction ordinateur \n");
                                            printf("************* Depart : %d %d | Arrivé : %d %d \n", p[player].liste[i].cX, p[player].liste[i].cY, destX, destY );
                                            unitMouvement(surface, &game_map[0], &p[0], player, p[player].liste[i].cX, p[player].liste[i].cY, destX, destY, nbSquare, aspectRatio);
                                            *fin = 0;
                                            *numFantassin = *numFantassin +1;
                                            i = MAXUNITE;

                                        }
                                    }
                                }
                            }
                            else{
                                printf("SHIP ne bouge pas ! \n");
                                *fin = 0;
                                *numFantassin = *numFantassin +1;
                                i = MAXUNITE;
                            }
                        }
                }
                else{
                    *fin = 0;
                    *numFantassin = *numFantassin +1;
                    i = MAXUNITE;
                }
            }
        }
    }

