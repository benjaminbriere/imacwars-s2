/////////////////////////////////////////////////
//                   MAP.H                     //
//                                             //
//     by Gartner Esther & Briere Benjamin     //
//                                             //
/////////////////////////////////////////////////

void drawSquare(int width, int height, int x, int y, int sizeW, int sizeH, float aspectRatio);
void draw_map_square(float aspectRatio, int width, int height, int x, int y, int sizeW, int sizeH, int type, GLuint texture_map[]);
void generateTexture(SDL_Surface* image, GLuint *texture_id);
void generateTextureTransparant(SDL_Surface* image, GLuint *texture_id);
int ground(int red, int green, int blue);
Uint32 getpixel(SDL_Surface *surface, int x, int y);
void mapCreator(SDL_Surface* surface, GLuint texture_map[], int nbSquare, map_square *game_map);
void mapDisplay(SDL_Surface* surface, GLuint texture_map[], int nbSquare, map_square *game_map, float aspectRatio);
void squareInfo(map_square *game_map, int x, int y, int nbSquare, SDL_Surface* surface);
void nullMap( int nbSquare, map_square *game_map);
int squareFree(map_square *game_map, int x, int y, int nbSquare, SDL_Surface* surface, int typeUnit);
void nullRange( int nbSquare, map_range *game_range);
void displayRangeMap(SDL_Surface* surface, int nbSquare, map_range *game_range, float aspectRatio);
void displayDeplacementMap(SDL_Surface* surface, int nbSquare, map_range *game_range, float aspectRatio);
