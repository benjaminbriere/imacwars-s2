/////////////////////////////////////////////////
//                  MENU.CPP                   //
//                                             //
//     by Gartner Esther & Briere Benjamin     //
//                                             //
/////////////////////////////////////////////////

#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <iostream>

using namespace std;

#include "structures.h"
#include "map.h"
#include "unit.h"
#include "menu.h"
#include "player.h"
//#include "game.cpp"

#define MAXUNITE 10
#define NBUNITE 5

void menuUnit(){

}

void lifeBar(int width, int height, int x, int y, int sizeW, int sizeH, float aspectRatio, int life){

    if (life>50){
        glColor3ub(0, 255, 0);
    }
    else if (life>25){
        glColor3ub(240,212, 56);
    }
    else{
        glColor3ub(255,0,0);
    }

  glBegin(GL_QUADS);
    if (aspectRatio > 1){
      glTexCoord2f(0, 1);
      glVertex2f(
        (( -1 + 2. * (x-1*sizeW/2) / (float) width )* aspectRatio),
        -(-1 + 2. * (y+1*sizeH/2) / (float) height));
      glTexCoord2f(1, 1);
      glVertex2f(
        (( -1 + 2. * ((x-1*sizeW/2)+(life*sizeW/100)) / (float) width) * aspectRatio),
        -(-1 + 2. * (y+1*sizeH/2) / (float) height));
      glTexCoord2f(1, 0);
      glVertex2f(
        (( -1 + 2. * ((x-1*sizeW/2)+(life*sizeW/100)) / (float) width) * aspectRatio),
        -(-1 + 2. * (y-1*sizeH/2) / (float) height));
      glTexCoord2f(0, 0);
      glVertex2f(
        (( -1 + 2. * (x-1*sizeW/2) / (float) width) * aspectRatio),
        -(-1 + 2. * (y-1*sizeH/2) / (float) height));
    }
    if (aspectRatio  == 1){
      glTexCoord2f(0, 1);
      glVertex2f(
        (( -1 + 2. * (x-1*sizeW/2) / (float) width )),
        -(-1 + 2. * (y+1*sizeH/2) / (float) height));
      glTexCoord2f(1, 1);
      glVertex2f(
        (( -1 + 2. * (x+1*sizeW/2) / (float) width )),
        -(-1 + 2. * (y+1*sizeH/2) / (float) height));
      glTexCoord2f(1, 0);
      glVertex2f(
        (( -1 + 2. * (x+1*sizeW/2) / (float) width )),
        -(-1 + 2. * (y-1*sizeH/2) / (float) height));
      glTexCoord2f(0, 0);
      glVertex2f(
        (( -1 + 2. * (x-1*sizeW/2) / (float) width )),
        -(-1 + 2. * (y-1*sizeH/2) / (float) height));
    }
    if (aspectRatio  < 1){
      glTexCoord2f(0, 1);
      glVertex2f(
        (( -1 + 2. * (x-1*sizeW/2) / (float) width )),
        -(-1 + 2. * (y+1*sizeH/2) / (float) height) / aspectRatio );
      glTexCoord2f(1, 1);
      glVertex2f(
        (( -1 + 2. * (x+1*sizeW/2) / (float) width )),
        -(-1 + 2. * (y+1*sizeH/2) / (float) height) / aspectRatio );
      glTexCoord2f(1, 0);
      glVertex2f(
        (( -1 + 2. * (x+1*sizeW/2) / (float) width )),
        -(-1 + 2. * (y-1*sizeH/2) / (float) height) / aspectRatio );
      glTexCoord2f(0, 0);
      glVertex2f(
        (( -1 + 2. * (x-1*sizeW/2) / (float) width )),
        -(-1 + 2. * (y-1*sizeH/2) / (float) height) / aspectRatio );
    }
  glEnd();
}

void displayStat(SDL_Surface* surface, GLuint texture_soldier[], int nbSquare,  float aspectRatio, int player, GLuint texture_stat[], int sizeSquare, int type){
        if (type >= 1 && type <= 10){
        glEnable(GL_TEXTURE_2D);
        glBindTexture(GL_TEXTURE_2D, texture_stat[player*5+(type-1)]);
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        draw_map_square(aspectRatio,
                        surface->w,
                        surface->h,
                        nbSquare*sizeSquare + (surface->w-(nbSquare*sizeSquare))/2,
                        surface->h/2 - (23* (surface->h/2) /100),
                        80*(surface->w-(nbSquare*sizeSquare))/100,
                        surface->h/4,
                        0,
                        texture_soldier );
        glDisable(GL_BLEND);
        glBindTexture(GL_TEXTURE_2D, 0);
        glDisable(GL_TEXTURE_2D);
        }
        else{
            if(player==0){
                glEnable(GL_TEXTURE_2D);
                glBindTexture(GL_TEXTURE_2D, texture_stat[10]);
                glEnable(GL_BLEND);
                glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
                draw_map_square(aspectRatio,
                                surface->w,
                                surface->h,
                                nbSquare*sizeSquare + (surface->w-(nbSquare*sizeSquare))/2,
                                surface->h/2 - (23* (surface->h/2) /100),
                                80*(surface->w-(nbSquare*sizeSquare))/100,
                                surface->h/4,
                                0,
                                texture_soldier );
                glDisable(GL_BLEND);
                glBindTexture(GL_TEXTURE_2D, 0);
                glDisable(GL_TEXTURE_2D);
            }
            if(player==1){
                glEnable(GL_TEXTURE_2D);
                glBindTexture(GL_TEXTURE_2D, texture_stat[11]);
                glEnable(GL_BLEND);
                glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
                draw_map_square(aspectRatio,
                                surface->w,
                                surface->h,
                                nbSquare*sizeSquare + (surface->w-(nbSquare*sizeSquare))/2,
                                surface->h/2 - (23* (surface->h/2) /100),
                                80*(surface->w-(nbSquare*sizeSquare))/100,
                                surface->h/4,
                                0,
                                texture_soldier );
                glDisable(GL_BLEND);
                glBindTexture(GL_TEXTURE_2D, 0);
                glDisable(GL_TEXTURE_2D);
            }
        }

}

void displayCapture(SDL_Surface* surface, GLuint texture_soldier[], GLuint texture_button[], map_square *game_map, int nbSquare,  float aspectRatio, int pl, int sizeSquare, int x, int y){
    //printf("RAPPORT CAPTURE : \n");
    int available;
    if (x == -1 || y == -1){
        available = 13;
    }
    else{
        x = x / sizeSquare;
        y = y / sizeSquare;
        if((game_map[y*nbSquare + x].type_soldier == 1 || game_map[y*nbSquare + x].type_soldier == 6 ) && game_map[y*nbSquare + x].action == 1){ // fantassin
          if(game_map[y*nbSquare + x].ground == 7) // case ville neutre
          {
              //printf("Ville neutre de coordonn�es %d | %d: \n", x, y);
              available = 0;
          }
          else{
            //printf("Ville non neutre de coordonn�es %d | %d: \n", x, y);
            if( pl == 0){
                if(game_map[y*nbSquare + x].ground == 9) // case ville bleue
                {
                    available = 0;
                }
                else{
                    available = 13;
                }
            }
            else{
                if(game_map[y*nbSquare + x].ground == 8) // case ville rouge
                {
                    available = 0;
                }
                else{
                    available = 13;
                }
            }
          }
        }
        else{ // pas fantassin
            //printf("non Unit� fantassin : \n");
            available = 13;
        }
    }
    draw_map_square(aspectRatio,
                    surface->w,
                    surface->h,
                    nbSquare*sizeSquare + (surface->w-(nbSquare*sizeSquare))/2,
                    surface->h/2 + (37* (surface->h/2) /100),
                    80*(surface->w-(nbSquare*sizeSquare))/100,
                    surface->h/15,
                    available,
                    texture_soldier );
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, texture_button[pl*6 + 1]);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    draw_map_square(aspectRatio,
                    surface->w,
                    surface->h,
                    nbSquare*sizeSquare + (surface->w-(nbSquare*sizeSquare))/2,
                    surface->h/2 + (37* (surface->h/2) /100),
                    80*(surface->w-(nbSquare*sizeSquare))/100,
                    surface->h/15,
                    0,
                    texture_soldier);
    glDisable(GL_BLEND);
    glBindTexture(GL_TEXTURE_2D, 0);
    glDisable(GL_TEXTURE_2D);

}

void displayAttackOrPurchase(SDL_Surface* surface, GLuint texture_soldier[], int nbSquare,  float aspectRatio, int pl, GLuint texture_stat[], int sizeSquare, int type, int mode, player *p, GLuint texture_button[]){

    int cost;
    if(type == 1)
    {
        cost = 500;
    }
    if(type == 2)
    {
        cost = 1000;
    }
    if(type == 3 || type == 4)
    {
        cost = 1500;
    }
    if(type == 5)
    {
        cost = 2000;
    }
    int available = 0;

    if (mode == 0){
        if(p[pl].gold<cost){
            available = 13;
        }
        else{
            available = 0;
        }
    }

    draw_map_square(aspectRatio,
                    surface->w,
                    surface->h,
                    nbSquare*sizeSquare + (surface->w-(nbSquare*sizeSquare))/2,
                    surface->h/2 + (22* (surface->h/2) /100),
                    80*(surface->w-(nbSquare*sizeSquare))/100,
                    surface->h/15,
                    available,
                    texture_soldier );

    glEnable(GL_TEXTURE_2D);
    if (mode == 0){
        glBindTexture(GL_TEXTURE_2D, texture_button[pl*6+5]);
    }
    else{
        glBindTexture(GL_TEXTURE_2D, texture_button[pl*6]);
    }
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    draw_map_square(aspectRatio,
                    surface->w,
                    surface->h,
                    nbSquare*sizeSquare + (surface->w-(nbSquare*sizeSquare))/2,
                    surface->h/2 + (22* (surface->h/2) /100),
                    80*(surface->w-(nbSquare*sizeSquare))/100,
                    surface->h/15,
                    0,
                    texture_soldier );
    glDisable(GL_BLEND);
    glBindTexture(GL_TEXTURE_2D, 0);
    glDisable(GL_TEXTURE_2D);
}

void menuDisplay(SDL_Surface* surface, GLuint texture_soldier[], int nbSquare, map_square *game_map,
                  player *p, float aspectRatio, int player, GLuint texture_figure[], GLuint texture_button[], GLuint texture_stat[]){
    int spaceBetween = 0;
    int sizeSquare;
    if(surface->w / nbSquare > (surface->h / nbSquare)){
        sizeSquare = (surface->h - (nbSquare-1)*spaceBetween) / nbSquare;
    }
    else{
        sizeSquare = (surface->w - (nbSquare-1)*spaceBetween) / nbSquare;
    }
    // MENU STANDARD

    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, texture_button[23+player]);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    draw_map_square(aspectRatio,
                    surface->w,
                    surface->h,
                    nbSquare*sizeSquare + (surface->w-(nbSquare*sizeSquare))/2,
                    surface->h / 2,
                    surface->w-(nbSquare*sizeSquare),
                    surface->h,
                    12,
                    texture_soldier );
    glDisable(GL_BLEND);
    glBindTexture(GL_TEXTURE_2D, 0);
    glDisable(GL_TEXTURE_2D);

    // AFFICHAGE GOLD ACHAT
    draw_map_square(aspectRatio,
                    surface->w,
                    surface->h,
                    nbSquare*sizeSquare + (surface->w-(nbSquare*sizeSquare))/2,
                    surface->h/2 - (90* (surface->h/2) /100),
                    80*(surface->w-(nbSquare*sizeSquare))/100,
                    surface->h/15,
                    0,
                    texture_soldier );

    int gold = p[player].gold;
    int milliers = (gold-(gold%1000))/1000;
    int centaines = ((gold-milliers*1000)-((gold%1000)%100))/100;
    int dizaines = (((gold-milliers*1000 - centaines * 100))- (((gold%1000)%100)%10))/10  ;
    int unites   = (((gold%1000)%100)%10);




    // MILLIERS
    draw_map_square(aspectRatio,
                    surface->w,
                    surface->h,
                    nbSquare*sizeSquare + (surface->w-(nbSquare*sizeSquare))/2-(((80*(surface->w-(nbSquare*sizeSquare))/100)/2.7)),
                    surface->h/2 - (90* (surface->h/2) /100),
                    (80*(surface->w-(nbSquare*sizeSquare))/100)/4.4,
                    (surface->h/15)-4,
                    12,
                    texture_soldier );


    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, texture_figure[milliers]);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    draw_map_square(aspectRatio,
                    surface->w,
                    surface->h,
                    nbSquare*sizeSquare + (surface->w-(nbSquare*sizeSquare))/2-(((80*(surface->w-(nbSquare*sizeSquare))/100)/2.7)),
                    surface->h/2 - (90* (surface->h/2) /100),
                    (80*(surface->w-(nbSquare*sizeSquare))/100)/4.4,
                    (surface->h/15)-4,
                    12,
                    texture_soldier );
    glDisable(GL_BLEND);
    glBindTexture(GL_TEXTURE_2D, 0);
    glDisable(GL_TEXTURE_2D);
    // CENTAINES

    draw_map_square(aspectRatio,
                    surface->w,
                    surface->h,
                    nbSquare*sizeSquare + (surface->w-(nbSquare*sizeSquare))/2-(((80*(surface->w-(nbSquare*sizeSquare))/100)/8)),
                    surface->h/2 - (90* (surface->h/2) /100),
                    (80*(surface->w-(nbSquare*sizeSquare))/100)/4.4,
                    (surface->h/15)-4,
                    12,
                    texture_soldier );

    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, texture_figure[centaines]);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    draw_map_square(aspectRatio,
                    surface->w,
                    surface->h,
                    nbSquare*sizeSquare + (surface->w-(nbSquare*sizeSquare))/2-(((80*(surface->w-(nbSquare*sizeSquare))/100)/8)),
                    surface->h/2 - (90* (surface->h/2) /100),
                    (80*(surface->w-(nbSquare*sizeSquare))/100)/4.4,
                    (surface->h/15)-4,
                    12,
                    texture_soldier );
    glDisable(GL_BLEND);
    glBindTexture(GL_TEXTURE_2D, 0);
    glDisable(GL_TEXTURE_2D);


    // DIZAINES

    draw_map_square(aspectRatio,
                    surface->w,
                    surface->h,
                    nbSquare*sizeSquare + (surface->w-(nbSquare*sizeSquare))/2+(((80*(surface->w-(nbSquare*sizeSquare))/100)/8)),
                    surface->h/2 - (90* (surface->h/2) /100),
                    (80*(surface->w-(nbSquare*sizeSquare))/100)/4.4,
                    (surface->h/15)-4,
                    12,
                    texture_soldier );

    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, texture_figure[dizaines]);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    draw_map_square(aspectRatio,
                    surface->w,
                    surface->h,
                    nbSquare*sizeSquare + (surface->w-(nbSquare*sizeSquare))/2+(((80*(surface->w-(nbSquare*sizeSquare))/100)/8)),
                    surface->h/2 - (90* (surface->h/2) /100),
                    (80*(surface->w-(nbSquare*sizeSquare))/100)/4.4,
                    (surface->h/15)-4,
                    12,
                    texture_soldier );
    glDisable(GL_BLEND);
    glBindTexture(GL_TEXTURE_2D, 0);
    glDisable(GL_TEXTURE_2D);

    // UNITES

    draw_map_square(aspectRatio,
                    surface->w,
                    surface->h,
                    nbSquare*sizeSquare + (surface->w-(nbSquare*sizeSquare))/2+(((80*(surface->w-(nbSquare*sizeSquare))/100)/2.7)),
                    surface->h/2 - (90* (surface->h/2) /100),
                    (80*(surface->w-(nbSquare*sizeSquare))/100)/4.4,
                    (surface->h/15)-4,
                    12,
                    texture_soldier );

    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, texture_figure[unites]);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    draw_map_square(aspectRatio,
                    surface->w,
                    surface->h,
                    nbSquare*sizeSquare + (surface->w-(nbSquare*sizeSquare))/2+(((80*(surface->w-(nbSquare*sizeSquare))/100)/2.7)),
                    surface->h/2 - (90* (surface->h/2) /100),
                    (80*(surface->w-(nbSquare*sizeSquare))/100)/4.4,
                    (surface->h/15)-4,
                    12,
                    texture_soldier );
    glDisable(GL_BLEND);
    glBindTexture(GL_TEXTURE_2D, 0);
    glDisable(GL_TEXTURE_2D);


    // AFFICHAGE UNITE ACHAT

//SOLDAT ROUGE
        int available;
        if( p[player].gold < 500 ){
           available = 13;
        }
        else{
            available = 0;
        }

        draw_map_square(aspectRatio,
                    surface->w,
                    surface->h,
                    nbSquare*sizeSquare + (surface->w-(nbSquare*sizeSquare))/2-(((80*(surface->w-(nbSquare*sizeSquare))/100)/3)),
                    surface->h/2 - (65* (surface->h/2) /100)-((((80*(surface->w-(nbSquare*sizeSquare))/100)/3))/2),
                    (80*(surface->w-(nbSquare*sizeSquare))/100)/3,
                    (80*(surface->w-(nbSquare*sizeSquare))/100)/3,
                    available,
                    texture_soldier );
        glEnable(GL_TEXTURE_2D);
        glBindTexture(GL_TEXTURE_2D, texture_soldier[player*NBUNITE+0]);
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        draw_map_square(aspectRatio,
                    surface->w,
                    surface->h,
                    nbSquare*sizeSquare + (surface->w-(nbSquare*sizeSquare))/2-(((80*(surface->w-(nbSquare*sizeSquare))/100)/3)),
                    surface->h/2 - (65* (surface->h/2) /100)-((((80*(surface->w-(nbSquare*sizeSquare))/100)/3))/2),
                    (80*(surface->w-(nbSquare*sizeSquare))/100)/3,
                    (80*(surface->w-(nbSquare*sizeSquare))/100)/3,
                    available,
                    texture_soldier );
        glDisable(GL_BLEND);
        glBindTexture(GL_TEXTURE_2D, 0);
        glDisable(GL_TEXTURE_2D);

//BAZOOKA ROUGE


        if( p[player].gold < 1000 ){
           available = 13;
        }
        else{
            available = 0;
        }

        draw_map_square(aspectRatio,
                    surface->w,
                    surface->h,
                    nbSquare*sizeSquare + (surface->w-(nbSquare*sizeSquare))/2,
                    surface->h/2 - (65* (surface->h/2) /100)-((((80*(surface->w-(nbSquare*sizeSquare))/100)/3))/2),
                    (80*(surface->w-(nbSquare*sizeSquare))/100)/3,
                    (80*(surface->w-(nbSquare*sizeSquare))/100)/3,
                    available,
                    texture_soldier );
        glEnable(GL_TEXTURE_2D);
        glBindTexture(GL_TEXTURE_2D, texture_soldier[player*NBUNITE+1]);
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        draw_map_square(aspectRatio,
                    surface->w,
                    surface->h,
                    nbSquare*sizeSquare + (surface->w-(nbSquare*sizeSquare))/2,
                    surface->h/2 - (65* (surface->h/2) /100)-((((80*(surface->w-(nbSquare*sizeSquare))/100)/3))/2),
                    (80*(surface->w-(nbSquare*sizeSquare))/100)/3,
                    (80*(surface->w-(nbSquare*sizeSquare))/100)/3,
                    available,
                    texture_soldier );
        glDisable(GL_BLEND);
        glBindTexture(GL_TEXTURE_2D, 0);
        glDisable(GL_TEXTURE_2D);

//TANK ROUGE

        if( p[player].gold < 1500 ){
           available = 13;
        }
        else{
            available = 0;
        }

        draw_map_square(aspectRatio,
                    surface->w,
                    surface->h,
                    nbSquare*sizeSquare + (surface->w-(nbSquare*sizeSquare))/2+(((80*(surface->w-(nbSquare*sizeSquare))/100)/3)),
                    surface->h/2 - (65* (surface->h/2) /100)-((((80*(surface->w-(nbSquare*sizeSquare))/100)/3))/2),
                    (80*(surface->w-(nbSquare*sizeSquare))/100)/3,
                    (80*(surface->w-(nbSquare*sizeSquare))/100)/3,
                    available,
                    texture_soldier );
        glEnable(GL_TEXTURE_2D);
        glBindTexture(GL_TEXTURE_2D, texture_soldier[player*NBUNITE+2]);
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        draw_map_square(aspectRatio,
                    surface->w,
                    surface->h,
                    nbSquare*sizeSquare + (surface->w-(nbSquare*sizeSquare))/2+(((80*(surface->w-(nbSquare*sizeSquare))/100)/3)),
                    surface->h/2 - (65* (surface->h/2) /100)-((((80*(surface->w-(nbSquare*sizeSquare))/100)/3))/2),
                    (80*(surface->w-(nbSquare*sizeSquare))/100)/3,
                    (80*(surface->w-(nbSquare*sizeSquare))/100)/3,
                    available,
                    texture_soldier );
        glDisable(GL_BLEND);
        glBindTexture(GL_TEXTURE_2D, 0);
        glDisable(GL_TEXTURE_2D);

// HELICOPTER ROUGE


        if( p[player].gold < 1500 ){
           available = 13;
        }
        else{
            available = 0;
        }

        draw_map_square(aspectRatio,
                    surface->w,
                    surface->h,
                    nbSquare*sizeSquare + (surface->w-(nbSquare*sizeSquare))/2-(((80*(surface->w-(nbSquare*sizeSquare))/100)/3)),
                    surface->h/2 - (65* (surface->h/2) /100)+((((80*(surface->w-(nbSquare*sizeSquare))/100)/3))/2)+1,
                    (80*(surface->w-(nbSquare*sizeSquare))/100)/3,
                    (80*(surface->w-(nbSquare*sizeSquare))/100)/3,
                    available,
                    texture_soldier );
        glEnable(GL_TEXTURE_2D);
        glBindTexture(GL_TEXTURE_2D, texture_soldier[player*NBUNITE+3]);
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        draw_map_square(aspectRatio,
                    surface->w,
                    surface->h,
                    nbSquare*sizeSquare + (surface->w-(nbSquare*sizeSquare))/2-(((80*(surface->w-(nbSquare*sizeSquare))/100)/3)),
                    surface->h/2 - (65* (surface->h/2) /100)+((((80*(surface->w-(nbSquare*sizeSquare))/100)/3))/2)+1,
                    (80*(surface->w-(nbSquare*sizeSquare))/100)/3,
                    (80*(surface->w-(nbSquare*sizeSquare))/100)/3,
                    available,
                    texture_soldier );
        glDisable(GL_BLEND);
        glBindTexture(GL_TEXTURE_2D, 0);
        glDisable(GL_TEXTURE_2D);

// SHIP ROUGE


        if( p[player].gold < 2000 ){
           available = 13;
        }
        else{
            available = 0;
        }

        draw_map_square(aspectRatio,
                    surface->w,
                    surface->h,
                    nbSquare*sizeSquare + (surface->w-(nbSquare*sizeSquare))/2,
                    surface->h/2 - (65* (surface->h/2) /100)+((((80*(surface->w-(nbSquare*sizeSquare))/100)/3))/2)+1,
                    (80*(surface->w-(nbSquare*sizeSquare))/100)/3,
                    (80*(surface->w-(nbSquare*sizeSquare))/100)/3,
                    available,
                    texture_soldier );
        glEnable(GL_TEXTURE_2D);
        glBindTexture(GL_TEXTURE_2D, texture_soldier[player*NBUNITE+4]);
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        draw_map_square(aspectRatio,
                    surface->w,
                    surface->h,
                    nbSquare*sizeSquare + (surface->w-(nbSquare*sizeSquare))/2,
                    surface->h/2 - (65* (surface->h/2) /100)+((((80*(surface->w-(nbSquare*sizeSquare))/100)/3))/2)+1,
                    (80*(surface->w-(nbSquare*sizeSquare))/100)/3,
                    (80*(surface->w-(nbSquare*sizeSquare))/100)/3,
                    available,
                    texture_soldier );
        glDisable(GL_BLEND);
        glBindTexture(GL_TEXTURE_2D, 0);
        glDisable(GL_TEXTURE_2D);

    // INFO UNITE

        draw_map_square(aspectRatio,
                        surface->w,
                        surface->h,
                        nbSquare*sizeSquare + (surface->w-(nbSquare*sizeSquare))/2,
                        surface->h/2 - (23* (surface->h/2) /100),
                        80*(surface->w-(nbSquare*sizeSquare))/100,
                        surface->h/4,
                        0,
                        texture_soldier );



    // VIE

        draw_map_square(aspectRatio,
                        surface->w,
                        surface->h,
                        nbSquare*sizeSquare + (surface->w-(nbSquare*sizeSquare))/2,
                        surface->h/2 + (7* (surface->h/2) /100),
                        80*(surface->w-(nbSquare*sizeSquare))/100,
                        surface->h/35,
                        0,
                        texture_soldier );



    // ACTION UNITE

    // CAPTURER //

    // SUPER POUVOIR

    draw_map_square(aspectRatio,
                    surface->w,
                    surface->h,
                    nbSquare*sizeSquare + (surface->w-(nbSquare*sizeSquare))/2,
                    surface->h/2 + (52* (surface->h/2) /100),
                    80*(surface->w-(nbSquare*sizeSquare))/100,
                    surface->h/15,
                    0,
                    texture_soldier );
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, texture_button[player*6 + 2]);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    draw_map_square(aspectRatio,
                    surface->w,
                    surface->h,
                    nbSquare*sizeSquare + (surface->w-(nbSquare*sizeSquare))/2,
                    surface->h/2 + (52* (surface->h/2) /100),
                    80*(surface->w-(nbSquare*sizeSquare))/100,
                    surface->h/15,
                    0,
                    texture_soldier );
    glDisable(GL_BLEND);
    glBindTexture(GL_TEXTURE_2D, 0);
    glDisable(GL_TEXTURE_2D);


    // FIN DE TOUR

    draw_map_square(aspectRatio,
                    surface->w,
                    surface->h,
                    nbSquare*sizeSquare + (surface->w-(nbSquare*sizeSquare))/2,
                    surface->h/2 + (75* (surface->h/2) /100),
                    80*(surface->w-(nbSquare*sizeSquare))/100,
                    surface->h/15,
                    0,
                    texture_soldier );
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, texture_button[player*6 + 3]);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    draw_map_square(aspectRatio,
                    surface->w,
                    surface->h,
                    nbSquare*sizeSquare + (surface->w-(nbSquare*sizeSquare))/2,
                    surface->h/2 + (75* (surface->h/2) /100),
                    80*(surface->w-(nbSquare*sizeSquare))/100,
                    surface->h/15,
                    0,
                    texture_soldier );
    glDisable(GL_BLEND);
    glBindTexture(GL_TEXTURE_2D, 0);
    glDisable(GL_TEXTURE_2D);

    // ABANDON
    draw_map_square(aspectRatio,
                    surface->w,
                    surface->h,
                    nbSquare*sizeSquare + (surface->w-(nbSquare*sizeSquare))/2,
                    surface->h/2 + (90* (surface->h/2) /100),
                    80*(surface->w-(nbSquare*sizeSquare))/100,
                    surface->h/15,
                    0,
                    texture_soldier );
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, texture_button[player*6 + 4]);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    draw_map_square(aspectRatio,
                    surface->w,
                    surface->h,
                    nbSquare*sizeSquare + (surface->w-(nbSquare*sizeSquare))/2,
                    surface->h/2 + (90* (surface->h/2) /100),
                    80*(surface->w-(nbSquare*sizeSquare))/100,
                    surface->h/15,
                    0,
                    texture_soldier );
    glDisable(GL_BLEND);
    glBindTexture(GL_TEXTURE_2D, 0);
    glDisable(GL_TEXTURE_2D);

}

