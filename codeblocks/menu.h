/////////////////////////////////////////////////
//                   MENU.H                    //
//                                             //
//     by Gartner Esther & Briere Benjamin     //
//                                             //
/////////////////////////////////////////////////

void menuDisplay(SDL_Surface* surface,
                 GLuint texture_soldier[],
                 int nbSquare,
                 map_square *game_map,
                 player *game_player,
                 float aspectRatio,
                 int player,
                 GLuint texture_figure[],
                 GLuint texture_button[],
                 GLuint texture_stat[]);

void lifeBar(int width, int height, int x, int y, int sizeW, int sizeH, float aspectRatio, int life);
void displayStat(SDL_Surface* surface, GLuint texture_soldier[], int nbSquare,  float aspectRatio, int player, GLuint texture_stat[], int sizeSquare, int type);
void displayAttackOrPurchase(SDL_Surface* surface, GLuint texture_soldier[], int nbSquare,  float aspectRatio, int pl, GLuint texture_stat[], int sizeSquare, int type, int mode, player *p, GLuint texture_button[]);
void displayCapture(SDL_Surface* surface, GLuint texture_soldier[], GLuint texture_button[], map_square *game_map, int nbSquare,  float aspectRatio, int pl, int sizeSquare, int x, int y);
