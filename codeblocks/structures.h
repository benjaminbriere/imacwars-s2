/////////////////////////////////////////////////
//               STRUCTURES.H                  //
//                                             //
//     by Gartner Esther & Briere Benjamin     //
//                                             //
/////////////////////////////////////////////////
#define MAXUNITE 10

typedef struct map_square{

    // PARTIE TERRAIN
    int cX;
    int cY;
    int ground;

    // PARTIE SOLDAT
    int player;
    int type_soldier;
    int life;
    int strength;
    int dexterity;
    int range;
    float fuel;
    int action;
    //
}map_square;

typedef struct player{
    int id;
    map_square liste[MAXUNITE];
    int nbUnite;
    int gold;
    int pouvoir;
    int ville;
}Player;


typedef struct map_range{
    int x;
    int y;
};

typedef struct node{
    int x;
    int y;
    float g;
    int h;
    float f;
    pair<int, int> neighbors[4];
    node *suivant;
    pair<int,int> parent;
}node;

typedef struct file{
    node *first;
    int taille;
}file;

typedef struct tree_node{
    int x;
    int y;
    float g;
    int h;
    float f;
    pair<int, int> neighbors[4];
    pair<int,int> parent;
    tree_node *left;
    tree_node *right;
}tree_node;

typedef struct node_parents{
    int x;
    int y;
    int px;
    int py;
}node_parents;

typedef struct villeIA{
    int x;
    int y;
    int etat;
    int cible;
}villeIA;
