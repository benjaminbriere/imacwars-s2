/////////////////////////////////////////////////
//                  GAME.CPP                   //
//                                             //
//     by Gartner Esther & Briere Benjamin     //
//                                             //
/////////////////////////////////////////////////
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

#include "structures.h"
#include "map.h"
#include "unit.h"
#include "menu.h"
#include "player.h"
#include "game.h"
#include "aStar.h"
#include "ia.h"

#define MAXUNITE 10
#define NBUNITE 5

static const Uint32 FRAMERATE_MILLISECONDS = 10000 / 60;

int game(SDL_Surface* surface,
          int nbSquare,
          map_square *game_map,
          player *game_player,
          float aspectRatio,
          GLuint texture_map[],
          GLuint texture_soldier[],
          GLuint texture_figure[],
          GLuint texture_button[],
          GLuint texture_stat[],
          GLuint texture_cursor[],
          GLuint texture_menu[]){


    Uint32 startTime = SDL_GetTicks();

    /* Placer ici le code de dessin */
    glClear(GL_COLOR_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    int loop = 1;
    int player = 0;
    int unitStat = 0;
    int sizeSquare;
    int spaceBetween = 0;
    int mode = 1;
    int modeAchat = 0;
    int curseurMvtX = -1;
    int curseurMvtY = -1;
    int curseurClicX = -1;
    int curseurClicY = -1;
    int capture = 0;
    int displayRange = 0;
    int mouvement = 0;
    int destX = -1;
    int destY = -1;

    int compteurpouvoir = 1;
    int menu = 1;
    int menulancement = 1;
    int menucarte = 0;
    int menureglejeu = 0;
    int menuchoixadversaire = 0;
    int ordi = 0;
    int menuchoixsuperpouvoir1 = 0;
    int menuchoixsuperpouvoir2 = 0;
    int menuabandon1 = 0;
    int menuabandon2 = 0;
    int menuabandon3 = 0;
    int menuchoixsuperpouvoirordi = 0;
    int pouvoir1 = 0;
    int pouvoir2 = 0;
    int pouvoir3 = 0;
    int pouvoir4 = 0;
    int choixPouvoirX = -1;
    int choixPouvoirY = -1;

    vector <pair<int,int> > tamp;
    pair<int,int> tamp2;
    tamp2.first = 1;
    tamp2.second = 1;
    tamp.push_back(tamp2);

    int tamptest = 0;
    int compteurtemptest = 0;
    int indexTemp = 0;
    int directionTemp;
    int stockageTypeSoldat;

    // J2 ORDINATEUR
    int numeroTour = 0;
    int tourOrdinateur = 0;
    int tourOrdinateurPurchase = 0;
    int tourOrdinateurNbFantassin = 0;
    int tourOrdinateurMouvementFantassin = 0;
    vector < pair<int,int> > listeFantassin;
    int tourOrdinateurNbUnits = 0;
    int tourOrdinateurMouvementUnits = 0;
    vector < pair<int,int> > listeUnits;
    int numFantassin;
    int indexTempUnit;
    int numUnits;
    int numeroUnits;
    int tamponXunit;
    int tamponYunit;
    int directionTempUnit;

// map pour la range
    map_range *game_range = NULL;
    game_range = (map_range *) malloc( nbSquare * nbSquare * sizeof(map_range));
    nullRange(nbSquare, &game_range[0]);

    map_range *game_mouvement = NULL;
    game_mouvement = (map_range *) malloc( nbSquare * nbSquare * sizeof(map_range));
    nullRange(nbSquare, &game_mouvement[0]);



    if(surface->w / nbSquare > (surface->h / nbSquare)){
        sizeSquare = (surface->h - (nbSquare-1)*spaceBetween) / nbSquare;
    }
    else{
        sizeSquare = (surface->w - (nbSquare-1)*spaceBetween) / nbSquare;
    }

    int tamponX =  tamp[0].first * sizeSquare + (sizeSquare/2);
    int tamponY =  tamp[0].second * sizeSquare + (sizeSquare/2);

    while(loop){

        if(menu == 1){

            if(menulancement == 1){

                glEnable(GL_TEXTURE_2D);
                glBindTexture(GL_TEXTURE_2D, texture_menu[0]);
                glEnable(GL_BLEND);
                glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
                glColor3ub(255,255,255);
                drawSquare(surface->w, surface->h, surface->w/2, surface->h/2, surface->w, surface->h, aspectRatio);
                glDisable(GL_BLEND);
                glBindTexture(GL_TEXTURE_2D, 0);
                glDisable(GL_TEXTURE_2D);

                glEnable(GL_TEXTURE_2D);
                glBindTexture(GL_TEXTURE_2D, texture_button[12]);
                glEnable(GL_BLEND);
                glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
                glColor3ub(255,255,255);
                drawSquare(surface->w, surface->h, surface->w/2, (surface->h/2)+170, 200, 100, aspectRatio);
                glDisable(GL_BLEND);
                glBindTexture(GL_TEXTURE_2D, 0);
                glDisable(GL_TEXTURE_2D);
            }
            if(menucarte == 1){
                glColor3ub(255,255,255);
                drawSquare(surface->w, surface->h, surface->w/2, surface->h/2, surface->w, surface->h, aspectRatio);

                glEnable(GL_TEXTURE_2D);
                glBindTexture(GL_TEXTURE_2D, texture_menu[12]);
                glEnable(GL_BLEND);
                glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
                glColor3ub(255,255,255);
                drawSquare(surface->w, surface->h, surface->w/2, surface->h/2, surface->w, surface->h, aspectRatio);
                glDisable(GL_BLEND);
                glBindTexture(GL_TEXTURE_2D, 0);
                glDisable(GL_TEXTURE_2D);

                glEnable(GL_TEXTURE_2D);
                glBindTexture(GL_TEXTURE_2D, texture_button[13]);
                glEnable(GL_BLEND);
                glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
                glColor3ub(255,255,255);
                drawSquare(surface->w, surface->h, (surface->w/2)-150, surface->h/2, 200, 100, aspectRatio);
                glDisable(GL_BLEND);
                glBindTexture(GL_TEXTURE_2D, 0);
                glDisable(GL_TEXTURE_2D);

                glEnable(GL_TEXTURE_2D);
                glBindTexture(GL_TEXTURE_2D, texture_button[14]);
                glEnable(GL_BLEND);
                glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
                glColor3ub(255,255,255);
                drawSquare(surface->w, surface->h, (surface->w/2)+150, surface->h/2, 200, 100, aspectRatio);
                glDisable(GL_BLEND);
                glBindTexture(GL_TEXTURE_2D, 0);
                glDisable(GL_TEXTURE_2D);

            }
            if(menureglejeu == 1){
                glEnable(GL_TEXTURE_2D);
                glBindTexture(GL_TEXTURE_2D, texture_menu[1]);
                glEnable(GL_BLEND);
                glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
                glColor3ub(255,255,255);
                drawSquare(surface->w, surface->h, surface->w/2, surface->h/2, surface->w, surface->h, aspectRatio);
                glDisable(GL_BLEND);
                glBindTexture(GL_TEXTURE_2D, 0);
                glDisable(GL_TEXTURE_2D);

                glEnable(GL_TEXTURE_2D);
                glBindTexture(GL_TEXTURE_2D, texture_button[15]);
                glEnable(GL_BLEND);
                glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
                glColor3ub(255,255,255);
                drawSquare(surface->w, surface->h, surface->w/2, 700, 200, 100, aspectRatio);
                glDisable(GL_BLEND);
                glBindTexture(GL_TEXTURE_2D, 0);
                glDisable(GL_TEXTURE_2D);
            }
            if(menuchoixadversaire == 1){
                glEnable(GL_TEXTURE_2D);
                glBindTexture(GL_TEXTURE_2D, texture_menu[2]);
                glEnable(GL_BLEND);
                glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
                glColor3ub(255,255,255);
                drawSquare(surface->w, surface->h, surface->w/2, surface->h/2, surface->w, surface->h, aspectRatio);
                glDisable(GL_BLEND);
                glBindTexture(GL_TEXTURE_2D, 0);
                glDisable(GL_TEXTURE_2D);

                glEnable(GL_TEXTURE_2D);
                glBindTexture(GL_TEXTURE_2D, texture_button[16]);
                glEnable(GL_BLEND);
                glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
                glColor3ub(255,255,255);
                drawSquare(surface->w, surface->h, (surface->w/2)-150, surface->h/2, 200, 100, aspectRatio);
                glDisable(GL_BLEND);
                glBindTexture(GL_TEXTURE_2D, 0);
                glDisable(GL_TEXTURE_2D);

                glEnable(GL_TEXTURE_2D);
                glBindTexture(GL_TEXTURE_2D, texture_button[17]);
                glEnable(GL_BLEND);
                glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
                glColor3ub(255,255,255);
                drawSquare(surface->w, surface->h, (surface->w/2)+150, surface->h/2, 200, 100, aspectRatio);
                glDisable(GL_BLEND);
                glBindTexture(GL_TEXTURE_2D, 0);
                glDisable(GL_TEXTURE_2D);
            }
            if(menuchoixsuperpouvoirordi == 1){
                glEnable(GL_TEXTURE_2D);
                glBindTexture(GL_TEXTURE_2D, texture_menu[3]);
                glEnable(GL_BLEND);
                glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
                glColor3ub(255,255,255);
                drawSquare(surface->w, surface->h, surface->w/2, surface->h/2, surface->w, surface->h, aspectRatio);
                glDisable(GL_BLEND);
                glBindTexture(GL_TEXTURE_2D, 0);
                glDisable(GL_TEXTURE_2D);

                glEnable(GL_TEXTURE_2D);
                glBindTexture(GL_TEXTURE_2D, texture_button[18]);
                glEnable(GL_BLEND);
                glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
                glColor3ub(255,255,255);
                drawSquare(surface->w, surface->h, (surface->w/2)-200, (surface->h/2)-150, 200, 100, aspectRatio);
                glDisable(GL_BLEND);
                glBindTexture(GL_TEXTURE_2D, 0);
                glDisable(GL_TEXTURE_2D);

                glEnable(GL_TEXTURE_2D);
                glBindTexture(GL_TEXTURE_2D, texture_button[19]);
                glEnable(GL_BLEND);
                glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
                glColor3ub(255,255,255);
                drawSquare(surface->w, surface->h, (surface->w/2)+200, (surface->h/2)-150, 200, 100, aspectRatio);
                glDisable(GL_BLEND);
                glBindTexture(GL_TEXTURE_2D, 0);
                glDisable(GL_TEXTURE_2D);

                glEnable(GL_TEXTURE_2D);
                glBindTexture(GL_TEXTURE_2D, texture_button[20]);
                glEnable(GL_BLEND);
                glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
                glColor3ub(255,255,255);
                drawSquare(surface->w, surface->h, (surface->w/2)-200, (surface->h/2)+150, 200, 100, aspectRatio);
                glDisable(GL_BLEND);
                glBindTexture(GL_TEXTURE_2D, 0);
                glDisable(GL_TEXTURE_2D);

                glEnable(GL_TEXTURE_2D);
                glBindTexture(GL_TEXTURE_2D, texture_button[21]);
                glEnable(GL_BLEND);
                glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
                glColor3ub(255,255,255);
                drawSquare(surface->w, surface->h, (surface->w/2)+200, (surface->h/2)+150, 200, 100, aspectRatio);
                glDisable(GL_BLEND);
                glBindTexture(GL_TEXTURE_2D, 0);
                glDisable(GL_TEXTURE_2D);

            }
            if(menuchoixsuperpouvoir1 == 1){
                glEnable(GL_TEXTURE_2D);
                glBindTexture(GL_TEXTURE_2D, texture_menu[3]);
                glEnable(GL_BLEND);
                glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
                glColor3ub(255,255,255);
                drawSquare(surface->w, surface->h, surface->w/2, surface->h/2, surface->w, surface->h, aspectRatio);
                glDisable(GL_BLEND);
                glBindTexture(GL_TEXTURE_2D, 0);
                glDisable(GL_TEXTURE_2D);

                glEnable(GL_TEXTURE_2D);
                glBindTexture(GL_TEXTURE_2D, texture_button[18]);
                glEnable(GL_BLEND);
                glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
                glColor3ub(255,255,255);
                drawSquare(surface->w, surface->h, (surface->w/2)-200, (surface->h/2)-150, 200, 100, aspectRatio);
                glDisable(GL_BLEND);
                glBindTexture(GL_TEXTURE_2D, 0);
                glDisable(GL_TEXTURE_2D);

                glEnable(GL_TEXTURE_2D);
                glBindTexture(GL_TEXTURE_2D, texture_button[19]);
                glEnable(GL_BLEND);
                glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
                glColor3ub(255,255,255);
                drawSquare(surface->w, surface->h, (surface->w/2)+200, (surface->h/2)-150, 200, 100, aspectRatio);
                glDisable(GL_BLEND);
                glBindTexture(GL_TEXTURE_2D, 0);
                glDisable(GL_TEXTURE_2D);

                glEnable(GL_TEXTURE_2D);
                glBindTexture(GL_TEXTURE_2D, texture_button[20]);
                glEnable(GL_BLEND);
                glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
                glColor3ub(255,255,255);
                drawSquare(surface->w, surface->h, (surface->w/2)-200, (surface->h/2)+150, 200, 100, aspectRatio);
                glDisable(GL_BLEND);
                glBindTexture(GL_TEXTURE_2D, 0);
                glDisable(GL_TEXTURE_2D);

                glEnable(GL_TEXTURE_2D);
                glBindTexture(GL_TEXTURE_2D, texture_button[21]);
                glEnable(GL_BLEND);
                glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
                glColor3ub(255,255,255);
                drawSquare(surface->w, surface->h, (surface->w/2)+200, (surface->h/2)+150, 200, 100, aspectRatio);
                glDisable(GL_BLEND);
                glBindTexture(GL_TEXTURE_2D, 0);
                glDisable(GL_TEXTURE_2D);

            }
            if(menuchoixsuperpouvoir2 == 1){
                glEnable(GL_TEXTURE_2D);
                glBindTexture(GL_TEXTURE_2D, texture_menu[4]);
                glEnable(GL_BLEND);
                glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
                glColor3ub(255,255,255);
                drawSquare(surface->w, surface->h, surface->w/2, surface->h/2, surface->w, surface->h, aspectRatio);
                glDisable(GL_BLEND);
                glBindTexture(GL_TEXTURE_2D, 0);
                glDisable(GL_TEXTURE_2D);

                glEnable(GL_TEXTURE_2D);
                glBindTexture(GL_TEXTURE_2D, texture_button[18]);
                glEnable(GL_BLEND);
                glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
                glColor3ub(255,255,255);
                drawSquare(surface->w, surface->h, (surface->w/2)-200, (surface->h/2)-150, 200, 100, aspectRatio);
                glDisable(GL_BLEND);
                glBindTexture(GL_TEXTURE_2D, 0);
                glDisable(GL_TEXTURE_2D);

                glEnable(GL_TEXTURE_2D);
                glBindTexture(GL_TEXTURE_2D, texture_button[19]);
                glEnable(GL_BLEND);
                glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
                glColor3ub(255,255,255);
                drawSquare(surface->w, surface->h, (surface->w/2)+200, (surface->h/2)-150, 200, 100, aspectRatio);
                glDisable(GL_BLEND);
                glBindTexture(GL_TEXTURE_2D, 0);
                glDisable(GL_TEXTURE_2D);

                glEnable(GL_TEXTURE_2D);
                glBindTexture(GL_TEXTURE_2D, texture_button[20]);
                glEnable(GL_BLEND);
                glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
                glColor3ub(255,255,255);
                drawSquare(surface->w, surface->h, (surface->w/2)-200, (surface->h/2)+150, 200, 100, aspectRatio);
                glDisable(GL_BLEND);
                glBindTexture(GL_TEXTURE_2D, 0);
                glDisable(GL_TEXTURE_2D);

                glEnable(GL_TEXTURE_2D);
                glBindTexture(GL_TEXTURE_2D, texture_button[21]);
                glEnable(GL_BLEND);
                glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
                glColor3ub(255,255,255);
                drawSquare(surface->w, surface->h, (surface->w/2)+200, (surface->h/2)+150, 200, 100, aspectRatio);
                glDisable(GL_BLEND);
                glBindTexture(GL_TEXTURE_2D, 0);
                glDisable(GL_TEXTURE_2D);

            }
            if(menuabandon1 == 1){
                glEnable(GL_TEXTURE_2D);
                glBindTexture(GL_TEXTURE_2D, texture_menu[5]);
                glEnable(GL_BLEND);
                glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
                glColor3ub(255,255,255);
                drawSquare(surface->w, surface->h, surface->w/2, surface->h/2, surface->w, surface->h, aspectRatio);
                glDisable(GL_BLEND);
                glBindTexture(GL_TEXTURE_2D, 0);
                glDisable(GL_TEXTURE_2D);

                glEnable(GL_TEXTURE_2D);
                glBindTexture(GL_TEXTURE_2D, texture_button[22]);
                glEnable(GL_BLEND);
                glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
                glColor3ub(255,255,255);
                drawSquare(surface->w, surface->h, surface->w/2, surface->h/2, 200, 100, aspectRatio);
                glDisable(GL_BLEND);
                glBindTexture(GL_TEXTURE_2D, 0);
                glDisable(GL_TEXTURE_2D);
            }
            if(menuabandon2 == 1){
                glEnable(GL_TEXTURE_2D);
                glBindTexture(GL_TEXTURE_2D, texture_menu[6]);
                glEnable(GL_BLEND);
                glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
                glColor3ub(255,255,255);
                drawSquare(surface->w, surface->h, surface->w/2, surface->h/2, surface->w, surface->h, aspectRatio);
                glDisable(GL_BLEND);
                glBindTexture(GL_TEXTURE_2D, 0);
                glDisable(GL_TEXTURE_2D);

                glEnable(GL_TEXTURE_2D);
                glBindTexture(GL_TEXTURE_2D, texture_button[22]);
                glEnable(GL_BLEND);
                glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
                glColor3ub(255,255,255);
                drawSquare(surface->w, surface->h, surface->w/2, surface->h/2, 200, 100, aspectRatio);
                glDisable(GL_BLEND);
                glBindTexture(GL_TEXTURE_2D, 0);
                glDisable(GL_TEXTURE_2D);
            }

            if(pouvoir1 == 1){
                glEnable(GL_TEXTURE_2D);
                glBindTexture(GL_TEXTURE_2D, texture_menu[8]);
                glEnable(GL_BLEND);
                glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
                glColor3ub(255,255,255);
                drawSquare(surface->w, surface->h, surface->w/2, surface->h/2, 1000, 300, aspectRatio);
                glDisable(GL_BLEND);
                glBindTexture(GL_TEXTURE_2D, 0);
                glDisable(GL_TEXTURE_2D);

                // COMPTEUR POUVOIR
                if(compteurpouvoir < 100 && compteurpouvoir != 0){
                    compteurpouvoir++;
                    printf("COMPTEUR !! \n");
                    if(compteurpouvoir == 100){
                        menu = 0;
                        pouvoir1 = 0;
                        compteurpouvoir = 0;
                        printf("Menu pouvoir à 0 \n");
                    }
                }
            }
            if(pouvoir2 == 1){
                glEnable(GL_TEXTURE_2D);
                glBindTexture(GL_TEXTURE_2D, texture_menu[9]);
                glEnable(GL_BLEND);
                glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
                glColor3ub(255,255,255);
                drawSquare(surface->w, surface->h, surface->w/2, surface->h/2, 1000, 300, aspectRatio);
                glDisable(GL_BLEND);
                glBindTexture(GL_TEXTURE_2D, 0);
                glDisable(GL_TEXTURE_2D);

                // COMPTEUR POUVOIR
                if(compteurpouvoir < 100 && compteurpouvoir != 0){
                    compteurpouvoir++;
                    printf("COMPTEUR !! \n");
                    if(compteurpouvoir == 100){
                        menu = 0;
                        pouvoir2 = 0;
                        compteurpouvoir = 0;
                        printf("Menu pouvoir à 0 \n");
                    }
                }
            }
            if(pouvoir3 == 1){
                glEnable(GL_TEXTURE_2D);
                glBindTexture(GL_TEXTURE_2D, texture_menu[10]);
                glEnable(GL_BLEND);
                glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
                glColor3ub(255,255,255);
                drawSquare(surface->w, surface->h, surface->w/2, surface->h/2, 1000, 300, aspectRatio);
                glDisable(GL_BLEND);
                glBindTexture(GL_TEXTURE_2D, 0);
                glDisable(GL_TEXTURE_2D);

                // COMPTEUR POUVOIR
                if(compteurpouvoir < 100 && compteurpouvoir != 0){
                    compteurpouvoir++;
                    //printf("COMPTEUR !! \n");
                    if(compteurpouvoir == 100){
                        menu = 0;
                        compteurpouvoir = 0;
                        printf("Menu pouvoir à 0 :) \n");
                        choixPouvoirX = 0;
                        choixPouvoirY = 0;
                    }
                }
            }
            if(pouvoir4 == 1){
                glEnable(GL_TEXTURE_2D);
                glBindTexture(GL_TEXTURE_2D, texture_menu[11]);
                glEnable(GL_BLEND);
                glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
                glColor3ub(255,255,255);
                drawSquare(surface->w, surface->h, surface->w/2, surface->h/2, 1000, 300, aspectRatio);
                glDisable(GL_BLEND);
                glBindTexture(GL_TEXTURE_2D, 0);
                glDisable(GL_TEXTURE_2D);

                // COMPTEUR POUVOIR
                if(compteurpouvoir < 100 && compteurpouvoir != 0){
                    compteurpouvoir++;
                    printf("COMPTEUR %d \n", compteurpouvoir);
                    if(compteurpouvoir == 100){
                        menu = 0;
                        //pouvoir4 = 0;
                        compteurpouvoir = 0;
                        printf("Menu pouvoir à 0 :( \n");
                        choixPouvoirX = 0;
                        choixPouvoirY = 0;
                    }
                }
            }

            /* Echange du front et du back buffer : mise a jour de la fenetre */
            SDL_GL_SwapBuffers();

            /* Boucle traitant les evenements */
            SDL_Event e;

            while(SDL_PollEvent(&e))
            {
                /* L'utilisateur ferme la fenetre : */
                if(e.type == SDL_QUIT)
                {
                    loop = 0;
                    break;
                }

                if(	e.type == SDL_KEYDOWN
                    && (e.key.keysym.sym == SDLK_q || e.key.keysym.sym == SDLK_ESCAPE))
                {
                    loop = 0;
                    break;
                }

                /* Quelques exemples de traitement d'evenements : */
                switch(e.type)
                {

                    case SDL_MOUSEBUTTONUP:
                        printf("clic en (%d, %d)\n", e.button.x, e.button.y);


                        if(menulancement == 1){
                            if(e.button.x >= 400 && e.button.x <= 600){
                                if(e.button.y >= 520 && e.button.y <= 620){
                                    menulancement = 0;
                                    menucarte = 1;
                                }
                            }
                        }
                        else if(menucarte == 1){

                            if(e.button.x >= 550 && e.button.x <= 750){
                                if(e.button.y >= 350 && e.button.y <= 450){
                                    menureglejeu = 1;
                                    menucarte = 0;

                                    }
                            }
                            if(e.button.x >= 250 && e.button.x <= 450){
                                            if(e.button.y >= 350 && e.button.y <= 450){
                                            menuchoixadversaire = 1;
                                            menucarte = 0;
                                            }

                                    }
                        }
                        else if(menureglejeu == 1){
                            if(e.button.x >= 400 && e.button.x <= 600){
                                if(e.button.y >= 650 && e.button.y <= 750){
                                    menucarte = 1;
                                    menureglejeu = 0;

                                }

                            }
                        }

                        else if(menuchoixadversaire == 1){
                            //Joueurs
                            if(e.button.x >= 550 && e.button.x <= 750){
                                if(e.button.y >= 350 && e.button.y <= 450){
                                    menuchoixadversaire = 0;
                                    menuchoixsuperpouvoir1 = 1;

                                    }
                            }
                            //Ordinateur
                            if(e.button.x >= 250 && e.button.x <= 450){
                                if(e.button.y >= 350 && e.button.y <= 450){
                                    menuchoixadversaire = 0;
                                    menuchoixsuperpouvoirordi = 1;
                                    ordi = 1;
                                }

                            }
                        }
                        //Choix du pouvoir pour le joueur jouant contre l'ordinateur
                        else if(menuchoixsuperpouvoirordi == 1){
                             //Arme chimique 1
                            if(e.button.x >= 200 && e.button.x <= 400){
                                if(e.button.y >= 200 && e.button.y <= 300){
                                    menuchoixsuperpouvoirordi = 0;
                                    menu = 0;
                                    game_player[0].pouvoir = 1;
                                    }
                            }
                            //Medecin 2
                            if(e.button.x >= 600 && e.button.x <= 800){
                                if(e.button.y >= 200 && e.button.y <= 300){
                                    menuchoixsuperpouvoirordi = 0;
                                    menu = 0;
                                    game_player[0].pouvoir = 2;
                                    }
                            }
                            //Tir satellite 3
                            if(e.button.x >= 600 && e.button.x <= 800){
                                if(e.button.y >= 500 && e.button.y <= 600){
                                    menuchoixsuperpouvoirordi = 0;
                                    menu = 0;
                                    game_player[0].pouvoir = 3;
                                    }
                            }
                            //Collabo 4
                            if(e.button.x >= 200 && e.button.x <= 400){
                                if(e.button.y >= 500 && e.button.y <= 600){
                                    menuchoixsuperpouvoirordi = 0;
                                    menu = 0;
                                    game_player[0].pouvoir = 4;
                                    }
                            }
                        }
                        //Choix du pouvoir pour le joueur 1
                        else if(menuchoixsuperpouvoir1 == 1){
                            //Arme chimique 1
                            if(e.button.x >= 200 && e.button.x <= 400){
                                if(e.button.y >= 200 && e.button.y <= 300){
                                    menuchoixsuperpouvoir2 = 1;
                                    menuchoixsuperpouvoir1 = 0;
                                    game_player[0].pouvoir = 1;
                                    }
                            }
                            //Medecin 2
                            if(e.button.x >= 600 && e.button.x <= 800){
                                if(e.button.y >= 200 && e.button.y <= 300){
                                    menuchoixsuperpouvoir2 = 1;
                                    menuchoixsuperpouvoir1 = 0;
                                    game_player[0].pouvoir = 2;
                                    }
                            }
                            //Tir satellite 3
                            if(e.button.x >= 600 && e.button.x <= 800){
                                if(e.button.y >= 500 && e.button.y <= 600){
                                    menuchoixsuperpouvoir2 = 1;
                                    menuchoixsuperpouvoir1 = 0;
                                    game_player[0].pouvoir = 3;
                                    }
                            }
                            //Collabo 4
                            if(e.button.x >= 200 && e.button.x <= 400){
                                if(e.button.y >= 500 && e.button.y <= 600){
                                    menuchoixsuperpouvoir2 = 1;
                                    menuchoixsuperpouvoir1 = 0;
                                    game_player[0].pouvoir = 4;
                                    }
                            }
                        }
                        //Choix du pouvoir pour le joueur 2
                        else if(menuchoixsuperpouvoir2 == 1){
                            //Arme chimique
                            if(e.button.x >= 200 && e.button.x <= 400){
                                if(e.button.y >= 200 && e.button.y <= 300){
                                    menuchoixsuperpouvoir2 = 0;
                                    menu = 0;
                                    game_player[1].pouvoir = 1;
                                    }
                            }
                            //Medecin
                            if(e.button.x >= 600 && e.button.x <= 800){
                                if(e.button.y >= 200 && e.button.y <= 300){
                                    menuchoixsuperpouvoir2 = 0;
                                    menu = 0;
                                    game_player[1].pouvoir = 2;
                                    }
                            }
                            //Tir satellite
                            if(e.button.x >= 600 && e.button.x <= 800){
                                if(e.button.y >= 500 && e.button.y <= 600){
                                    menuchoixsuperpouvoir2 = 0;
                                    menu = 0;
                                    game_player[1].pouvoir = 3;
                                    }
                            }
                            //Collabo
                            if(e.button.x >= 200 && e.button.x <= 400){
                                if(e.button.y >= 500 && e.button.y <= 600){
                                    menuchoixsuperpouvoir2 = 0;
                                    menu = 0;
                                    game_player[1].pouvoir = 4;
                                    }
                            }
                        }
                        else if(menuabandon1 == 1){
                                if(e.button.x >= 400 && e.button.x <= 600){
                                    if(e.button.y >= 350 && e.button.y <= 450){
                                        loop = 0;
                                    }
                                }
                            }
                        else if(menuabandon2 == 1){
                                if(e.button.x >= 400 && e.button.x <= 600){
                                    if(e.button.y >= 350 && e.button.y <= 450){
                                        loop = 0;
                                    }
                                }
                            }


                        break;

                    /* Touche clavier */
                    case SDL_KEYDOWN:
                        printf("echo 13");
                        printf("touche pressee (code = %d)\n", e.key.keysym.sym);
                        break;
                    default:
                        break;
                }
            }


            Uint32 elapsedTime = SDL_GetTicks() - startTime;
            /* Si trop peu de temps s'est ecoule, on met en pause le programme */
            if(elapsedTime < FRAMERATE_MILLISECONDS)
            {
                SDL_Delay(FRAMERATE_MILLISECONDS - elapsedTime);
            }


        }
        else{
            if(player == 0){

                glClear(GL_COLOR_BUFFER_BIT);
                mapDisplay(surface, texture_map, nbSquare, &game_map[0], aspectRatio);
                menuDisplay(surface, texture_soldier, nbSquare, &game_map[0], &game_player[0],
                            aspectRatio, player, texture_figure, texture_button, texture_stat);

                displayStat(surface, texture_soldier, nbSquare,aspectRatio, player, texture_stat, sizeSquare, unitStat );

                displayAttackOrPurchase(surface, texture_button, nbSquare,aspectRatio, player, texture_stat, sizeSquare, unitStat, mode, &game_player[0], texture_button );

                displayCapture(surface, texture_soldier, texture_button, &game_map[0], nbSquare, aspectRatio, player, sizeSquare,curseurClicX, curseurClicY);

                soldierDisplay(surface, texture_soldier, nbSquare, &game_map[0], aspectRatio);

                if( game_player[0].nbUnite == 0 ){
                    menu=1;
                    menuabandon1 = 1;
                }
                else if(game_player[1].nbUnite == 0){
                                menu=1;
                                menuabandon2 = 1;
                }
                else{
                    if(tamptest == 1){
                        animationMouvement(&game_map[0],surface,
                                            texture_soldier,
                                            tamp,
                                            &indexTemp,
                                            &tamponX, &tamponY,
                                            &directionTemp,
                                            sizeSquare,
                                            aspectRatio,
                                            nbSquare,
                                            stockageTypeSoldat,
                                            &tamptest,
                                            &game_player[0],
                                            player);
                        printf("\n");
                        if(tamptest == 0){
                            game_map[curseurClicY/sizeSquare * nbSquare + curseurClicX/sizeSquare].type_soldier = stockageTypeSoldat;
                            unitMouvement(surface, &game_map[0], &game_player[0], player, curseurClicX/sizeSquare, curseurClicY/sizeSquare, destX, destY, nbSquare, aspectRatio);
                        }

                    }
                    // TEST A*

                    // FIN TEST A*

                    if(displayRange == 1){
                        displayRangeMap(surface, nbSquare, &game_range[0], aspectRatio);
                    }

                    if(mouvement == 1) {
                        displayDeplacementMap(surface, nbSquare, &game_mouvement[0], aspectRatio);
                    }

                    //playerUnit( &game_player[0], player);

                    if(curseurClicX != -1 && curseurClicY != -1){

                        lifeBar(surface->w,
                            surface->h,
                            nbSquare*sizeSquare + (surface->w-(nbSquare*sizeSquare))/2,
                            surface->h/2 + (7* (surface->h/2) /100),
                            80*(surface->w-(nbSquare*sizeSquare))/100,
                            surface->h/35,
                            aspectRatio,
                            game_map[curseurClicY/sizeSquare * nbSquare + curseurClicX/sizeSquare].life);
                            displayStat(surface, texture_soldier, nbSquare,aspectRatio, player, texture_stat, sizeSquare, unitStat );
                    }

                    if(curseurMvtX != -1 && curseurMvtY != -1){
                        glEnable(GL_TEXTURE_2D);
                        glBindTexture(GL_TEXTURE_2D, texture_cursor[0]);
                        glEnable(GL_BLEND);
                        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
                        glColor3ub(255, 255,255);
                        curseurMvtX = (curseurMvtX / sizeSquare) * sizeSquare + sizeSquare/2;
                        curseurMvtY = (curseurMvtY / sizeSquare) * sizeSquare + sizeSquare/2;
                        drawSquare(surface->w, surface->h, curseurMvtX, curseurMvtY, sizeSquare, sizeSquare, aspectRatio);
                        glDisable(GL_BLEND);
                        glBindTexture(GL_TEXTURE_2D, 0);
                        glDisable(GL_TEXTURE_2D);
                    }
                }

            }
            else if (player == 1){
                glClear(GL_COLOR_BUFFER_BIT);
                mapDisplay(surface, texture_map, nbSquare, &game_map[0], aspectRatio);
                menuDisplay(surface, texture_soldier, nbSquare, &game_map[0], &game_player[0],
                            aspectRatio, player, texture_figure, texture_button, texture_stat);

                displayStat(surface, texture_soldier, nbSquare,aspectRatio, player, texture_stat, sizeSquare, unitStat );

                displayAttackOrPurchase(surface, texture_button, nbSquare,aspectRatio, player, texture_stat, sizeSquare, unitStat, mode, &game_player[0], texture_button );
                displayCapture(surface, texture_soldier, texture_button, &game_map[0], nbSquare, aspectRatio, player, sizeSquare, curseurClicX, curseurClicY);

                soldierDisplay(surface, texture_soldier, nbSquare, &game_map[0], aspectRatio);

                if (tourOrdinateur == 1){
                    int numero = 0;
                    int numeroUnits = 0;
                    printf("=================== TOUR %d ============================\n", numeroTour);


                    if(tourOrdinateurPurchase == 1){
                        listeFantassin.clear();
                        listeUnits.clear();
                        printf("=================== MODE ACHAT ============================\n");
                        computerTurnPurchase(surface, &game_map[0],&game_player[0],player, nbSquare, aspectRatio, &tourOrdinateur, sizeSquare);
                        tourOrdinateurPurchase = 0;
                        for (int i=0; i<MAXUNITE; i++){
                            if(game_player[player].liste[i].type_soldier == 1+NBUNITE*player){
                                pair<int,int> temp;
                                temp.first = game_player[player].liste[i].cX;
                                temp.second = game_player[player].liste[i].cY;
                                listeFantassin.push_back(temp);
                            }
                            else if(game_player[player].liste[i].type_soldier > 1+NBUNITE*player && game_player[player].liste[i].type_soldier <= 5+NBUNITE*player){
                                pair<int,int> temp;
                                temp.first = game_player[player].liste[i].cX;
                                temp.second = game_player[player].liste[i].cY;
                                listeUnits.push_back(temp);
                            }
                        }

                        tamponX = listeFantassin[numero].first * sizeSquare + (sizeSquare/2);
                        tamponY = listeFantassin[numero].second * sizeSquare + (sizeSquare/2);

                        tamponXunit = listeUnits[numero].first * sizeSquare + (sizeSquare/2);
                        tamponYunit = listeUnits[numero].second * sizeSquare + (sizeSquare/2);
                        //std::reverse(listeFantassin.begin(), listeFantassin.end());
                        indexTemp = 0;
                        numFantassin = 0;
                        printf("tamponX = %d \n", tamponX);
                        printf("tamponY = %d \n", tamponY);
                        printf("taille listeFantassin %d \n",listeFantassin.size());
;                       for(int j = 0; j<listeFantassin.size(); j++){
                            printf("fantassin %d : %d %d \n", j, listeFantassin[j].first, listeFantassin[j].second);
                        }

                        indexTempUnit = 0;
                        numUnits = 0;
                        printf("tamponX Unit = %d \n", tamponXunit);
                        printf("tamponY Unit = %d \n", tamponYunit);
                        printf("taille Units %d \n",listeUnits.size());
;                       for(int j = 0; j<listeUnits.size(); j++){
                            printf("Unité %d : %d %d \n", j, listeUnits[j].first, listeUnits[j].second);
                        }
/*
                        printf("affichage des unites du joueur 2 \n");
                        for(int j = 0; j<MAXUNITE; j++){
                            printf("Unité %d : %d %d | action %d et fuel %d \n", j, game_player[1].liste[j].cX, game_player[1].liste[j].cY, game_player[1].liste[j].action , game_player[1].liste[j].fuel);
                        }
*/
                    }
                    if(tourOrdinateurNbFantassin < listeFantassin.size() ){
                        printf("================= MODE DEPLACEMENT SOLDAT %d ====================\n", numFantassin);
                        //printf("numero %d \n", numero);
                        if(tourOrdinateurMouvementFantassin == 1){
                            //computerTurn(surface, &game_map[0],&game_player[0],player, nbSquare, aspectRatio, &tourOrdinateurMouvementFantassin);
                            computerTurnMouvementFantassin(surface,
                                                           &game_map[0],
                                                           texture_soldier,
                                                           &game_player[0],
                                                           player,
                                                           nbSquare,
                                                           aspectRatio,
                                                           &tourOrdinateurMouvementFantassin,
                                                           &numFantassin,
                                                           listeFantassin,
                                                           &tamponX,
                                                           &tamponY,
                                                           &indexTemp,
                                                           &directionTemp,
                                                           sizeSquare);

                            printf("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^valeur de tamponX et tamponY = %d %d \n", tamponX, tamponY);
                            if(tourOrdinateurMouvementFantassin == 0){
                                if(numFantassin < listeFantassin.size()){
                                    numero++;
                                    indexTemp = 0;
                                    tamponX = listeFantassin[numFantassin].first * sizeSquare + (sizeSquare/2);
                                    tamponY = listeFantassin[numFantassin].second * sizeSquare + (sizeSquare/2);
                                    directionTemp = 0;
                                    //printf("=================================================================================VALEUR de tamponX et tamponY = %d %d \n", tamponX, tamponY);

                                    tourOrdinateurMouvementFantassin = 1;
                                    tourOrdinateurNbFantassin++;
                                    printf("============================================================================ Tour Ordinateur NB Fantassin : %d \n", tourOrdinateurNbFantassin );
                                }
                                else{
                                    tourOrdinateurMouvementFantassin = 0;
                                    numeroTour++;
                                    tourOrdinateurNbFantassin = listeFantassin.size() ;
                                    printf("============================================================================ Tour Ordinateur NB Fantassin : %d \n", tourOrdinateurNbFantassin );
                                    for(int j = 0; j<MAXUNITE; j++){
                                        printf("Unité %d : %d %d | type %d | action %d et fuel %d \n", j, game_player[1].liste[j].cX, game_player[1].liste[j].cY, game_player[1].liste[j].type_soldier,  game_player[1].liste[j].action , game_player[1].liste[j].fuel);
                                    }
                                    printf("tourOrdinateurNbFantassin %d \n", tourOrdinateurNbFantassin);
                                    printf("================= FIN DEPLACEMENT  ====================\n\n");
                                }
                            }
                        }
                        else{
                            if( game_player[0].nbUnite == 0 ){
                                menu=1;
                                menuabandon1 = 1;
                            }
                            else if(game_player[1].nbUnite == 0){
                                menu=1;
                                menuabandon2 = 1;
                            }

                        }
                    }
                    else if(tourOrdinateurNbUnits < listeUnits.size() ){
                        printf("================= MODE DEPLACEMENT UNITS %d ====================\n", numUnits);
/*
                        printf("\naffichage des unites du joueur 2 \n");
                        for(int j = 0; j<MAXUNITE; j++){
                            printf("Unité %d : %d %d | type %d | action %d et fuel %d \n", j, game_player[1].liste[j].cX, game_player[1].liste[j].cY, game_player[1].liste[j].type_soldier,  game_player[1].liste[j].action , game_player[1].liste[j].fuel);
                        }
                        printf("\n");
*/
                        //printf("numero %d \n", numero);
                        if(tourOrdinateurMouvementUnits == 1){
                            //computerTurn(surface, &game_map[0],&game_player[0],player, nbSquare, aspectRatio, &tourOrdinateurMouvementFantassin);
                            computerTurnMouvementOthers(surface,
                                                           &game_map[0],
                                                           texture_soldier,
                                                           &game_player[0],
                                                           player,
                                                           nbSquare,
                                                           aspectRatio,
                                                           &tourOrdinateurMouvementUnits,
                                                           &numUnits,
                                                           listeUnits,
                                                           &tamponXunit,
                                                           &tamponYunit,
                                                           &indexTempUnit,
                                                           &directionTempUnit,
                                                           sizeSquare);

                            printf("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^valeur de tamponXunit et tamponY = %d %d \n", tamponXunit, tamponYunit);
                            if(tourOrdinateurMouvementUnits == 0){
                                if(numUnits < listeUnits.size()){
                                    numeroUnits++;
                                    indexTempUnit = 0;
                                    tamponXunit = listeUnits[numUnits].first * sizeSquare + (sizeSquare/2);
                                    tamponYunit = listeUnits[numUnits].second * sizeSquare + (sizeSquare/2);
                                    directionTempUnit = 0;
                                    printf("=================================================================================VALEUR de tamponXunit et tamponYunit = %d %d \n", tamponXunit, tamponYunit);
                                    tourOrdinateurNbUnits++;
                                    if (game_player[0].nbUnite == 0){
                                        tourOrdinateurMouvementUnits = 0;
                                    }else{
                                        tourOrdinateurMouvementUnits = 1;
                                    }

                                }
                                else{
                                    tourOrdinateurMouvementUnits = 0;
                                    //numeroTour++;
                                    tourOrdinateurNbUnits++;
                                    printf("================= FIN DEPLACEMENT  ====================\n\n");
                                }
                            }
                        }
                        else{
                            tourOrdinateur = 0;
                            printf("NOMBRE DE VILLE DU JOUEUR BLEU %d \n", game_player[0].ville );
                            game_player[1].gold = game_player[1].gold + 500 + (game_player[1].ville * 500);
                            printf("GOLD UPDATE\n");
                            if (game_player[1].gold>9999)
                            {
                                game_player[1].gold = 9999;
                            }

                            player = 0;
                            printf("PLAYER UPDATE \n");
                            reapprovisionnement(nbSquare, &game_map[0], &game_player[0], player);
                            printf("REAPPROVISIONNEMENT UPDATE \n");
                            nullRange(nbSquare, &game_range[0]);
                            printf("RANGE UPDATE \n");
                            displayRange = 0;
                            printf("DISPLAY RANGE UPDATE \n");

                            mode = 1;
                            unitStat = 0;
                            curseurClicX = -1;
                            curseurClicY = -1;
                            displayRange = 0;
                            mouvement = 0;
                            destX = -1;
                            destY = -1;

                            if( game_player[0].nbUnite == 0 ){
                                menu=1;
                                menuabandon1 = 1;
                            }
                            else if(game_player[1].nbUnite == 0){
                                menu=1;
                                menuabandon2 = 1;
                            }

                        }
                    }

                    else{
                        tourOrdinateur = 0;
                        printf("NOMBRE DE VILLE DU JOUEUR BLEU %d \n", game_player[0].ville );
                        game_player[1].gold = game_player[1].gold + 500 + (game_player[1].ville * 500);
                        printf("GOLD UPDATE\n");
                        if (game_player[1].gold>9999)
                        {
                            game_player[1].gold = 9999;
                        }

                        player = 0;
                        printf("PLAYER UPDATE \n");
                        reapprovisionnement(nbSquare, &game_map[0], &game_player[0], player);
                        printf("REAPPROVISIONNEMENT UPDATE \n");
                        nullRange(nbSquare, &game_range[0]);
                        printf("RANGE UPDATE \n");
                        displayRange = 0;
                        printf("DISPLAY RANGE UPDATE \n");

                        mode = 1;
                        unitStat = 0;
                        curseurClicX = -1;
                        curseurClicY = -1;
                        displayRange = 0;
                        mouvement = 0;
                        destX = -1;
                        destY = -1;

                        if( game_player[0].nbUnite == 0 ){
                                menu=1;
                                menuabandon1 = 1;
                        }
                        else if(game_player[1].nbUnite == 0){
                                menu=1;
                                menuabandon2 = 1;
                        }

                    }

                }
                else{

                    if(tamptest == 1){
                        animationMouvement(&game_map[0],surface,
                                            texture_soldier,
                                            tamp,
                                            &indexTemp,
                                            &tamponX, &tamponY,
                                            &directionTemp,
                                            sizeSquare,
                                            aspectRatio,
                                            nbSquare,
                                            stockageTypeSoldat,
                                            &tamptest,
                                            &game_player[0],
                                            player);
                        printf("\n");
                        if(tamptest == 0){
                            game_map[curseurClicY/sizeSquare * nbSquare + curseurClicX/sizeSquare].type_soldier = stockageTypeSoldat;
                            unitMouvement(surface, &game_map[0], &game_player[0], player, curseurClicX/sizeSquare, curseurClicY/sizeSquare, destX, destY, nbSquare, aspectRatio);
                        }

                    }

                    if(displayRange == 1){
                        displayRangeMap(surface, nbSquare, &game_range[0], aspectRatio);
                    }

                    if(mouvement == 1) {
                        displayDeplacementMap(surface, nbSquare, &game_mouvement[0], aspectRatio);
                    }

                    if(curseurClicX != -1 && curseurClicY != -1){
                        lifeBar(surface->w,
                            surface->h,
                            nbSquare*sizeSquare + (surface->w-(nbSquare*sizeSquare))/2,
                            surface->h/2 + (7* (surface->h/2) /100),
                            80*(surface->w-(nbSquare*sizeSquare))/100,
                            surface->h/35,
                            aspectRatio,
                            game_map[curseurClicY/sizeSquare * nbSquare + curseurClicX/sizeSquare].life);
                    }

                    if(curseurMvtX != -1 && curseurMvtY != -1){
                        glEnable(GL_TEXTURE_2D);
                        glBindTexture(GL_TEXTURE_2D, texture_cursor[1]);
                        glEnable(GL_BLEND);
                        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
                        glColor3ub(255, 255,255);
                        curseurMvtX = (curseurMvtX / sizeSquare) * sizeSquare + sizeSquare/2;
                        curseurMvtY = (curseurMvtY / sizeSquare) * sizeSquare + sizeSquare/2;
                        drawSquare(surface->w, surface->h, curseurMvtX, curseurMvtY, sizeSquare, sizeSquare, aspectRatio);
                        glDisable(GL_BLEND);
                        glBindTexture(GL_TEXTURE_2D, 0);
                        glDisable(GL_TEXTURE_2D);
                    }
                }

            }

            /* Echange du front et du back buffer : mise a jour de la fenetre */
            SDL_GL_SwapBuffers();

            /* Boucle traitant les evenements */
            SDL_Event e;

            while(SDL_PollEvent(&e))
            {
                /* L'utilisateur ferme la fenetre : */
                if(e.type == SDL_QUIT)
                {
                    loop = 0;
                    break;
                }

                if(	e.type == SDL_KEYDOWN
                    && (e.key.keysym.sym == SDLK_q || e.key.keysym.sym == SDLK_ESCAPE))
                {
                    loop = 0;
                    break;
                }

                /* Quelques exemples de traitement d'evenements : */
                if(tamptest == 0){
                    switch(e.type)
                    {

                        //*/
                        case SDL_MOUSEMOTION:
                            //drawSquare(surface->w, surface->h, e.button.x, e.button.y, sizeSquare, sizeSquare, aspectRatio);
                            if(e.button.x <= nbSquare * (surface->h / nbSquare)){ // CLIC DANS LA MAP
                                    curseurMvtX = e.button.x;
                                    curseurMvtY = e.button.y;
                            }
                            else{
                                curseurMvtX = -1;
                                curseurMvtY = -1;
                            }

                            break;
                        /* Clic souris */
                        case SDL_MOUSEBUTTONUP:
                            printf("clic en (%d, %d)\n", e.button.x, e.button.y);

                            if(e.button.x <= nbSquare * (surface->h / nbSquare)){ // CLIC DANS LA MAP
                                squareInfo(&game_map[0],e.button.x,e.button.y, nbSquare, surface);
                                int finDeRange = 0; // condition d'arret de l'affichage de la port�e de tire

                                if(choixPouvoirX == 0 && choixPouvoirY == 0){
                                    printf("choix de la cible a colabo \n");
                                    if(pouvoir4 == 1){
                                        if( !(game_map[e.button.y / sizeSquare * nbSquare + e.button.x /sizeSquare].player == player+1) ){
                                            if( !(game_map[e.button.y / sizeSquare * nbSquare + e.button.x /sizeSquare].player == 0)){
                                                 playerPower(surface, &game_map[0], &game_player[0], player, nbSquare, aspectRatio, e.button.x / sizeSquare, e.button.y / sizeSquare);
                                            }
                                            else{
                                                compteurpouvoir = 1;
                                            }
                                        }
                                        pouvoir4 = 0;
                                    }

                                    if(pouvoir3 == 1){
                                        if( !(game_map[e.button.y / sizeSquare * nbSquare + e.button.x /sizeSquare].player == player+1) ){
                                            if( !(game_map[e.button.y / sizeSquare * nbSquare + e.button.x /sizeSquare].player == 0)){
                                                 playerPower(surface, &game_map[0], &game_player[0], player, nbSquare, aspectRatio, e.button.x / sizeSquare, e.button.y / sizeSquare);
                                            }
                                            else{
                                                compteurpouvoir = 1;
                                            }
                                        }
                                        pouvoir3 = 0;

                                    }

                                }

                                if(modeAchat == 1){ // MODE ACHAT
                                int cost;
                                if(unitStat == 1){
                                    cost = 500;
                                }
                                else if(unitStat == 2){
                                    cost = 1000;
                                }
                                else if(unitStat == 3 || unitStat == 4){
                                    cost = 1500;
                                }
                                else if(unitStat == 5){
                                    cost = 2000;
                                }
                                else{
                                    cost = 10000;
                                }
                                if(game_player[player].gold>=cost){
                                        if(game_player[player].nbUnite<10){
                                            int res;
                                            res = squareFree(&game_map[0], e.button.x, e.button.y, nbSquare, surface, unitStat);
                                            if(res == 1){
                                                soldierCreator(e.button.x, e.button.y, nbSquare, surface,
                                                               &game_map[0], unitStat, &game_player[0], player);
                                                game_player[player].nbUnite++;
                                                game_player[player].gold = game_player[player].gold-cost;
                                                modeAchat = 0;
                                                mode == 0;
                                            }
                                            else{
                                                modeAchat = 1;
                                            }
                                        }
                                }
                                else if(game_player[player].gold<cost){
                                        modeAchat = 1;
                                    }
                                }
                                // FIN MODE ACHAT

                                if(displayRange == 1){ // SI L'AFFICHAGE DE LA PORTEE DE TIR EST ACTIVE
                                    printf("mode attaque initié \n");
                                    mode = 1; // MODE ATTAQUE
                                    displayRange = 0;
                                    if( curseurClicX == e.button.x && curseurClicY == e.button.y){ // SI ON CLIQUE SUR L'UNITE POUR ATTAQUER ON NE DOIT PAS FAIRE DE DEPLACEMENT DANS LA BOUCLE SUIVANTE
                                        mouvement = 0;
                                        printf("Suicide non autorisé \n");
                                    }
                                    else{
                                        if(game_map[e.button.y/sizeSquare * nbSquare + e.button.x/sizeSquare].player != 0
                                           && game_map[e.button.y/sizeSquare * nbSquare + e.button.x/sizeSquare].player != player+1)
                                        {
                                            printf("tir initié \n");
                                            if( rangeCheck(&game_map[0], curseurClicX/sizeSquare, curseurClicY/sizeSquare, e.button.x/sizeSquare, e.button.y/sizeSquare, nbSquare) == 1 ){
                                                printf("ATTAQUE !!! \n");
                                                attaque(&game_map[0], &game_player[0], player,
                                                        curseurClicX/sizeSquare,
                                                        curseurClicY/sizeSquare,
                                                        e.button.x/sizeSquare,
                                                        e.button.y/sizeSquare,
                                                        nbSquare);
                                                if( game_map[curseurClicY/sizeSquare * nbSquare + curseurClicX/sizeSquare].player == 0 ){
                                                    printf("unite s'est suicidé \n");
                                                    mouvement = 0;
                                                }

                                            }
                                            else{
                                                printf("TROP LOIN ! \n");
                                            }


                                        }
                                        else{
                                            printf("tir inutile \n");
                                        }
                                    }
                                    modeAchat = 0;
                                    finDeRange = 1;
                                }
                                if(displayRange == 0){ // SI L'AFFFICHAGE DE LA PORTEE N'EST PAS ACTIVE
                                    printf("MODE DEPLACEMENT \n");
                                    mode = 1;
                                    modeAchat = 0;
                                    printf("\naffichage mvt cas 1 \n");
                                    if(mouvement == 0){
                                        printf("affichage mvt cas 2 \n");
                                        curseurClicX = e.button.x;
                                        curseurClicY = e.button.y;
                                    }
                                    unitStat = (curseurClicY/sizeSquare)* nbSquare + (curseurClicX/sizeSquare);
                                    printf("UNIT STAT : %d\n", unitStat);
                                    if ( game_map[unitStat].type_soldier == 0 || game_map[unitStat].player != player+1 ){
                                        printf("affichage mvt cas 3 \n");
                                        unitStat = game_map[unitStat].type_soldier - NBUNITE * player ;
                                        displayRange = 0;
                                    }
                                    else{
                                        printf("affichage mvt cas 4 \n");

                                        printf("DEPLACEMENT UNITE\n");

                                        nullRange(nbSquare, &game_range[0]);
                                        nullRange(nbSquare, &game_mouvement[0]);
                                        unitStat = game_map[unitStat].type_soldier - NBUNITE * player ;
                                        // RANGE //

                                        // MOUVEMENT
                                        if (game_map[(curseurClicY/sizeSquare)* nbSquare + (curseurClicX/sizeSquare)].fuel > 0){
                                            if(mouvement == 1 && finDeRange == 0){
                                                destX = e.button.x/sizeSquare;
                                                destY = e.button.y/sizeSquare;
                                                if(!(destX == curseurClicX/sizeSquare && destY == curseurClicY/sizeSquare)){
                                                    displayRange = 0;
                                                    int range = game_map[curseurClicY/sizeSquare*nbSquare + curseurClicX/sizeSquare].fuel;
                                                    int distance = mapPath(surface, nbSquare, &game_map[0], aspectRatio, curseurClicX/sizeSquare, curseurClicY/sizeSquare, destX, destY);
                                                    if(distance != -1 && distance <= range + 1){
                                                        tamptest = 1;
                                                        indexTemp = 0;
                                                        tamponX = curseurClicX/sizeSquare * sizeSquare + (sizeSquare/2);
                                                        tamponY = curseurClicY/sizeSquare * sizeSquare + (sizeSquare/2);
                                                        stockageTypeSoldat = game_map[curseurClicY/sizeSquare * nbSquare + curseurClicX/sizeSquare].type_soldier;
                                                        tamp = vectorMapPath(surface, nbSquare, &game_map[0], aspectRatio, curseurClicX/sizeSquare, curseurClicY/sizeSquare, destX, destY);
                                                        game_map[curseurClicY/sizeSquare * nbSquare + curseurClicX/sizeSquare].type_soldier = 0;
                                                    }

                                                    //unitMouvement(surface, &game_map[0], &game_player[0], player, curseurClicX/sizeSquare, curseurClicY/sizeSquare, destX, destY, nbSquare, aspectRatio);
                                                }
                                                else{
                                                    printf("case identique à l'origine");
                                                }
                                                mouvement = 0;
                                            }
                                            else{
                                                destX = -1;
                                                destY = -1;
                                                mouvementCreator(surface, &game_mouvement[0], &game_map[0], curseurClicX/sizeSquare, curseurClicY/sizeSquare, nbSquare, unitStat,aspectRatio);
                                                mouvement = 1;
                                            }
                                            //rangeCreator(&game_range[0], &game_map[0], curseurClicX/sizeSquare, curseurClicY/sizeSquare, nbSquare);

                                        }

                                    }
                                    displayRange = 0;
                                }
                            }
                            else{ // CLIC EN MENU
                                //fin du tour
                                displayRange = 0;
                                modeAchat = 0;

                                if(
                                   (e.button.x >= (nbSquare*sizeSquare + ((10*(surface->w-(nbSquare*sizeSquare)))/100)))
                                   &&
                                   (e.button.x <= (nbSquare*sizeSquare + ((90*(surface->w-(nbSquare*sizeSquare)))/100)))
                                   &&
                                   (e.button.y >= 735)
                                   &&
                                   (e.button.y <= 785))
                                    {
                                        if (player == 0)
                                        {
                                            printf("NOMBRE DE VILLE DU JOUEUR ROUGE %d \n", game_player[1].ville );
                                            game_player[0].gold = game_player[0].gold + 500 + (game_player[0].ville * 500);
                                            if (game_player[0].gold>=9999)
                                                {
                                                    game_player[0].gold = 9999;
                                                }
                                            player = 1;
                                            reapprovisionnement(nbSquare, &game_map[0], &game_player[0], player);
                                            nullRange(nbSquare, &game_range[0]);
                                            displayRange = 0;
                                            if(ordi == 1){
                                                //GESTION IA
                                                tourOrdinateur = 1;
                                                tourOrdinateurPurchase = 1;
                                                tourOrdinateurNbFantassin = 0;
                                                tourOrdinateurMouvementFantassin = 1;
                                                numFantassin = 0;

                                                tourOrdinateurNbUnits = 0;
                                                tourOrdinateurMouvementUnits = 1;
                                                numUnits = 0;
                                            }

                                        }
                                        else{
                                                printf("NOMBRE DE VILLE DU JOUEUR BLEU %d \n", game_player[0].ville );
                                                game_player[1].gold = game_player[1].gold + 500 + (game_player[1].ville * 500);
                                                printf("GOLD UPDATE\n");
                                                if (game_player[1].gold>9999)
                                                {
                                                    game_player[1].gold = 9999;
                                                }
                                                player = 0;
                                                printf("PLAYER UPDATE \n");
                                                reapprovisionnement(nbSquare, &game_map[0], &game_player[0], player);
                                                printf("REAPPROVISIONNEMENT UPDATE \n");
                                                nullRange(nbSquare, &game_range[0]);
                                                printf("RANGE UPDATE \n");
                                                displayRange = 0;
                                                printf("DISPLAY RANGE UPDATE \n");

                                        }
                                        mode = 1;
                                        unitStat = 0;
                                        curseurClicX = -1;
                                        curseurClicY = -1;
                                        displayRange = 0;
                                        mouvement = 0;
                                        destX = -1;
                                        destY = -1;
                                        compteurpouvoir = 1;

                                        printf("FIN DE TOUR \n");
                                    }
                                // ABANDON //
                                if(
                                   (e.button.x >= (nbSquare*sizeSquare + ((10*(surface->w-(nbSquare*sizeSquare)))/100)))
                                   &&
                                   (e.button.x <= (nbSquare*sizeSquare + ((90*(surface->w-(nbSquare*sizeSquare)))/100)))
                                   &&
                                   (e.button.y >= 675)
                                   &&
                                   (e.button.y <= 725))
                                    {
                                        if(game_player[player].id == 1){
                                           menu=1;
                                           menuabandon1 = 1;
                                            int pouvoir1 = 0;
                                            int pouvoir2 = 0;
                                            int pouvoir3 = 0;
                                            int pouvoir4 = 0;
                                       }
                                        if(game_player[player].id == 2){
                                            menu = 1;
                                            menuabandon2 = 1;
                                            int pouvoir1 = 0;
                                            int pouvoir2 = 0;
                                            int pouvoir3 = 0;
                                            int pouvoir4 = 0;
                                        }

                                    }

                                // SUPER POUVOIR ///
                                if(
                                   (e.button.x >= (nbSquare*sizeSquare + ((10*(surface->w-(nbSquare*sizeSquare)))/100)))
                                   &&
                                   (e.button.x <= (nbSquare*sizeSquare + ((90*(surface->w-(nbSquare*sizeSquare)))/100)))
                                   &&
                                   (e.button.y >= 583)
                                   &&
                                   (e.button.y <= 633))
                                    {
                                        if( game_player[player].pouvoir == 1){
                                            menu = 1;
                                            pouvoir1 = 1;
                                            playerPower(surface, &game_map[0], &game_player[0], player, nbSquare, aspectRatio, -1, -1);
                                        }
                                        if( game_player[player].pouvoir == 2){
                                            menu = 1;
                                            pouvoir2 = 1;
                                            playerPower(surface, &game_map[0], &game_player[0], player, nbSquare, aspectRatio, -1, -1);
                                        }
                                        if( game_player[player].pouvoir == 3){
                                            menu = 1;
                                            pouvoir3 = 1;
                                            //playerPower(surface, &game_map[0], &game_player[0], player, nbSquare, aspectRatio, 1, 1);
                                        }
                                        if( game_player[player].pouvoir == 4){
                                            menu = 1;
                                            pouvoir4 = 1;
                                            //playerPower(surface, &game_map[0], &game_player[0], player, nbSquare, aspectRatio, 1, 1);
                                        }
                                        printf("POWER !!! \n");
                                    }

                                // CAPTURE
                                if(
                                   (e.button.x >= (nbSquare*sizeSquare + ((10*(surface->w-(nbSquare*sizeSquare)))/100)))
                                   &&
                                   (e.button.x <= (nbSquare*sizeSquare + ((90*(surface->w-(nbSquare*sizeSquare)))/100)))
                                   &&
                                   (e.button.y >= 523)
                                   &&
                                   (e.button.y <= 573))
                                    {
                                        if(curseurClicX != -1 && curseurClicY != -1){
                                            captureBuilding(&game_map[0], &game_player[0], nbSquare,  aspectRatio, player, sizeSquare, curseurClicX/sizeSquare, curseurClicY/sizeSquare);
                                        }

                                    }

                                // ATTAQUE / ACHAT
                                if(
                                   (e.button.x >= (nbSquare*sizeSquare + ((10*(surface->w-(nbSquare*sizeSquare)))/100)))
                                   &&
                                   (e.button.x <= (nbSquare*sizeSquare + ((90*(surface->w-(nbSquare*sizeSquare)))/100)))
                                   &&
                                   (e.button.y >= 463)
                                   &&
                                   (e.button.y <= 513))
                                    {
                                        if(mode == 0){
                                            modeAchat = 1;
                                        }
                                        if(modeAchat == 0 && curseurClicX != -1 && curseurClicY != -1 ){
                                           printf("ATTAQUE !!! \n");
                                           printf("case n* %d \n", curseurClicY/sizeSquare*nbSquare+curseurClicX/sizeSquare);
                                           if ( game_map[curseurClicY/sizeSquare*nbSquare+curseurClicX/sizeSquare].type_soldier == 0){
                                                mouvement = 0;
                                                printf("case selectionné non soldat \n");
                                                printf("case n* %d \n", curseurClicY/sizeSquare*nbSquare+curseurClicX/sizeSquare);
                                            }
                                            else{
                                                printf("bon soldat \n");
                                                nullRange(nbSquare, &game_range[0]);
                                                nullRange(nbSquare, &game_mouvement[0]);
                                                unitStat = game_map[(curseurClicY/sizeSquare)* nbSquare + (curseurClicX/sizeSquare)].type_soldier - NBUNITE * player ;
                                                // RANGE //
                                                if (game_map[(curseurClicY/sizeSquare)* nbSquare + (curseurClicX/sizeSquare)].action == 1){
                                                    displayRange = 1;
                                                    rangeCreator(&game_range[0], &game_map[0], curseurClicX/sizeSquare, curseurClicY/sizeSquare, nbSquare);
                                                    //mouvementCreator(surface, &game_mouvement[0], &game_map[0], curseurClicX/sizeSquare, curseurClicY/sizeSquare, nbSquare, unitStat,aspectRatio);
                                                }
                                                // MOUVEMENT
                                                /*
                                                if (game_map[(curseurClicY/sizeSquare)* nbSquare + (curseurClicX/sizeSquare)].fuel > 0){
                                                    if(mouvement == 1){
                                                        destX = e.button.x/sizeSquare;
                                                        destY = e.button.y/sizeSquare;
                                                        if(!(destX == curseurClicX/sizeSquare && destY == curseurClicY/sizeSquare)){
                                                            unitMouvement(surface, &game_map[0], &game_player[0], player, curseurClicX/sizeSquare, curseurClicY/sizeSquare, destX, destY, nbSquare, aspectRatio);
                                                        }
                                                        else{
                                                            printf("case identique � l'origine");
                                                        }
                                                        mouvement = 0;
                                                    }
                                                    else{
                                                        destX = -1;
                                                        destY = -1;
                                                        mouvementCreator(surface, &game_mouvement[0], &game_map[0], curseurClicX/sizeSquare, curseurClicY/sizeSquare, nbSquare, unitStat,aspectRatio);
                                                        mouvement = 1;
                                                    }
                                                    //rangeCreator(&game_range[0], &game_map[0], curseurClicX/sizeSquare, curseurClicY/sizeSquare, nbSquare);

                                                }
                                                */
                                            }
                                        }
                                    }


                                // SELECTION UNITS

                                if((e.button.x >= 820)&&(e.button.x <= 872)&&(e.button.y >= 88)&&(e.button.y <= 140))
                                    {
                                        printf("soldier !!! \n");
                                        unitStat = 1;
                                        mode = 0;
                                    }
                                if((e.button.x >= 874)&&(e.button.x <= 926)&&(e.button.y >= 88)&&(e.button.y <= 140))
                                    {
                                        printf("bazooka !!! \n");
                                        unitStat = 2;
                                        mode = 0;
                                    }
                                if((e.button.x >= 928)&&(e.button.x <= 980)&&(e.button.y >= 88)&&(e.button.y <= 140))
                                    {
                                        printf("tank !!! \n");
                                        unitStat = 3;
                                        mode = 0;
                                    }
                                if((e.button.x >= 820)&&(e.button.x <= 872)&&(e.button.y >= 142)&&(e.button.y <= 194))
                                    {
                                        printf("helicopter !!! \n");
                                        unitStat = 4;
                                        mode = 0;
                                    }
                                if((e.button.x >= 874)&&(e.button.x <= 926)&&(e.button.y >= 142)&&(e.button.y <= 194))
                                    {
                                        printf("ship !!! \n");
                                        unitStat = 5;
                                        mode = 0;
                                    }
                                }


                                //
                            break;

                        /* Touche clavier */
                        case SDL_KEYDOWN:
                            printf("echo 13");
                            printf("touche pressee (code = %d)\n", e.key.keysym.sym);
                            break;
                        default:
                            break;
                    }
                }

            }


            Uint32 elapsedTime = SDL_GetTicks() - startTime;
            /* Si trop peu de temps s'est ecoule, on met en pause le programme */
            if(elapsedTime < FRAMERATE_MILLISECONDS)
            {
                SDL_Delay(FRAMERATE_MILLISECONDS - elapsedTime);
            }

        }
    }

    free(game_range);
    free(game_mouvement);
    return loop;

}
