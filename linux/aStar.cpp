/////////////////////////////////////////////////
//                 aSTAR.CPP                   //
//                                             //
//     by Gartner Esther & Briere Benjamin     //
//                                             //
/////////////////////////////////////////////////
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

#include "structures.h"
#include "map.h"
#include "unit.h"
#include "menu.h"
#include "player.h"
#include "game.h"
#include "aStar.h"

#define MAXUNITE 10
#define NBUNITE 5



void enfiler(file *file, int x, int y, int g, int h, int f, int parentX, int parentY){

    node *nouveau = NULL;
    nouveau = (node *) malloc(sizeof(node));
    if (file == NULL || nouveau == NULL)
    {
        exit(EXIT_FAILURE);
    }

    nouveau->x = x;
    nouveau->y = y;
    nouveau->h = h;
    nouveau->g = g;
    nouveau->f = f;
    nouveau->suivant = NULL;
    nouveau->parent.first = parentX;
    nouveau->parent.second = parentY;

    if (file->first != NULL) /* La file n'est pas vide */
    {
        /* On se positionne � la fin de la file */
        file->taille++;
        node *elementActuel = file->first;
        while (elementActuel->suivant != NULL)
        {
            elementActuel = elementActuel->suivant;
                //printf("ajout de X %d  Y %d \n", elementActuel->x, elementActuel->y);
                //printf("valeur de parent X : %d \n", elementActuel->parent.first);
                //printf("valeur de parent Y : %d \n\n", elementActuel->parent.second);
        }
        elementActuel->suivant = nouveau;

    }
    else /* La file est vide, notre �l�ment est le premier */
    {
        file->first = nouveau;
        file->taille++;
    }
}

void ajouterArbre(tree_node *arbre, int x, int y, int g, int h, int f, int parentX, int parentY){

    tree_node *tmpNode;
    tree_node *tmpTree = arbre;

    tree_node *nouveau = NULL;
    nouveau = (tree_node *) malloc(sizeof(tree_node));

    nouveau->x = x;
    nouveau->y = y;
    nouveau->h = h;
    nouveau->g = g;
    nouveau->f = f;
    nouveau->left = NULL;
    nouveau->right = NULL;
    nouveau->parent.first = parentX;
    nouveau->parent.second = parentY;

    if (arbre == NULL || nouveau == NULL)
    {
        exit(EXIT_FAILURE);
    }

    if (arbre->x == -1 && arbre->y == -1) /* La file n'est pas vide */
    {
            //printf("ARBRE VIDE \n");
            arbre->x = x;
            arbre->y = y;
            arbre->h = h;
            arbre->g = g;
            arbre->f = f;
            arbre->left = NULL;
            arbre->right = NULL;
            arbre->parent.first = parentX;
            arbre->parent.second = parentY;
            //printf("valeur de x dans l'arbre : %d \n", arbre->x);
    }
    else{

        //printf("AJOUTER FEUILLE \n");

        do{
            tmpNode = tmpTree;
            if(x > tmpTree->x )
            {
                //printf("droite \n");
                tmpTree = tmpTree->right;
                if(!tmpTree) tmpNode->right = nouveau;
            }
            else
            {
                //printf("gauche \n");
                tmpTree = tmpTree->left;
                if(!tmpTree) tmpNode->left = nouveau;
            }
        }
        while(tmpTree);
    }
}

void enfilerNode(file *file, node *n){

    node *nouveau;
    nouveau = (node *) malloc(sizeof(node));
    if (file == NULL || nouveau == NULL)
    {
        exit(EXIT_FAILURE);
    }

    nouveau->x = n->x;
    nouveau->y = n->y;
    nouveau->h = n->h;
    nouveau->g = n->g;
    nouveau->f = n->f;
    //printf("valeur de parent X : %d \n", n->parent.first);
    //printf("valeur de parent Y : %d \n", n->parent.second);
    nouveau->parent.first = n->parent.first;
    nouveau->parent.second = n->parent.second;
    nouveau->suivant = NULL;

    if (file->first != NULL) /* La file n'est pas vide */
    {
        /* On se positionne � la fin de la file */
        file->taille++;
        node *elementActuel = file->first;
        while (elementActuel->suivant != NULL)
        {
            elementActuel = elementActuel->suivant;
        }
        elementActuel->suivant = nouveau;
        //printf("valeur de parent X apr�s enfilage : %d \n", elementActuel->suivant->parent.first);
        //printf("valeur de parent Y apr�s enfilage : %d \n", elementActuel->suivant->parent.second);

    }
    else /* La file est vide, notre �l�ment est le premier */
    {
        file->first = nouveau;
        file->taille++;
    }
}

void lectureFile(file *file){

    if (file == NULL)
    {
        exit(EXIT_FAILURE);
    }

    if (file->first != NULL) /* La file n'est pas vide */
    {
        printf("======================== LECTURE FILE ========================\n");
        printf("=                                                            =\n");
        /* On se positionne � la fin de la file */
        node *elementActuel = file->first;
        while (elementActuel != NULL)
        {
            printf("= NODE CURRENT   X : %d | Y : %d | F : %d | G : %d | H : %d       =\n", elementActuel->x, elementActuel->y, elementActuel->f, elementActuel->g, elementActuel->h);
            printf("= NODE PARENT    X : %d | Y : %d                               =\n", elementActuel->parent.first, elementActuel->parent.second);
            printf("=                                                            =\n");

            elementActuel = elementActuel->suivant;
        }
        printf("==============================================================\n\n");
    }
    else /* La file est vide, notre �l�ment est le premier */
    {
        printf("============== FILE VIDE ================\n");
    }
}

void parents( node *enfant, node *p){

    if(p->parent.first == NULL){
        printf("NULL \n");
    }
    else{
        printf("PARENT A DES GRANDS PARENTS \n");
    }
}

void retirerFile(file *file, int x, int y){

    if (file == NULL)
    {
        exit(EXIT_FAILURE);
    }

    if (file->first != NULL)
    {

        if(file->first->suivant == NULL){ // CAS IL N'Y A QU'UNE SEULE VALEUR DANS LA FILE
           file->taille--;
           file->first= NULL;
        }
        else{
            node *elementDefile = file->first;
            if(elementDefile->x == x && elementDefile->y == y){ // CAS VALEUR A ENLEVER PREMIERE CASE
                elementDefile->x = elementDefile->suivant->x;
                elementDefile->y = elementDefile->suivant->y;
                elementDefile->f = elementDefile->suivant->f;
                elementDefile->g = elementDefile->suivant->g;
                elementDefile->h = elementDefile->suivant->h;
                elementDefile->suivant = elementDefile->suivant->suivant;
            }
            else{
                for(int i = 0 ; i < file->taille ; i++){
                   if(elementDefile->suivant->x != x && elementDefile->suivant->y != y){
                        elementDefile = elementDefile->suivant;
                    }
                }
                elementDefile->suivant = elementDefile->suivant->suivant;
            }
            file->taille--;
        }
    }
}

int rechercherFile(file *file, int x, int y){
    int res = 0;


    if (file == NULL)
    {
        exit(EXIT_FAILURE);
    }

    if (file->first != NULL) /* La file n'est pas vide */
    {
        /* On se positionne � la fin de la file */
        node *elementActuel = file->first;
        while (elementActuel->suivant != NULL)
        {
            if(elementActuel->x == x && elementActuel->y == y){
                res = 1;
            }
            elementActuel = elementActuel->suivant;
        }

    }
    return res;
}

int rechercherArbre(tree_node *tree, int x, int y){

    //printf("Rechercher Arbre \n");
    //printf("x : %d et y : %d \n", x, y);
    //printf("tree =  x : %d et y : %d \n", tree->x, tree->y);

    while(tree)
    {
        if(y*10+x == (tree->y)*10+tree->x ){
            //printf(" val 1 : %d et val 2 : %d \n", y*10+x, (tree->y)*10+tree->x );
            return 1;
        }
        if(x > tree->x ){
            //printf(" \- %d %d \n", tree->x, tree->y);
            tree = tree->right;
        }
        else{
            //printf(" -/ %d %d \n", tree->x, tree->y);
            tree = tree->left;
        }
    }
    return 0;

}

void backFile(file *file, int * x, int * y){
    int res = 0;

    if (file == NULL)
    {
        exit(EXIT_FAILURE);
    }

    if (file->first != NULL) /* La file n'est pas vide */
    {
        /* On se positionne � la fin de la file */
        node *elementActuel = file->first;
        while (elementActuel != NULL)
        {
            //printf("---------------------------------------------------------- ELEMENT X : %d, Y : %d\n", elementActuel->x, elementActuel->y);
            if(elementActuel->x == *x && elementActuel->y == *y){
                * x = elementActuel->parent.first;
                * y = elementActuel->parent.second;
                //printf("---------------------------------------------------------- PARENT X : %d, Y : %d\n", elementActuel->parent.first, elementActuel->parent.second);
            }
            elementActuel = elementActuel->suivant;
        }

    }
    //printf("---------------------------------------------------------- NOUVEAU X : %d, Y : %d\n", *x, *y);

}

void displayObstacle(SDL_Surface* surface, int nbSquare, map_square *game_map, float aspectRatio){
    int spaceBetween = 1;
    int sizeSquare;
    if(surface->w / nbSquare > (surface->h / nbSquare)){
        sizeSquare = (surface->h - (nbSquare-1)*spaceBetween) / nbSquare;
    }
    else{
        sizeSquare = (surface->w - (nbSquare-1)*spaceBetween) / nbSquare;
    }
    for(int y = 0 ; y< nbSquare ; y++)
    {
        for (int x = 0; x < nbSquare ; x++)
        {
            glEnable(GL_BLEND);
            glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
            if( game_map[y*nbSquare+x].ground == 5 || game_map[y*nbSquare+x].ground == 3 ){
                glColor4ub(0,0,0,150);
            }
            else{
                glColor4ub(255,255,255,200);
            }
            drawSquare(surface->w,
                       surface->h,
                       ((sizeSquare/2)+x*(sizeSquare)+(x*spaceBetween)),
                       ((sizeSquare/2)+y*(sizeSquare)+(y*spaceBetween)),
                       sizeSquare,
                       sizeSquare,
                       aspectRatio);
            glDisable(GL_BLEND);
        }
    }
}

void displayOpenList(SDL_Surface* surface, file *file, int nbSquare, map_square *game_map, float aspectRatio){

    int spaceBetween = 1;
    int sizeSquare;
    if(surface->w / nbSquare > (surface->h / nbSquare)){
        sizeSquare = (surface->h - (nbSquare-1)*spaceBetween) / nbSquare;
    }
    else{
        sizeSquare = (surface->w - (nbSquare-1)*spaceBetween) / nbSquare;
    }

    if (file == NULL)
    {
        exit(EXIT_FAILURE);
    }

    //printf("valeurs d'openList 1 : X = %d | Y = %d \n", file->first->x, file->first->y);
    /* On v�rifie s'il y a quelque chose � d�filer */
    while (file->first != NULL)
    {
        node *elementDefile = file->first;
        //printf("valeurs d'openList 2 : X = %d | Y = %d \n", file->first->x, file->first->y);
            glEnable(GL_BLEND);
            glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
            glColor4ub(0,255,0,100);
            drawSquare(surface->w,
                       surface->h,
                       ((sizeSquare/2)+file->first->x*(sizeSquare)+(file->first->x*spaceBetween)),
                       ((sizeSquare/2)+file->first->y*(sizeSquare)+(file->first->y*spaceBetween)),
                       sizeSquare,
                       sizeSquare,
                       aspectRatio);
            glDisable(GL_BLEND);

        file->first = elementDefile->suivant;
    }
}

void displayClosedList(SDL_Surface* surface, file *file, int nbSquare, map_square *game_map, float aspectRatio){

    int spaceBetween = 1;
    int sizeSquare;
    if(surface->w / nbSquare > (surface->h / nbSquare)){
        sizeSquare = (surface->h - (nbSquare-1)*spaceBetween) / nbSquare;
    }
    else{
        sizeSquare = (surface->w - (nbSquare-1)*spaceBetween) / nbSquare;
    }

    if (file == NULL)
    {
        exit(EXIT_FAILURE);
    }

    //printf("valeurs d'openList 1 : X = %d | Y = %d \n", file->first->x, file->first->y);
    /* On v�rifie s'il y a quelque chose � d�filer */
    while (file->first != NULL)
    {
        node *elementDefile = file->first;
        //printf("valeurs d'openList 2 : X = %d | Y = %d \n", file->first->x, file->first->y);
            glEnable(GL_BLEND);
            glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
            glColor4ub(255,0,0,100);
            drawSquare(surface->w,
                       surface->h,
                       ((sizeSquare/2)+file->first->x*(sizeSquare)+(file->first->x*spaceBetween)),
                       ((sizeSquare/2)+file->first->y*(sizeSquare)+(file->first->y*spaceBetween)),
                       sizeSquare,
                       sizeSquare,
                       aspectRatio);
            glDisable(GL_BLEND);

        file->first = elementDefile->suivant;
    }
}

void neighbors(node *current, map_square *game_map, int nbSquare, int type){
    // case TOP
    if(current->y-1>=0){
        if( type == 5 || type == 10 ){
            if(game_map[((current->y)-1)*nbSquare+(current->x)].ground == 3 && game_map[((current->y)-1)*nbSquare+(current->x)].type_soldier == 0)
            {
                current->neighbors[0].first = (current->y)-1;
                current->neighbors[0].second = current->x;
            }
            else{
                current->neighbors[0].first = -1;
                current->neighbors[0].second = -1;
            }
        }
        else if( type == 4 || type == 9){
            if(game_map[((current->y)-1)*nbSquare+(current->x)].ground != 5 && game_map[((current->y)-1)*nbSquare+(current->x)].type_soldier == 0)
            {
                current->neighbors[0].first = (current->y)-1;
                current->neighbors[0].second = current->x;
            }
            else{
                current->neighbors[0].first = -1;
                current->neighbors[0].second = -1;
            }
        }
        else{
            if(game_map[((current->y)-1)*nbSquare+(current->x)].ground != 3 && game_map[((current->y)-1)*nbSquare+(current->x)].ground != 5 && game_map[((current->y)-1)*nbSquare+(current->x)].type_soldier == 0)
            {
                current->neighbors[0].first = (current->y)-1;
                current->neighbors[0].second = current->x;
            }
            else{
                current->neighbors[0].first = -1;
                current->neighbors[0].second = -1;
            }
        }

    }
    else{
        current->neighbors[0].first = -1;
        current->neighbors[0].second = -1;
    }

    // case RIGHT
    if(current->x+1<nbSquare){
        if( type == 5 || type == 10 ){
            if(game_map[current->y*nbSquare+((current->x)+1)].ground == 3 && game_map[current->y*nbSquare+((current->x)+1)].type_soldier == 0)
            {
                current->neighbors[1].first = current->y;
                current->neighbors[1].second = current->x+1;
            }
            else{
                current->neighbors[1].first = -1;
                current->neighbors[1].second = -1;
            }
        }
        else if( type == 4 || type == 9){
            if(game_map[current->y*nbSquare+((current->x)+1)].ground != 5 && game_map[current->y*nbSquare+((current->x)+1)].type_soldier == 0)
            {
                current->neighbors[1].first = current->y;
                current->neighbors[1].second = current->x+1;
            }
            else{
                current->neighbors[1].first = -1;
                current->neighbors[1].second = -1;
            }
        }
        else{
            if(game_map[current->y*nbSquare+((current->x)+1)].ground != 3 && game_map[current->y*nbSquare+((current->x)+1)].ground != 5 && game_map[current->y*nbSquare+((current->x)+1)].type_soldier == 0)
            {
                current->neighbors[1].first = current->y;
                current->neighbors[1].second = current->x+1;
            }
            else{
                current->neighbors[1].first = -1;
                current->neighbors[1].second = -1;
            }
        }
    }
    else{
        current->neighbors[1].first = -1;
        current->neighbors[1].second = -1;
    }

    // case BOTTOM

    if(current->y+1<nbSquare){
        if( type == 5 || type == 10 ){
            if(game_map[((current->y)+1)*nbSquare+((current->x))].ground == 3 && game_map[(current->y+1)*nbSquare+((current->x))].type_soldier == 0)
            {
                current->neighbors[2].first = current->y+1;
                current->neighbors[2].second = current->x;
            }
            else{
                current->neighbors[2].first = -1;
                current->neighbors[2].second = -1;
            }
        }
        else if( type == 4 || type == 9){
            if(game_map[((current->y)+1)*nbSquare+((current->x))].ground != 5 && game_map[(current->y+1)*nbSquare+((current->x))].type_soldier == 0)
            {
                current->neighbors[2].first = current->y+1;
                current->neighbors[2].second = current->x;
            }
            else{
                current->neighbors[2].first = -1;
                current->neighbors[2].second = -1;
            }
        }
        else{
            if(game_map[(current->y+1)*nbSquare+((current->x))].ground != 3 && game_map[(current->y+1)*nbSquare+((current->x))].ground != 5 && game_map[(current->y+1)*nbSquare+((current->x))].type_soldier == 0)
            {
                current->neighbors[2].first = current->y+1;
                current->neighbors[2].second = current->x;
            }
            else{
                current->neighbors[2].first = -1;
                current->neighbors[2].second = -1;
            }
        }
    }
    else{
        current->neighbors[2].first = -1;
        current->neighbors[2].second = -1;
    }
    // case LEFT
    if(current->x-1>=0){
        if( type == 5 || type == 10){
            if(game_map[current->y*nbSquare+((current->x)-1)].ground == 3 && game_map[current->y*nbSquare+((current->x)-1)].type_soldier == 0)
            {
                current->neighbors[3].first = current->y;
                current->neighbors[3].second = (current->x)-1;
            }
            else{
                current->neighbors[3].first = -1;
                current->neighbors[3].second = -1;
            }
        }
        else if( type == 4 || type == 9){
            if(game_map[current->y*nbSquare+((current->x)-1)].ground != 5 && game_map[current->y*nbSquare+((current->x)-1)].type_soldier == 0)
            {
                current->neighbors[3].first = current->y;
                current->neighbors[3].second = (current->x)-1;
            }
            else{
                current->neighbors[3].first = -1;
                current->neighbors[3].second = -1;
            }
        }
        else{
            if(game_map[current->y*nbSquare+((current->x)-1)].ground != 3 && game_map[current->y*nbSquare+((current->x)-1)].ground != 5 && game_map[current->y*nbSquare+((current->x)-1)].type_soldier == 0)
            {
                current->neighbors[3].first = current->y;
                current->neighbors[3].second = (current->x)-1;
            }
            else{
                current->neighbors[3].first = -1;
                current->neighbors[3].second = -1;
            }
        }
    }
    else{
        current->neighbors[3].first = -1;
        current->neighbors[3].second = -1;
    }
}

int heuristic(node *current, node *endPath){
    int part1 = abs(current->x - endPath->x);
    int part2 = abs(current->y - endPath->y);
    //printf("------ valeur de part1 : %d et part2 : %d \n",part1,part2);
    return (part1+part2);
}

void trouverParent(vector <node_parents> n, int x, int y, int *px, int *py){

    int find = 0;
    for (int i = 0; i < n.size(); i++){
        if(find == 0){
            if(n[i].x == x && n[i].y == y){
                *px = n[i].px;
                *py = n[i].py;
                find = 1;
            }
        }
    }
}

void creerPath(vector <node_parents> *n, vector < pair<int,int> > *p, int debutX, int debutY, int finX, int finY){
    int px;
    int py;
    int temp = 1;
    pair<int,int> parent;
    parent.first = finX;
    parent.second = finY;
    p->push_back(parent);

    while(temp){
            if (finX == debutX && finY == debutY){
                temp = 0;
            }
            else{
                //printf("recherche parent");
                trouverParent(*n, finX, finY, &px, &py );
                parent.first = px;
                parent.second = py;
                p->push_back(parent);
                finX = px;
                finY = py;
            }
    }
}

void displayPath(SDL_Surface* surface, vector < pair<int,int> > p, int nbSquare, map_square *game_map, float aspectRatio, int debutX, int debutY){
    int spaceBetween = 1;
    int sizeSquare;
    if(surface->w / nbSquare > (surface->h / nbSquare)){
        sizeSquare = (surface->h - (nbSquare-1)*spaceBetween) / nbSquare;
    }
    else{
        sizeSquare = (surface->w - (nbSquare-1)*spaceBetween) / nbSquare;
    }

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glColor4ub(255,255,0,200);
    drawSquare(surface->w,
               surface->h,
               ((sizeSquare/2)+(debutX)*(sizeSquare)+((debutX)*spaceBetween)),
               ((sizeSquare/2)+(debutY)*(sizeSquare)+((debutY)*spaceBetween)),
               sizeSquare,
               sizeSquare,
               aspectRatio);
    glDisable(GL_BLEND);
    //printf("valeurs d'openList 1 : X = %d | Y = %d \n", file->first->x, file->first->y);
    /* On v�rifie s'il y a quelque chose � d�filer */
    for (int i = 0; i < p.size(); i++){

        //printf("valeurs d'openList 2 : X = %d | Y = %d \n", file->first->x, file->first->y);
            glEnable(GL_BLEND);
            glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
            glColor4ub(0,0,255,200);
            drawSquare(surface->w,
                       surface->h,
                       ((sizeSquare/2)+( p[i].first)*(sizeSquare)+((p[i].first)*spaceBetween)),
                       ((sizeSquare/2)+(p[i].second)*(sizeSquare)+((p[i].second)*spaceBetween)),
                       sizeSquare,
                       sizeSquare,
                       aspectRatio);
            glDisable(GL_BLEND);

    }
}

float solVoisin(map_square *game_map, int x, int y, int nbSquare){
    int ground = game_map[y*nbSquare+x].ground;
    if(ground == 1 || ground == 3 || ground == 7){
        return 1;
    }
    if(ground == 2 || ground == 4){
        return 2;
    }
    if(ground == 6){
        return 0.5;
    }
}


float mapPath(SDL_Surface* surface, int nbSquare, map_square *game_map, float aspectRatio, int startX, int startY, int clicX, int clicY){

    if (clicX != -1 && clicY != -1)
    {

        node startPath;
        startPath.x = startX;
        startPath.y = startY;
        startPath.g = 0.0;
        int type = game_map[startY*nbSquare+startX].type_soldier;

        float distance = 0.0;

        startPath.parent.first = -1;
        startPath.parent.second = -1;

        //neighbors(&startPath, &game_map[0], nbSquare, type);
/*
        printf("\nTEST ARBRE \n ");
        tree_node testTree;
        testTree.x = -1;
        testTree.y = -1;
        testTree.left = NULL;
        testTree.right = NULL;

        ajouterArbre(&testTree, 1,1,1,1,1,1,1);
        ajouterArbre(&testTree, 1,2,1,1,1,1,1);
        ajouterArbre(&testTree, 1,3,1,1,1,1,1);
        ajouterArbre(&testTree, 2,2,1,1,1,1,1);
        ajouterArbre(&testTree, 1,4,1,1,1,1,1);
        ajouterArbre(&testTree, 1,5,1,1,1,1,1);
        ajouterArbre(&testTree, 2,3,1,1,1,1,1);
        ajouterArbre(&testTree, 4,1,1,1,1,1,1);
        ajouterArbre(&testTree, 4,2,1,1,1,1,1);
        ajouterArbre(&testTree, 5,3,1,1,1,1,1);
        ajouterArbre(&testTree, 6,3,1,1,1,1,1);
        printf("(1,7) est-il dans l'arbre ? : %d \n\n", rechercherArbre(&testTree,1,7));
        printf("(1,4) est-il dans l'arbre ? : %d \n\n", rechercherArbre(&testTree,1,4));
        printf("(2,3) est-il dans l'arbre ? : %d \n\n", rechercherArbre(&testTree,2,3));
        printf("(6,3) est-il dans l'arbre ? : %d \n\n", rechercherArbre(&testTree,6,3));
        printf("(7,3) est-il dans l'arbre ? : %d \n\n", rechercherArbre(&testTree,7,3));
*/

        node endPath;
        endPath.x = clicX;
        endPath.y = clicY;
        endPath.g = 0.0;
        endPath.f = 0;
        endPath.h = 0;
        //neighbors(&endPath, &game_map[0], nbSquare);

        startPath.f = heuristic(&startPath, &endPath);
        startPath.h = startPath.g + startPath.f;

        file openList;
        openList.first = NULL;
        openList.taille = 0;
        tree_node closedList;
        closedList.left = NULL;
        closedList.right = NULL;

        int cheminTrouve = 0;

        vector< pair<int,int> > path;

        enfiler(&openList, startPath.x, startPath.y, startPath.g, startPath.h, startPath.f, startPath.parent.first, startPath.parent.second );

            node winner;
            winner.x = 0;
            winner.y = 0;
            winner.g = 0.0;
            winner.f = startPath.f;
            winner.h = startPath.h;
            winner.parent.first = -1;
            winner.parent.second = -1;

            node current;
            current.x = 0;
            current.y = 0;
            current.g = 0.0;
            current.f = 0;
            current.h = 0;
            current.parent.first = -1;
            current.parent.second = -1;

            node *elementDefile = openList.first;

            vector<node_parents> np;
            node_parents p;
            /*
            p.x = 3;
            p.y = 2;
            p.px = 1;
            p.py = 4;
            np.push_back(p);

            printf("valeur du vecteur 1 : x = %d | y = %d | px = %d | py = %d \n", np[0].x, np[0].y, np[0].px, np[0].py);
            */

    int res = 0;
    while(res == 0 ){
         if(openList.taille > 0){
            elementDefile = openList.first;
            winner.x = openList.first->x;
            winner.y = openList.first->y;
            winner.g = openList.first->g;
            winner.f = openList.first->f;
            winner.h = openList.first->h;
            winner.parent.first = openList.first->parent.first;
            winner.parent.second = openList.first->parent.second;
            for( int i = 0; i<openList.taille; i++){
                if(elementDefile->f < winner.f){
                    winner.x = elementDefile->x;
                    winner.y = elementDefile->y;
                    winner.g = elementDefile->g;
                    winner.f = elementDefile->f;
                    winner.h = elementDefile->h;
                    winner.parent.first = elementDefile->parent.first;
                    winner.parent.second = elementDefile->parent.second;

               }
               elementDefile = elementDefile->suivant;
            }

            current.x = winner.x;
            current.y = winner.y;
            current.f = winner.f;
            current.g = winner.g;
            current.h = winner.h;
            current.parent.first = winner.parent.first;
            current.parent.second = winner.parent.second;

            neighbors(&current, &game_map[0], nbSquare, type);
            //printf("DISTANCE BIS : %f \n",current.g);
            if(current.x == endPath.x && current.y == endPath.y){
                int resX = current.parent.first;
                int resY = current.parent.second;

                creerPath(&np, &path, startPath.x, startPath.y, endPath.x, endPath.y);
                //distance = current.f;
                for(int j=0; j < (path.size() - 1) ; j++){
                    //printf("CASE %d %d -- ", path[j].first, path[j].second );
                    distance = distance + solVoisin(&game_map[0], path[j].first, path[j].second, nbSquare);
                }
                //printf("\n ");


                res = 1;
                cheminTrouve =  1;
            }

            retirerFile(&openList, current.x, current.y);
            int tX = current.parent.first;
            int tY = current.parent.second;
            ajouterArbre(&closedList, current.x, current.y, current.g, current.h, current.f, tX, tY);

            for (int i = 0; i< 4; i++){
                if(rechercherArbre(&closedList, current.neighbors[i].second, current.neighbors[i].first) == 0
                   &&current.neighbors[i].first != -1
                   && current.neighbors[i].second != -1)
                {
                    node neighbor;
                    neighbor.x = current.neighbors[i].second;
                    neighbor.y = current.neighbors[i].first;
                    neighbor.f = current.f;
                    neighbor.h = 1;


                    float tempG = current.g + solVoisin(&game_map[0], current.x, current.y, nbSquare);
                    //printf("SOL VOISIN : %f \n", tempG);

                    if(rechercherFile(&openList, current.neighbors[i].second, current.neighbors[i].first) == 1){
                        if(tempG < neighbor.g)
                        {
                            neighbor.g = tempG;
                        }
                    } else{
                        neighbor.g = tempG;
                        //printf("neighbor.g : %f \n", neighbor.g);
                        int h = heuristic(&neighbor, &endPath);
                        neighbor.h = h;
                        neighbor.f = neighbor.h + neighbor.g;

                        int tempX = current.x;
                        int tempY = current.y;

                        neighbor.parent.first = tempX;
                        neighbor.parent.second = tempY;

                        node_parents n;
                        n.x = neighbor.x;
                        n.y = neighbor.y;
                        n.px = tempX;
                        n.py = tempY;
                        np.push_back(n);
                        enfiler(&openList, neighbor.x, neighbor.y, neighbor.g, neighbor.h, neighbor.f, tempX, tempY);
                    }
                }
            }



        }
        else{
            res = 1;
            cheminTrouve = 0;
        }
    }

        //free(&testTree);
        if(cheminTrouve == 1){
            //displayOpenList(surface, &openList, nbSquare, &game_map[0], aspectRatio);
            //displayClosedList(surface, &closedList, nbSquare, &game_map[0], aspectRatio);
            //displayPath(surface, path, nbSquare, &game_map[0], aspectRatio, startPath.x, startPath.y);
            //printf("DISTANCE : %f \n", distance);
            return distance;
        }
        else{
            return -1;
        }
    }
}


vector < pair<int,int> > vectorMapPath(SDL_Surface* surface, int nbSquare, map_square *game_map, float aspectRatio, int startX, int startY, int clicX, int clicY){

    if (clicX != -1 && clicY != -1)
    {

        node startPath;
        startPath.x = startX;
        startPath.y = startY;
        startPath.g = 0;
        int type = game_map[startY*nbSquare+startX].type_soldier;

        startPath.parent.first = -1;
        startPath.parent.second = -1;

        //neighbors(&startPath, &game_map[0], nbSquare, type);
/*
        printf("\nTEST ARBRE \n ");
        tree_node testTree;
        testTree.x = -1;
        testTree.y = -1;
        testTree.left = NULL;
        testTree.right = NULL;

        ajouterArbre(&testTree, 1,1,1,1,1,1,1);
        ajouterArbre(&testTree, 1,2,1,1,1,1,1);
        ajouterArbre(&testTree, 1,3,1,1,1,1,1);
        ajouterArbre(&testTree, 2,2,1,1,1,1,1);
        ajouterArbre(&testTree, 1,4,1,1,1,1,1);
        ajouterArbre(&testTree, 1,5,1,1,1,1,1);
        ajouterArbre(&testTree, 2,3,1,1,1,1,1);
        ajouterArbre(&testTree, 4,1,1,1,1,1,1);
        ajouterArbre(&testTree, 4,2,1,1,1,1,1);
        ajouterArbre(&testTree, 5,3,1,1,1,1,1);
        ajouterArbre(&testTree, 6,3,1,1,1,1,1);
        printf("(1,7) est-il dans l'arbre ? : %d \n\n", rechercherArbre(&testTree,1,7));
        printf("(1,4) est-il dans l'arbre ? : %d \n\n", rechercherArbre(&testTree,1,4));
        printf("(2,3) est-il dans l'arbre ? : %d \n\n", rechercherArbre(&testTree,2,3));
        printf("(6,3) est-il dans l'arbre ? : %d \n\n", rechercherArbre(&testTree,6,3));
        printf("(7,3) est-il dans l'arbre ? : %d \n\n", rechercherArbre(&testTree,7,3));
*/

        node endPath;
        endPath.x = clicX;
        endPath.y = clicY;
        endPath.g = 0;
        endPath.f = 0;
        endPath.h = 0;
        //neighbors(&endPath, &game_map[0], nbSquare);

        startPath.f = heuristic(&startPath, &endPath);
        startPath.h = startPath.g + startPath.f;

        file openList;
        openList.first = NULL;
        openList.taille = 0;
        tree_node closedList;
        closedList.left = NULL;
        closedList.right = NULL;

        int cheminTrouve = 0;

        vector< pair<int,int> > path;

        enfiler(&openList, startPath.x, startPath.y, startPath.g, startPath.h, startPath.f, startPath.parent.first, startPath.parent.second );

            node winner;
            winner.x = 0;
            winner.y = 0;
            winner.g = 0;
            winner.f = startPath.f;
            winner.h = startPath.h;
            winner.parent.first = -1;
            winner.parent.second = -1;

            node current;
            current.x = 0;
            current.y = 0;
            current.g = 0;
            current.f = 0;
            current.h = 0;
            current.parent.first = -1;
            current.parent.second = -1;

            node *elementDefile = openList.first;

            vector<node_parents> np;
            node_parents p;
            /*
            p.x = 3;
            p.y = 2;
            p.px = 1;
            p.py = 4;
            np.push_back(p);

            printf("valeur du vecteur 1 : x = %d | y = %d | px = %d | py = %d \n", np[0].x, np[0].y, np[0].px, np[0].py);
            */

    int res = 0;
    while(res == 0 ){
         if(openList.taille > 0){
            elementDefile = openList.first;
            winner.x = openList.first->x;
            winner.y = openList.first->y;
            winner.g = openList.first->g;
            winner.f = openList.first->f;
            winner.h = openList.first->h;
            winner.parent.first = openList.first->parent.first;
            winner.parent.second = openList.first->parent.second;
            for( int i = 0; i<openList.taille; i++){
                if(elementDefile->f < winner.f){
                    winner.x = elementDefile->x;
                    winner.y = elementDefile->y;
                    winner.g = elementDefile->g;
                    winner.f = elementDefile->f;
                    winner.h = elementDefile->h;
                    winner.parent.first = elementDefile->parent.first;
                    winner.parent.second = elementDefile->parent.second;

               }
               elementDefile = elementDefile->suivant;
            }

            current.x = winner.x;
            current.y = winner.y;
            current.f = winner.f;
            current.g = winner.g;
            current.h = winner.h;
            current.parent.first = winner.parent.first;
            current.parent.second = winner.parent.second;

            neighbors(&current, &game_map[0], nbSquare, type);

            if(current.x == endPath.x && current.y == endPath.y){
                int resX = current.parent.first;
                int resY = current.parent.second;

                creerPath(&np, &path, startPath.x, startPath.y, endPath.x, endPath.y);

                res = 1;
                cheminTrouve =  1;
            }

            retirerFile(&openList, current.x, current.y);
            int tX = current.parent.first;
            int tY = current.parent.second;
            ajouterArbre(&closedList, current.x, current.y, current.g, current.h, current.f, tX, tY);

            for (int i = 0; i< 4; i++){
                if(rechercherArbre(&closedList, current.neighbors[i].second, current.neighbors[i].first) == 0
                   &&current.neighbors[i].first != -1
                   && current.neighbors[i].second != -1)
                {
                    node neighbor;
                    neighbor.x = current.neighbors[i].second;
                    neighbor.y = current.neighbors[i].first;
                    neighbor.f = current.f;
                    neighbor.h = 1;

                    float tempG = current.g + solVoisin(&game_map[0], current.x, current.y, nbSquare);

                    if(rechercherFile(&openList, current.neighbors[i].second, current.neighbors[i].first) == 1){
                        if(tempG < neighbor.g)
                        {
                            neighbor.g = tempG;
                        }
                    } else{
                        neighbor.g = tempG;
                        int h = heuristic(&neighbor, &endPath);
                        neighbor.h = h;
                        neighbor.f = neighbor.h + neighbor.g;

                        int tempX = current.x;
                        int tempY = current.y;

                        neighbor.parent.first = tempX;
                        neighbor.parent.second = tempY;

                        node_parents n;
                        n.x = neighbor.x;
                        n.y = neighbor.y;
                        n.px = tempX;
                        n.py = tempY;
                        np.push_back(n);
                        enfiler(&openList, neighbor.x, neighbor.y, neighbor.g, neighbor.h, neighbor.f, tempX, tempY);
                    }
                }
            }



        }
        else{
            res = 1;
            cheminTrouve = 0;
        }
    }

        //free(&testTree);
        if(cheminTrouve == 1){
            return path;
        }
        else{
            path[0].first = -1;
            path[0].second = -1;
            return path;
        }
    }
}





