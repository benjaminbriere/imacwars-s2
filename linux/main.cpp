#include <SDL/SDL.h>
#include <SDL/SDL_image.h>

#include <GL/gl.h>
#include <GL/glu.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <iostream>

using namespace std;

#include "structures.h"
#include "map.h"
#include "unit.h"
#include "menu.h"
#include "player.h"
#include "game.h"

#define MAXUNITE 10
#define NBUNITE 5

/* Dimensions initiales et titre de la fenetre */
static const unsigned int WINDOW_WIDTH = 1000;
static const unsigned int WINDOW_HEIGHT = 800;
static const char WINDOW_TITLE[] = "IMAC WARS V2 ";
static float aspectRatio;

// default FONT
/* Espace fenetre virtuelle */
static const float GL_VIEW_SIZE = 2.;

/* Nombre de bits par pixel de la fenetre */
static const unsigned int BIT_PER_PIXEL = 32;

/* Nombre minimal de millisecondes separant le rendu de deux images */
static const Uint32 FRAMERATE_MILLISECONDS = 10000 / 60;

void reshape(SDL_Surface** surface, unsigned int width, unsigned int height){
    printf("echo 1 \n");
    SDL_Surface* surface_temp = SDL_SetVideoMode(
        width, height, BIT_PER_PIXEL,
        SDL_OPENGL | SDL_GL_DOUBLEBUFFER | SDL_RESIZABLE);
    printf("echo 2 \n");
    if(NULL == surface_temp)
    {
        fprintf(
            stderr,
            "Erreur lors du redimensionnement de la fenetre.\n");
        exit(EXIT_FAILURE);
    }
    printf("echo 3 \n");
    *surface = surface_temp;
    printf("echo 4 \n");
    aspectRatio = width / (float) height;
    printf("echo 5 \n");
    glViewport(0, 0, (*surface)->w, (*surface)->h);
    printf("echo 6 \n");
    glMatrixMode(GL_PROJECTION);
    printf("echo 7 \n");
    glLoadIdentity();
    printf("echo 8 \n");
    if( aspectRatio > 1)
    {
        printf("echo 8-1 \n");
        gluOrtho2D(
        -GL_VIEW_SIZE / 2. * aspectRatio, GL_VIEW_SIZE / 2. * aspectRatio,
        -GL_VIEW_SIZE / 2., GL_VIEW_SIZE / 2.);
        printf("echo 8-2 \n");
    }
    //printf("echo 9 \n");
    else
    {
        printf("echo 9-1 \n");
        gluOrtho2D(
        -GL_VIEW_SIZE / 2., GL_VIEW_SIZE / 2.,
        -GL_VIEW_SIZE / 2. / aspectRatio, GL_VIEW_SIZE / 2. / aspectRatio);
        printf("echo 9-2 \n");
    }
}


int main(int argc, char** argv)
{
    Uint32 startTime = SDL_GetTicks();
    /* Initialisation de la SDL */
    if(-1 == SDL_Init(SDL_INIT_VIDEO))
    {
        fprintf(
            stderr,
            "Impossible d'initialiser la SDL. Fin du programme.\n");
        return EXIT_FAILURE;
    }

    //srand (time(NULL));


    /* Ouverture d'une fenetre et creation d'un contexte OpenGL */
    SDL_Surface* surface;


    reshape(&surface, WINDOW_WIDTH, WINDOW_HEIGHT);

    /* Initialisation du titre de la fenetre */
    SDL_WM_SetCaption(WINDOW_TITLE, NULL);

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    GLuint plaine;
    SDL_Surface* image = IMG_Load("doc/gazon.jpg");
    generateTexture(image, &plaine);
    GLuint mer;
    image = IMG_Load("doc/mer.jpg");
    generateTexture(image, &mer);
    GLuint desert;
    image = IMG_Load("doc/sable.jpg");
    generateTexture(image, &desert);
    GLuint foret;
    image = IMG_Load("doc/foret.jpg");
    generateTexture(image, &foret);
    GLuint ville;
    image = IMG_Load("doc/villeNeutre.png");
    generateTexture(image, &ville);
    GLuint montagne;
    image = IMG_Load("doc/montagne.jpg");
    generateTexture(image, &montagne);
    GLuint route;
    image = IMG_Load("doc/route.jpg");
    generateTexture(image, &route);
    GLuint villeRouge;
    image = IMG_Load("doc/villeRouge.png");
    generateTexture(image, &villeRouge);
    GLuint villeBleue;
    image = IMG_Load("doc/villeBleue.png");
    generateTexture(image, &villeBleue);


    GLuint redSoldier;
    image = IMG_Load("doc/redSoldier.png");
    generateTextureTransparant(image, &redSoldier);
    GLuint blueSoldier;
    image = IMG_Load("doc/blueSoldier.png");
    generateTextureTransparant(image, &blueSoldier);
    GLuint redBazooka;
    image = IMG_Load("doc/redBazooka.png");
    generateTextureTransparant(image, &redBazooka);
    GLuint blueBazooka;
    image = IMG_Load("doc/blueBazooka.png");
    generateTextureTransparant(image, &blueBazooka);
    GLuint redTank;
    image = IMG_Load("doc/redTank.png");
    generateTextureTransparant(image, &redTank);
    GLuint blueTank;
    image = IMG_Load("doc/blueTank.png");
    generateTextureTransparant(image, &blueTank);
    GLuint redCopter;
    image = IMG_Load("doc/redCopter.png");
    generateTextureTransparant(image, &redCopter);
    GLuint blueCopter;
    image = IMG_Load("doc/blueCopter.png");
    generateTextureTransparant(image, &blueCopter);
    GLuint redShip;
    image = IMG_Load("doc/redShip.png");
    generateTextureTransparant(image, &redShip);
    GLuint blueShip;
    image = IMG_Load("doc/blueShip.png");
    generateTextureTransparant(image, &blueShip);

    GLuint figure1;
    image = IMG_Load("doc/numbers/1.png");
    generateTextureTransparant(image, &figure1);
    GLuint figure2;
    image = IMG_Load("doc/numbers/2.png");
    generateTextureTransparant(image, &figure2);
    GLuint figure3;
    image = IMG_Load("doc/numbers/3.png");
    generateTextureTransparant(image, &figure3);
    GLuint figure4;
    image = IMG_Load("doc/numbers/4.png");
    generateTextureTransparant(image, &figure4);
    GLuint figure5;
    image = IMG_Load("doc/numbers/5.png");
    generateTextureTransparant(image, &figure5);
    GLuint figure6;
    image = IMG_Load("doc/numbers/6.png");
    generateTextureTransparant(image, &figure6);
    GLuint figure7;
    image = IMG_Load("doc/numbers/7.png");
    generateTextureTransparant(image, &figure7);
    GLuint figure8;
    image = IMG_Load("doc/numbers/8.png");
    generateTextureTransparant(image, &figure8);
    GLuint figure9;
    image = IMG_Load("doc/numbers/9.png");
    generateTextureTransparant(image, &figure9);
    GLuint figure0;
    image = IMG_Load("doc/numbers/0.png");
    generateTextureTransparant(image, &figure0);

    GLuint button1;
    image = IMG_Load("doc/buttons/redAttack.png");
    generateTextureTransparant(image, &button1);
    GLuint button2;
    image = IMG_Load("doc/buttons/redCapture.png");
    generateTextureTransparant(image, &button2);
    GLuint button3;
    image = IMG_Load("doc/buttons/redPower.png");
    generateTextureTransparant(image, &button3);
    GLuint button4;
    image = IMG_Load("doc/buttons/redAbandon.png");
    generateTextureTransparant(image, &button4);
    GLuint button5;
    image = IMG_Load("doc/buttons/redEnd.png");
    generateTextureTransparant(image, &button5);
    GLuint button6;
    image = IMG_Load("doc/buttons/redPurchase.png");
    generateTextureTransparant(image, &button6);
    GLuint button7;
    image = IMG_Load("doc/buttons/blueAttack.png");
    generateTextureTransparant(image, &button7);
    GLuint button8;
    image = IMG_Load("doc/buttons/blueCapture.png");
    generateTextureTransparant(image, &button8);
    GLuint button9;
    image = IMG_Load("doc/buttons/bluePower.png");
    generateTextureTransparant(image, &button9);
    GLuint button10;
    image = IMG_Load("doc/buttons/blueAbandon.png");
    generateTextureTransparant(image, &button10);
    GLuint button11;
    image = IMG_Load("doc/buttons/blueEnd.png");
    generateTextureTransparant(image, &button11);
    GLuint button12;
    image = IMG_Load("doc/buttons/bluePurchase.png");
    generateTextureTransparant(image, &button12);
    GLuint button13;
    image = IMG_Load("doc/buttons/menuLancerJeu.png");
    generateTextureTransparant(image, &button13);
    GLuint button14;
    image = IMG_Load("doc/buttons/menuJouer.png");
    generateTextureTransparant(image, &button14);
    GLuint button15;
    image = IMG_Load("doc/buttons/menuRegleJeu.png");
    generateTextureTransparant(image, &button15);
    GLuint button16;
    image = IMG_Load("doc/buttons/menuRetour.png");
    generateTextureTransparant(image, &button16);
    GLuint button17;
    image = IMG_Load("doc/buttons/menuOrdinateur.png");
    generateTextureTransparant(image, &button17);
    GLuint button18;
    image = IMG_Load("doc/buttons/menuJoueur.png");
    generateTextureTransparant(image, &button18);
    GLuint button19;
    image = IMG_Load("doc/buttons/superpouvoir_armechimique.png");
    generateTextureTransparant(image, &button19);
    GLuint button20;
    image = IMG_Load("doc/buttons/superpouvoir_medecin.png");
    generateTextureTransparant(image, &button20);
    GLuint button21;
    image = IMG_Load("doc/buttons/superpouvoir_collabo.png");
    generateTextureTransparant(image, &button21);
    GLuint button22;
    image = IMG_Load("doc/buttons/superpouvoir_tirsatellite.png");
    generateTextureTransparant(image, &button22);
    GLuint button23;
    image = IMG_Load("doc/buttons/menuQuitter.png");
    generateTextureTransparant(image, &button23);
    /*GLuint redBanner;
    image = IMG_Load("doc/buttons/baniereJ1.png");
    generateTextureTransparant(image, &redBanner);
    GLuint blueBanner;
    image = IMG_Load("doc/buttons/baniereJ2.png");
    generateTextureTransparant(image, &blueBanner);*/



    GLuint stat1;
    image = IMG_Load("doc/stats/statRedSoldier.png");
    generateTextureTransparant(image, &stat1);
    GLuint stat2;
    image = IMG_Load("doc/stats/statRedBazooka.png");
    generateTextureTransparant(image, &stat2);
    GLuint stat3;
    image = IMG_Load("doc/stats/statRedTank.png");
    generateTextureTransparant(image, &stat3);
    GLuint stat4;
    image = IMG_Load("doc/stats/statRedCopter.png");
    generateTextureTransparant(image, &stat4);
    GLuint stat5;
    image = IMG_Load("doc/stats/statRedShip.png");
    generateTextureTransparant(image, &stat5);
    GLuint stat6;
    image = IMG_Load("doc/stats/statBlueSoldier.png");
    generateTextureTransparant(image, &stat6);
    GLuint stat7;
    image = IMG_Load("doc/stats/statBlueBazooka.png");
    generateTextureTransparant(image, &stat7);
    GLuint stat8;
    image = IMG_Load("doc/stats/statBlueTank.png");
    generateTextureTransparant(image, &stat8);
    GLuint stat9;
    image = IMG_Load("doc/stats/statBlueCopter.png");
    generateTextureTransparant(image, &stat9);
    GLuint stat10;
    image = IMG_Load("doc/stats/statBlueShip.png");
    generateTextureTransparant(image, &stat10);
    GLuint stat11;
    image = IMG_Load("doc/stats/statRedTeam.png");
    generateTextureTransparant(image, &stat11);
    GLuint stat12;
    image = IMG_Load("doc/stats/statBlueTeam.png");
    generateTextureTransparant(image, &stat12);

    GLuint redCursor;
    image = IMG_Load("doc/redCursor.png");
    generateTextureTransparant(image, &redCursor);
    GLuint blueCursor;
    image = IMG_Load("doc/blueCursor.png");
    generateTextureTransparant(image, &blueCursor);

    GLuint menu1;
    image = IMG_Load("doc/menu/imacwars.png");
    generateTexture(image, &menu1);
    GLuint menu2;
    image = IMG_Load("doc/menu/reglejeu.png");
    generateTexture(image, &menu2);
    GLuint menu3;
    image = IMG_Load("doc/menu/adversaire.png");
    generateTexture(image, &menu3);
    GLuint menu4;
    image = IMG_Load("doc/menu/superpouvoir_joueur1.png");
    generateTexture(image, &menu4);
    GLuint menu5;
    image = IMG_Load("doc/menu/superpouvoir_joueur2.png");
    generateTexture(image, &menu5);
    GLuint menu6;
    image = IMG_Load("doc/menu/abandonjoueur1.png");
    generateTexture(image, &menu6);
    GLuint menu7;
    image = IMG_Load("doc/menu/abandonjoueur2.png");
    generateTexture(image, &menu7);
    GLuint menu8;
    image = IMG_Load("doc/menu/abandonordi.png");
    generateTexture(image, &menu8);
    GLuint menu9;
    image = IMG_Load("doc/menu/superpouvoir_bombechimique.png");
    generateTextureTransparant(image, &menu9);
    GLuint menu10;
    image = IMG_Load("doc/menu/superpouvoir_medecin.png");
    generateTextureTransparant(image, &menu10);
    GLuint menu11;
    image = IMG_Load("doc/menu/superpouvoir_satellite.png");
    generateTextureTransparant(image, &menu11);
    GLuint menu12;
    image = IMG_Load("doc/menu/superpouvoir_collabo.png");
    generateTextureTransparant(image, &menu12);
    GLuint menu13;
    image = IMG_Load("doc/menu/menu.png");
    generateTexture(image, &menu13);



    GLuint texture_map[9];
    texture_map[0] = plaine;
    texture_map[1] = foret;
    texture_map[2] = mer;
    texture_map[3] = desert;
    texture_map[4] = montagne;
    texture_map[5] = route;
    texture_map[6] = ville;
    texture_map[7] = villeRouge;
    texture_map[8] = villeBleue;

    GLuint texture_soldier[10];
    texture_soldier[0] = redSoldier;
    texture_soldier[1] = redBazooka;
    texture_soldier[2] = redTank;
    texture_soldier[3] = redCopter;
    texture_soldier[4] = redShip;
    texture_soldier[5] = blueSoldier;
    texture_soldier[6] = blueBazooka;
    texture_soldier[7] = blueTank;
    texture_soldier[8] = blueCopter;
    texture_soldier[9] = blueShip;

    GLuint texture_figure[10];
    texture_figure[0] = figure0;
    texture_figure[1] = figure1;
    texture_figure[2] = figure2;
    texture_figure[3] = figure3;
    texture_figure[4] = figure4;
    texture_figure[5] = figure5;
    texture_figure[6] = figure6;
    texture_figure[7] = figure7;
    texture_figure[8] = figure8;
    texture_figure[9] = figure9;

    GLuint texture_button[25];
    texture_button[0] = button1;
    texture_button[1] = button2;
    texture_button[2] = button3;
    texture_button[3] = button4;
    texture_button[4] = button5;
    texture_button[5] = button6;
    texture_button[6] = button7;
    texture_button[7] = button8;
    texture_button[8] = button9;
    texture_button[9] = button10;
    texture_button[10] = button11;
    texture_button[11] = button12;
    texture_button[12] = button13;
    texture_button[13] = button14;
    texture_button[14] = button15;
    texture_button[15] = button16;
    texture_button[16] = button17;
    texture_button[17] = button18;
    texture_button[18] = button19;
    texture_button[19] = button20;
    texture_button[20] = button21;
    texture_button[21] = button22;
    texture_button[22] = button23;
    /*texture_button[23] = redBanner;
    texture_button[24] = blueBanner;*/

    GLuint texture_stat[12];
    texture_stat[0] = stat1;
    texture_stat[1] = stat2;
    texture_stat[2] = stat3;
    texture_stat[3] = stat4;
    texture_stat[4] = stat5;
    texture_stat[5] = stat6;
    texture_stat[6] = stat7;
    texture_stat[7] = stat8;
    texture_stat[8] = stat9;
    texture_stat[9] = stat10;
    texture_stat[10] = stat11;
    texture_stat[11] = stat12;

    GLuint texture_cursor[2];
    texture_cursor[0] = redCursor;
    texture_cursor[1] = blueCursor;

    GLuint texture_menu[13];
    texture_menu[0] = menu1;
    texture_menu[1] = menu2;
    texture_menu[2] = menu3;
    texture_menu[3] = menu4;
    texture_menu[4] = menu5;
    texture_menu[5] = menu6;
    texture_menu[6] = menu7;
    texture_menu[7] = menu8;
    texture_menu[8] = menu9;
    texture_menu[9] = menu10;
    texture_menu[10] = menu11;
    texture_menu[11] = menu12;
    texture_menu[12] = menu13;


    //printf(" Case mer | int : %d / Glint8 : %u \n", mer, mer);
    //printf(" Case tab[1] | int : %d / Glint8 : %u \n", texture_map[1], texture_map[1]);

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // GESTION DES FONT

    // FIN GESTION FONT
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    int nbSquare = 10;
    map_square *game_map = NULL;
    game_map = (map_square *) malloc( nbSquare * nbSquare * sizeof(map_square));
    nullMap(nbSquare, &game_map[0]);
    player *game_player = NULL;
    game_player = (player *) malloc( 2 * sizeof(player));
    playerCreator(&game_player[0],&game_map[0], nbSquare);
    printf("GOLD player 1 : %d \n", game_player[0].gold);
    printf("GOLD player 2 : %d \n", game_player[1].gold);
    mapCreator(surface, texture_map, nbSquare, &game_map[0]);



///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /* Boucle principale */
    int loop = 1;
    int lancement = 0;
    int type = 0;
    int mode = 0;
    int player = 0;
    while(loop)
    {
        /* Recuperation du temps au debut de la boucle */


        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // GESTION DES CAS
/*
        if(mode == 0){ // CAS STANDARD : AFFICHAGE MAP
            mapDisplay(surface, texture_map, nbSquare, &game_map[0], aspectRatio);
            menuDisplay(surface, texture_soldier, nbSquare, &game_map[0], &game_player[0],
                        aspectRatio, player, texture_figure, texture_button, texture_stat);
            soldierDisplay(surface, texture_soldier, nbSquare, &game_map[0], aspectRatio);

        }
        else if (mode == 1){ //CAS ACHAT UNITE
            mapDisplay(surface, texture_map, nbSquare, &game_map[0], aspectRatio);
            soldierDisplay(surface, texture_soldier, nbSquare, &game_map[0], aspectRatio);
        }
*/

        loop = game(surface, nbSquare, &game_map[0], &game_player[0], aspectRatio,
                  texture_map,
                  texture_soldier,
                  texture_figure,
                  texture_button,
                  texture_stat,
                  texture_cursor,
                  texture_menu
                );

        // FIN GESTION DES CAS
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////




    }

    //glDeleteTextures(1, texture_id);
    //glDeleteTextures(1, &mer);
    glDeleteTextures(1, texture_map);
    glDeleteTextures(1, texture_soldier);
    glDeleteTextures(1, texture_figure);
    glDeleteTextures(1, texture_button);
    glDeleteTextures(1, texture_stat);
    glDeleteTextures(1, texture_cursor);
    glDeleteTextures(1, texture_menu);
    //glDeleteTextures(1, &soldier);
    /* Liberation de la m�moire occupee par img */

    SDL_FreeSurface(surface);
    SDL_FreeSurface(image);



    //SDL_FreeSurface(image1);
    /* Liberation des ressources associees a la SDL */

    SDL_Quit();
    free(game_map);
    free(game_player);

    return EXIT_SUCCESS;
}
