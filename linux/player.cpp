/////////////////////////////////////////////////
//                PLAYER.CPP                   //
//                                             //
//     by Gartner Esther & Briere Benjamin     //
//                                             //
/////////////////////////////////////////////////
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
//#include <SDL/SDL_ttf.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <iostream>

using namespace std;

#include "structures.h"
#include "map.h"
#include "unit.h"
#include "menu.h"
#include "player.h"
//#include "game.cpp"

#define MAXUNITE 10
#define NBUNITE 5


void playerCreator(player *game_player, map_square *game_map, int nbSquare){
    game_player[0].gold = 3000;
    game_player[0].id = 1;
    game_player[0].nbUnite = 2;
    game_player[0].pouvoir = 1;
    game_player[0].ville = 0;
    game_player[0].liste[0]= unit_soldier(0, 1, nbSquare, game_map, 1);
    game_player[0].liste[1]= unit_soldier(1, 0, nbSquare, game_map, 1);

    game_player[1].gold = 2000;
    game_player[1].id = 2;
    game_player[1].nbUnite = 2;
    game_player[1].pouvoir = 2;
    game_player[1].ville = 0;
    game_player[1].liste[0]= unit_soldier(1, 1, nbSquare, game_map, 2);
    game_player[1].liste[1]= unit_soldier(8, 9, nbSquare, game_map, 2);
}

void playerUnit(player *game_player, int pl){

    printf(" LECTURE SOLDATS \n\n");

    for (int i = 0; i < game_player[pl].nbUnite; i++){
        printf("cX : %d \n", game_player[pl].liste[i].cX);
        printf("cY : %d \n", game_player[pl].liste[i].cY);
        printf("fuel : %d \n", game_player[pl].liste[i].fuel);
        printf("type : %d \n", game_player[pl].liste[i].type_soldier);
    }

    printf(" FIN LECTURE SOLDATS \n\n");
}

void playerPower(SDL_Surface* surface, map_square *game_map, player *p,int player, int nbSquare, float aspectRatio, int x, int y){

    if( p[player].pouvoir != 0){

        if( p[player].pouvoir == 1){ // GAZ TOXIQUE

            for (int i = 0; i < MAXUNITE ; i++){
                if( p[0].liste[i].player == 1 ){
                    if(p[0].liste[i].life / 2 == 0){
                        p[0].liste[i].life = 1;
                        game_map[p[0].liste[i].cY * nbSquare + p[0].liste[i].cX].life = 1;
                    }else{
                        p[0].liste[i].life = p[0].liste[i].life / 2;
                        game_map[p[0].liste[i].cY * nbSquare + p[0].liste[i].cX].life = p[0].liste[i].life;
                    }
                }

                if( p[1].liste[i].player == 2 ){
                    if(p[1].liste[i].life / 2 == 0){
                        p[1].liste[i].life = 1;
                        game_map[p[1].liste[i].cY * nbSquare + p[1].liste[i].cX].life = 1;
                    }else{
                        p[1].liste[i].life = p[1].liste[i].life / 2;
                        game_map[p[1].liste[i].cY * nbSquare + p[1].liste[i].cX].life = p[1].liste[i].life;
                    }
                }
            }

            p[player].pouvoir = 0;
        }

        if( p[player].pouvoir == 2){ // SUPER SOIN

            for (int i = 0; i < MAXUNITE ; i++){

                if( p[player].liste[i].player == 1+player ){
                    if(p[player].liste[i].life > 50){
                        p[player].liste[i].life = 100;
                        game_map[p[player].liste[i].cY * nbSquare + p[player].liste[i].cX].life = 100;
                    }else{
                        p[player].liste[i].life = p[player].liste[i].life +50;
                        game_map[p[player].liste[i].cY * nbSquare + p[player].liste[i].cX].life = p[player].liste[i].life;
                    }
                }
            }

            p[player].pouvoir = 0;
        }

        if( p[player].pouvoir == 3){ // RAYON DE LA MORT

            game_map[y*nbSquare+x].action = 0;
            game_map[y*nbSquare+x].dexterity = 0;
            game_map[y*nbSquare+x].fuel = 0;
            game_map[y*nbSquare+x].life = 0;
            game_map[y*nbSquare+x].player = 0;
            game_map[y*nbSquare+x].range = 0;
            game_map[y*nbSquare+x].strength = 0;
            game_map[y*nbSquare+x].type_soldier =0;

            if(player == 0){
                for (int i = 0; i < MAXUNITE ; i++){
                    if(p[1].liste[i].cX == x && p[1].liste[i].cY == y){
                        p[1].liste[i] = game_map[y*nbSquare+x];
                        p[1].nbUnite = p[1].nbUnite - 1;
                    }
                }
            }
            else{
                for (int i = 0; i < MAXUNITE ; i++){
                    if(p[0].liste[i].cX == x && p[0].liste[i].cY == y){
                        p[0].liste[i] = game_map[y*nbSquare+x];
                        p[0].nbUnite = p[0].nbUnite - 1;
                    }
                }
            }

            p[player].pouvoir = 0;
        }

        if( p[player].pouvoir == 4){ // COLABO

            if(player == 0){

                if( p[0].nbUnite < MAXUNITE ){

                    game_map[y*nbSquare+x].player = 1;
                    game_map[y*nbSquare+x].type_soldier = game_map[y*nbSquare+x].type_soldier - 5;

                    for (int i = 0; i < MAXUNITE ; i++){
                        if(p[1].liste[i].cX == x && p[1].liste[i].cY == y){
                            p[1].liste[i] = game_map[y*nbSquare+x];
                            p[1].nbUnite = p[1].nbUnite - 1;
                            p[0].nbUnite = p[0].nbUnite + 1;
                        }
                    }

                    p[player].pouvoir = 0;

                }
            }

            if(player == 1){

                if( p[1].nbUnite < MAXUNITE ){

                    game_map[y*nbSquare+x].player = 0;
                    game_map[y*nbSquare+x].type_soldier = game_map[y*nbSquare+x].type_soldier + 5;

                    for (int i = 0; i < MAXUNITE ; i++){
                        if(p[0].liste[i].cX == x && p[0].liste[i].cY == y){
                            p[0].liste[i] = game_map[y*nbSquare+x];
                            p[0].nbUnite = p[0].nbUnite - 1;
                            p[1].nbUnite = p[1].nbUnite + 1;
                        }
                    }

                    p[player].pouvoir = 0;

                }
            }


        }

    }

}
