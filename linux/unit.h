/////////////////////////////////////////////////
//                    UNIT.H                   //
//                                             //
//     by Gartner Esther & Briere Benjamin     //
//                                             //
/////////////////////////////////////////////////

#include <vector>

void soldierCreator(int x, int y, int nbSquare, SDL_Surface* surface, map_square *game_map, int type, player *p, int player);
void soldierDisplay(SDL_Surface* surface, GLuint texture_soldier[], int nbSquare, map_square *game_map, float aspectRatio);

map_square unit_soldier(int x, int y, int nbSquare, map_square *game_map, int id);
void reapprovisionnement(int nbSquare, map_square *game_map, player *p, int player);
void captureBuilding(map_square *game_map, player *game_player, int nbSquare,  float aspectRatio, int pl, int sizeSquare, int x, int y);

void rangeCreator(map_range *game_range, map_square *game_map, int x, int y, int nbSquare);

void mouvementCreator(SDL_Surface* surface,map_range *game_range, map_square *game_map, int x, int y, int nbSquare, int type, float aspectRatio);

void unitMouvement(SDL_Surface* surface, map_square *game_map, player *p, int pl, int x, int y, int destX, int destY, int nbSquare, float aspectRatio);

int rangeCheck(map_square *game_map, int x, int y, int destX, int destY, int nbSquare);

float force (int attaquant, int defenseur);
void attaque (map_square *game_map, player *p, int pl, int attaquantX, int attaquantY, int defenseurX, int defenseurY, int nbSquare);

void animationMouvement(map_square *game_map, SDL_Surface* surface, GLuint texture_soldier[],
                        vector < pair<int,int> > tamp, int *indexTemp, int *tamponX, int *tamponY,
                        int *directionTemp, int sizeSquare, float aspectRatio, int nbSquare, int type, int *tamptest, player *p, int pl);
