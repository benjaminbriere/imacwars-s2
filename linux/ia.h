/////////////////////////////////////////////////
//                    IA.H                     //
//                                             //
//     by Gartner Esther & Briere Benjamin     //
//                                             //
/////////////////////////////////////////////////

#include <vector>

void computerTurnPurchase(SDL_Surface* surface, map_square *game_map, player *p,int player, int nbSquare, float aspectRatio, int *fin, int sizeSquare);
void computerTurn(SDL_Surface* surface, map_square *game_map, player *p,int player, int nbSquare, float aspectRatio, int *fin);

void computerTurnMouvementFantassin(SDL_Surface* surface, map_square *game_map, GLuint texture_soldier[], player *p,int player, int nbSquare, float aspectRatio, int *fin, int *numFantassin, vector < pair<int,int> > listeFantassin, int *x, int *y, int *indexTemp, int *directionTemp, int sizeSquare);

villeIA isRange(int x, int y, int destX, int destY, int range, int nbSquare, int aspectRatio, SDL_Surface* surface, map_square *game_map, int k, player *p, int player );
int toRange(int x, int y,int range, int nbSquare, map_square *game_map);
int findTarget(int x, int y, int cibleX, int cibleY, int range, int nbSquare, map_square *game_map);

villeIA isRangeRes(int destX, int destY, int range, int nbSquare, int aspectRatio, SDL_Surface* surface, map_square *game_map, int k, player *p, int player );

void purchaseUnitComputer(int nbSquare, SDL_Surface* surface, map_square *game_map, int type, player *p, int player, int sizeSquare);

void computerTurnMouvementOthers(SDL_Surface* surface, map_square *game_map, GLuint texture_soldier[], player *p,int player, int nbSquare, float aspectRatio, int *fin, int *numFantassin, vector < pair<int,int> > listeFantassin ,int *x, int *y, int *indexTemp, int *directionTemp, int sizeSquare);

int outMap(int x, int nbSquare);
